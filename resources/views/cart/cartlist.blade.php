
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">
    <title>Cart</title>
</head>
<body class="cart">
<header class="header header-transparent">
    <div class="header-middle">
        <div class="container">
            <div class="header-left">
                <a href="details.html"><i class='fas fa-close ml-2' style="font-size: 20px;"></i></a>&nbsp;&nbsp;

            </div>
            <!-- End .header-left -->
            <a href="#" class="logo">
                <b><span class="font">Carrito</span></b>
            </a>
            <!-- End .header-right -->
        </div>
        <!-- End .container-fluid -->
    </div>
    <!-- End .header-middle -->

</header>
<div class="container-fluid mt-62">
    <div class="row ">
        @foreach($carts as $cart)
        <div class="col-12 mt-4">
            <i class="fas fa-ellipsis-v dot"></i>
            <div class="box">
                <img src="{{ $cart->product_image  }}" width="100%">
            </div>
            <p>${{ $cart->product_price }} <small>{{ $cart->product_color }}</small></p>
            <p class="p-0 m-0"><b>{{ $cart->product_name }}</b></p>
            <p><?php echo $cart->description ?></p>
            <select>
                <option>Qty</option>
                <option value="1" {{ $cart->quantity == 1 ? 'selected':'' }}>1</option>
                <option value="2" {{ $cart->quantity == 2 ? 'selected':'' }}>2</option>
                <option value="3" {{ $cart->quantity == 3 ? 'selected':'' }}>3</option>
            </select>
            <!-- End .product-single-tabs -->

        </div>
            @endforeach
    </div>
    <div class="line_grey mt-3"></div>
    <div class="row py-2">
        <div class="col-8">
            <div class="form-group m-0">
                <input type="email" class="form-control" placeholder="Coupon or Voucher">
                <!-- <select class="form-control" id="exampleFormControlSelect1">
                  <option>Coupon or voucher</option>
                  <option>Coupon or voucher</option>
                  <option>Coupon or voucher</option>
                  <option>Vouchers</option>
                  <option>Vouchers</option>
                </select> -->
            </div>
        </div>
        <div class="col-4 text-center d-flex justify-content-center">
            <a href="" class="send"><i class="fas fa-arrow-right"></i></a>
        </div>
    </div>
    <div class="line_grey"></div>

    <div class="row mt-3">
        <div class="col-6">
            <div class="pull-left">
                <p class="grey">Subtotal:</p>
            </div>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <p class="grey">${{ $subtotal->price }} <small>mxn</small></p>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-6">
            <div class="pull-left">
                <p class="grey">Envio:</p>
            </div>
        </div>
        <div class="col-6">
            <div class="pull-right">
                <p class="grey">$50 <small>mxn</small></p>
            </div>
        </div>
    </div>
    <hr>
    <div class="row ">
        <div class="col-6 text-center">
            <p class="grey">Total:</p>
        </div>
        <div class="col-6 text-right">
            <p class="or"><b>${{ $subtotal->price + 50 }} <small>mxn</small></b></p>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <h4 class="text-center"><b>Also you can like this products</b></h4>
        </div>
    </div>

    <div class="row">
        @foreach($products as $product)
            <?php $productId = \App\Traits\CommonTrait::encodeId($product->pid); ?>
            <div class="col-6 col-md-4 col-lg-3 col-xl-2">
                <a href="{{ route('product.description',$productId) }}">

                <div class="product-default">
                <figure>
                    <a href="#">
                            <img src="{{URL::asset('storage/images/'. $product->images) }}" alt="Fjords" style="width:100%">                    </a>
                </figure>
                <div class="product-details">
                    <h2 class="product-title">
                        <a href="#">{{ $product->pname }}</a>
                    </h2>
                    <div class="price-box">
                        <span class="product-price"><i>${{ $product->product_price }}</i> MXN</span>
                    </div>
                    <!-- End .price-box -->
                    <div class="category-wrap">
                        <div class="category-list">
                            <a href="#" class="product-category">recibelo</a>
                        </div>
                    </div>
                </div>
                <!-- End .product-details -->
            </div>
                </a>
        </div>
            @endforeach

    </div>
</div>
<div class="product-action mt-3">
    <div class="product_price ml-3">
        <span>Total</span>
        <span><b>${{ $subtotal->price + 50 }} Mxn</b></span>
    </div>
    <a href="details-2.html" class="paction add-compare" title="Add to compare">
        <span>Continuar</span>
    </a>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<script src="{{ asset('assets/js/number.js') }}"></script>
</body>
</html>