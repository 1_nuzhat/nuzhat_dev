@include('front_layouts.header')
<style>
    .cate h4 {
        font-size :17px}
    .cate {
        font-size: 15px;
    }
</style>

{{--         <div class="container-fluid infinite">--}}
{{--            <div class="row">--}}
{{--                <div class="col-2"></div>--}}
{{--                <div class="col-10">--}}
{{--                   <div class="sort">--}}
{{--                       <p>Sort By:</p>--}}
{{--                   </div>--}}
{{--                   <div class="select_1">--}}
{{--                       <ul>--}}
{{--                           <li>Recommend</li>--}}
{{--                           <li class="active">Hottest</li>--}}
{{--                           <li>Newest</li>--}}
{{--                           <li>Rating</li>--}}
{{--                           <li>Trending</li>--}}
{{--                       </ul>--}}
{{--                   </div>--}}
{{--                   <div class="price_1">--}}
{{--                       <p>Price <i class="fa fa-sort" aria-hidden="true"></i></p>--}}
{{--                   </div>--}}
{{--                </div>--}}
{{--             </div>--}}
{{--         </div>--}}

<!-- infinite scroll  -->
<section >
    <div class="black">
        <ul>
            <li class="active"><img src="{{ asset('assets/img/FLOATING-BAR3.jpg') }}" alt="icon"></li>

            <li><img src="{{ asset('assets/img/FLOATING-BAR2.jpg') }}" alt="icon"></li>

            <li><img src="{{ asset('assets/img/FLOATING-BAR4.jpg') }}" alt="icon"></li>

        </ul>
    </div>
    <div class="container-fluid my-3 infinite">
        <div class="row">
            <div class="col-2">
                <div class="fixed">
                    @foreach($familySubcats as $familySub)
                        <div class="cate">
                            <h4><b>{{ $familySub['fname'] }}</b></h4>
                            <ul>
                                @foreach($familySub['subcat'] as $subcat)
                                    <?php $subId = \App\Traits\CommonTrait::encodeId($subcat->categories_id) ?>
                                    <li><a href="{{ route('subcat.products',$subId) }}" ><p style="color: black;">{{ $subcat->category->cat_name }}</p></a></li>
                                    {{--                                        <li><a href="{{ route('subcat.products') }}" class="productlist" data-url="{{ route('subcat.products') }}" data-category="{{ $familySub['fid'] }}" data-subcat="{{ $subcat->categories_id }}">{{ $subcat->category->cat_name }}</a></li>--}}
                                @endforeach

                            </ul>
                        </div>
                    @endforeach
                </div>


            </div>
            <div class="col-10">
                <input type="hidden" id="lastId" value="{{ $lastProductId}}">
                <input type="hidden" id="previousID" >
                <div class="row branchReplace productslisting" id="productslisting">
                    @foreach($products as $product)
                        <?php $productId = \App\Traits\CommonTrait::encodeId($product->pid); ?>
                        <div class="col-sm-3">
                            <div class="thumbnail">
                                <a href="{{ route('product.description',$productId) }}">
                                    <img class="lazy" src="{{URL::asset('assets/img/PRELOADER.jpg') }}" data-src="{{URL::asset('storage/images/'. $product->images) }}" alt="Fjords" style="width:100%">
                                    <div class="caption">
                                        <p style="color: #000000;">{{ $product->pname }}</p>
                                        <p >${{ $product->product_price }}<span>MXN</span></p>
                                        <p ></p>
                                    </div></a>
                            </div>
                        </div>
                    @endforeach
                </div>

                <!-- <div class="row my-5">
                    <div class="col-12">
                    <div class="text-center">
                        <button class="see_more">Ver mas</button>
                    </div>
                    </div>
                </div> -->
            </div>
        </div>
    </div>
</section>
<!-- infinite scroll  -->
<!-- </div> -->



<!-- dekstop-view  -->

<div class="mobile_view">
    <div id="myHeader" class="category_top">
        <div class="category_title">
            <div><a class="back" href="{{ url('/') }}"><img src="{{ asset('assets/img/back.png') }}"></a></div>
            <div>
                <h2>Almacenamiiento y organizacion</h2>
                <span class="total product">11 Productors</span>
            </div>
            <div>
                <div class="dropdown cart-dropdown">
                    <a href="{{ route('cart.list') }}" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
                        <img src="{{ asset('assets/img/2 icon gray.png') }}">
                        <!--<span class="cart-count">2</span>-->
                    </a>
                </div>
                <!-- End .dropdown -->
            </div>
        </div>
        <section class="filter_Sec">
            <div class="select-custom">
                <select name="orderby" class="form-control">
                    <option value="menu_order" selected="selected">ordenar</option>
                    <option value="popularity">1</option>
                    <option value="rating">2</option>
                    <option value="date">3</option>
                    <option value="price">4</option>
                    <option value="price-desc">5</option>
                </select>
            </div>
            {{--            <div class="filter">--}}
            {{--                <span>filtro <img src="assets/img/filter.png"></span>--}}
            {{--            </div>--}}
        </section>
    </div>

    <section class="container-fluid item-product scrolldown" style="display:block;">
        <div class="row row-sm productslisting">
            @foreach($products as $product)
                <?php $productId = \App\Traits\CommonTrait::encodeId($product->pid); ?>
                <div class="col-6 col-md-4 col-lg-3 col-xl-2">
                    <div class="product-default">
                        <figure>
                            <a href="{{ route('product.description',$productId) }}">
                                <img class="lazy" src="{{URL::asset('assets/img/PRELOADER.jpg') }}" data-src="{{URL::asset('storage/images/'. $product->images) }}">
                            </a>
                        </figure>
                        <div class="product-details">
                            <h2 class="product-title">
                                <a href="product.html">{{ $product->pname }}</a>
                            </h2>
                            <div class="price-box">
                                <span class="product-price"><i>${{ $product->product_price }}</i> MXN</span>
                            </div>
                            <!-- End .price-box -->
                            <div class="category-wrap">
                                <div class="category-list">
                                    <p ></p>
                                </div></a>
                            </div>
                        </div>
                        <!-- End .product-details -->
                    </div>
                </div>
            @endforeach

        </div>
    </section>
    <div class="filter-overlay"></div>
</div>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/number.js') }}"></script>
<script>
    $(document).ready(function(){
        scrollerData();
        var i =2;
        $(window).scroll(function() {
            if($(window).scrollTop() >= $('.scrolldown').offset().top + $('.scrolldown').outerHeight() - window.innerHeight) {
                // ajax call get data from server and append to the div
                // if (scroller) {
                var rout = '{!! Route::currentRouteName() !!}';
                // alert(rout);
                var lastId = $('#lastId').val();
                var previousID = $('#previousID').val();
                if (lastId) {
                    if (previousID != lastId) {
                        $('#previousID').val(lastId);
                        $.ajax(
                            {
                                url: '{{ route('get.more.data.category') }}',
                                type: 'GET',
                                data: {
                                    "page": i,
                                    'currentriute': rout,
                                    'lastId': lastId
                                },
                                success: function (result) {
                                    if (result.success) {
                                        // $('.productslisting').html();
                                        // toastr.success('Cart updated successfully');
                                        $('.productslisting').append(result.data);
                                        $('#lastId').val(result.lastProductId);
                                    } else {
                                        return false;
                                    }
                                }
                            });
                        i = parseInt(i) + 1;
                    } else {
                        return false;
                    }
                } else {
                    return false;
                }
            }
            // }
        });
    });
</script>
</body>
</html>