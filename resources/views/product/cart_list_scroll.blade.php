
@foreach($products as $product)
    <?php $productId = \App\Traits\CommonTrait::encodeId($product->pid); ?>
    <div class="col-6 col-md-4 col-lg-3 col-xl-2">
        <div class="product-default">
            <figure>
                <a href="{{ route('product.description',$productId) }}">
                    <img class="lazy" src="{{URL::asset('assets/img/PRELOADER.jpg') }}"
                         data-src="{{URL::asset('storage/images/'. $product->images) }}">
                </a>
            </figure>
            <div class="product-details">
                <h2 class="product-title">
                    <a href="#">{{ $product->pname }}</a>
                </h2>
                <div class="price-box">
                    <span class="product-price"><i>${{ $product->product_price }}</i> MXN</span>
                </div>
                <!-- End .price-box -->
                <div class="category-wrap">
                    <div class="category-list">
                        <a href="#" class="product-category">Recibelo Mañana</a>
                    </div>
                </div>
            </div>
            <!-- End .product-details -->
        </div>
    </div>
@endforeach


<script>
    scrollerData();
</script>
