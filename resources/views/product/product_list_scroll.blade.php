@foreach($products as $product)
    <?php $productId = \App\Traits\CommonTrait::encodeId($product->pid); ?>
    <div class="col-sm-3">
        <div class="thumbnail">
            <a href="{{ route('product.description',$productId) }}">
                <img class="lazy" src="{{URL::asset('assets/img/PRELOADER.jpg') }}" data-src="{{URL::asset('storage/images/'. $product->images) }}" alt="Fjords" style="width:100%">
                <div class="caption">
                    <p style="color: #000000;">{{ $product->pname }}</p>
                    <p >${{ $product->product_price }}<span>MXN</span></p>
                    <p >Recibelo Mañana</p>
                </div></a>
        </div>
    </div>
@endforeach


<script>
    scrollerData();
</script>
