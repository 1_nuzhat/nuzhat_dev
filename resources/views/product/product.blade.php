@include('front_layouts.header')
<style>
    .num input {
        width: 30px;
        text-align: center;
        font-size: 18px;
        color: #a79f9f;
        border: 1px solid rgb(0, 0, 0);
        display: inline-block;
        vertical-align: baseline;
    }

    .num {
        margin-top: 5px;
    }

    span.minus {
        border: 1px solid #000;
        padding: 4px 5px;
        cursor: pointer;
    }

    span.plus {
        border: 1px solid #000;
        padding: 4px 5px;
        display: inline;
        cursor: pointer;
        vertical-align: baseline;
    }

    .play_icon img {
        width: 70px !important;
    }

    .play_icon {
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }

    iframe {
        position: relative;
    }

</style>
<section class="dekstop_slider mb-3">
    <div class="black">
        <ul>
            <li class="active"><img src="{{ asset('assets/img/FLOATING-BAR3.jpg') }}" alt="icon"></li>

            <li><img src="{{ asset('assets/img/FLOATING-BAR2.jpg') }}" alt="icon"></li>

            <li><img src="{{ asset('assets/img/FLOATING-BAR4.jpg') }}" alt="icon"></li>

        </ul>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <div class="fixed">
                    <div class="row">
                        <div class="col-md-2 col-4">
                            <div class="box">
                                @foreach($varients as $productImg)
                                    @if($productImg->productImage->images)
                                        <div class="box_1 image_data" data-url="{{ route('image.view') }}"
                                             data-imageid="{{$productImg->productImage->id}}">
                                            <div class="over_cover"></div>
                                            {{--                                    <div class="over_cover">+7</div>--}}
                                            <span class="">
                                         <img src="{{URL::asset('storage/images/'. $productImg->productImage->images) }}"
                                              class="img-fluid"></span>
                                        </div>
                                    @endif
                                @endforeach
                                @if($pvariant->video_link)
                                    <div class="box_1 video_data" data-url="{{ route('video.view') }}"
                                         data-videoid="{{$pvariant->id}}">
                                        <div class="over_cover"></div>
                                        <div class="over_cover"><img src="{{ asset('assets/img/PLAY ICON copy.png') }}"
                                                                     style="width: 20px;"></div>
                                        <span class="">
{{--                                             <iframe src="https://www.youtube.com/embed/{{$pvariant->video_id}}?" frameborder="0" allowfullscreen></iframe>--}}

                                            {{--                                             <x-embed url="{{ $pvariant->video_link }}" />--}}
                                            {{--                                              <iframe class="img-fluid" height="480" width="500"--}}
                                            {{--                                                      src="https://www.youtube.com/watch?v=pK060iUFWXg/?controls=0">--}}
                                            {{--                         </iframe>--}}
                                            {{--                                         <img src="{{ $pvariant->video_link }}" class="img-fluid">--}}
                                         </span>
                                    </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md-10 col-8 p-3">
                            <div class="text-center">
                                <div class="large replace_image">
                                    <?php $i = 0;?>
                                    @foreach($varients as $productImg)
                                        @if ($loop->first)
                                            @if($productImg->productImage->images)
                                                <?php $i++; ?>
                                                <img src="{{URL::asset('storage/images/'. $productImg->productImage->images) }}"
                                                     class="img-fluid">
                                            @endif
                                        @endif
                                    @endforeach
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
            <div class="col-lg-6" style="margin-top: 46px;">
                <div class="fixed">
                    <h2>{{ $pvariant->name }}</h2>
                    <div class="caption">
                        <p style="color: #ff5500;font-size: 30px;font-weight: bold;">$<span
                                    class="sizePrice">{{ @$pvariant->productVariant->product_price }}</span><span>MXN</span>
                        </p>
                    </div>
                    <form>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="inputState" class="grey">Color:</label>
                                <select id="inputState" class="form-control product_colors"
                                        data-url="{{ route('product.size') }}" data-productId="{{ $pvariant->id }}">
                                    <option>Selecciona</option>
                                    @foreach($colors as $productColor)
                                        <option value="{{ $productColor->colorData->id }}"> {{ $productColor->colorData->color_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group col-md-6 size_details">
                                <label for="inputState" class="grey">Talla:</label>
                                <select id="inputState" class="form-control color_size"
                                        data-url="{{ route('get.size.price') }}" data-productId="{{ $pvariant->id }}">
                                    <option>Selecciona</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-md-2 p-0">
                                <div class="num">
                                    <div class="number">
                                        <span class="minus removeqnt">-</span>
                                        <input type="text" id="quantity" name="p_qnt" value="1"/>
                                        <span class="plus addqnt">+</span>
                                    </div>
                                </div>
                            </div>
                            {{--                           <div class="form-group col-md-2">--}}
                            {{--                            <label for="inputZip "><span class="removeqnt">minus</span></label>--}}
                            {{--                           <input type="hidden" class="form-control" id="quantity" name="p_qnt" value="1">--}}
                            {{--                               <label for="inputZip "><span class="addqnt">plush</span></label>--}}
                            {{--                        </div>--}}

                            <div class="form-group col-md-10">
                                <button type="button" class="sign addtocart" data-url="{{ route('add.cart') }}"
                                        data-pid="{{ $pvariant->id }}">Comprar
                                </button>
                            </div>
                        </div>


                    </form>
                    <!-- End .price-box -->
                    <div class="product-desc">
                        <ul>
                        @foreach($msg as $msgs)
                            <li class="mb-2">{{ $msgs->title }}
                            </li>
                            @endforeach
                            <li
                                    class="mb-2">15 días de garantía
                            </li>
                            <li class="mb-2">Págalo en efectivo al recibir
                            </li>
                            <li class="mb-2">Cambios y devoluciones sin costo
                            </li>
                        </ul>
                    </div>
                    <!-- End .product-desc -->
                    <div class="list_1 mt-5">
                        <p><b><?php echo $pvariant->size_desc;?></b></p>
                        <p></p>
                        <?php echo $pvariant->description;?>
                        {{--                         {{ $desc }}--}}
                        {{--                        {{  strip_tags(preg_replace('/\s|&nbsp;/', '', $pvariant->description)) }}--}}
                    </div>
                    <!-- end list  -->
                    @foreach($productImage as $pimage)
                        <div class="img">
                            <img src="{{URL::asset('storage/images/'. $pimage->image) }}" class="img-fluid">
                        </div>

                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
<!-- infinite scroll  -->
<section class="scrolldown">
    <div class="container my-3 infinite">
        <div class="row">
            <div class="col-12">
                <div class="text-center">
                    <h5><b>Articulos similares:</b></h5>
                </div>
                <div class="line"></div>
            </div>
        </div>
        <input type="hidden" id="lastId" value="{{ $lastProductId}}">
        <input type="hidden" id="previousID" >
        <div class="row productslisting" id="productslisting">
            @foreach($products as $product)
                <?php $productId = \App\Traits\CommonTrait::encodeId($product->pid); ?>
                <div class="col-sm-3">
                    <div class="thumbnail">
                        <a href="{{ route('product.description',[$productId,$str]) }}">
                            <img class="lazy" src="{{URL::asset('assets/img/PRELOADER.jpg') }}"
                                 data-src="{{URL::asset('storage/images/'. $product->images) }}" alt="Fjords"
                                 style="width:100%">
                            <div class="caption">
                                <p style="color: #000000;">{{ $product->pname }}</p>
                                <p>${{ $product->product_price }}<span>MXN</span></p>
                                @foreach($msg as $msgs)
                                <p>{{ $msgs->title }}</p>
                                @endforeach
                            </div>
                        </a>
                    </div>
                </div>
            @endforeach

        </div>
        <div class="row my-5">
            <div class="col-12">
                <div class="text-center">
                    {{--                           <button class="see_more">Ver mas</button>--}}
                </div>
            </div>
        </div>
    </div>
</section>
<!-- infinite scroll  -->
</div>


<!-- dekstop-view  -->


<div class="container-fluid mt-5">
    <div class="row">
        <div class="col-12">
            <div class="product-single-container">
                <div class="row">
                    <div class="product-single-gallery">
                        <div class="product-slider-container product-item">
                            <!-- slider -->
                            <div id="demo" class="carousel slide" data-ride="carousel">
                                <ul class="carousel-indicators">
                                    <?php $k = 0; ?>
                                    @foreach($productImage as $pimage)
                                        <li data-target="#demo" data-slide-to="{{ $k }}"
                                            class="{{ $k==0 ? 'active' :'' }}"></li>
                                        <?php $k++; ?>
                                    @endforeach
                                </ul>
                                <div class="carousel-inner">
                                    <?php $m = 0; ?>
                                    @foreach($productImage as $pimage)
                                        <div class="carousel-item {{ $m == 0 ?'active' :'' }}">
                                            <img src="{{URL::asset('storage/images/'. $pimage->image) }}" alt="banner"
                                                 class="img-fluid">
                                            <!-- <div class="carousel-caption">
                                               <h3>Los Angeles</h3>
                                               <p>We had such a great time in LA!</p>
                                               </div>-->
                                        </div>
                                        <?php $m++; ?>
                                    @endforeach

                                </div>
                                <a class="carousel-control-prev" href="#demo" data-slide="prev">
                                    <span class="carousel-control-prev-icon"></span>
                                </a>
                                <a class="carousel-control-next" href="#demo" data-slide="next">
                                    <span class="carousel-control-next-icon"></span>
                                </a>
                            </div>
                            <!-- slider -->
                            <!-- End .product-single -->
                            <span class="prod-full-screen">
                           <i class="icon-plus"></i>
                           </span>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="product-single-details">
                            <h1 class="product-title">{{ $pvariant->name }}</h1>
                            <div class="price-box">
                                <span class="product-price">MXN$<span
                                            class="sizePrice">{{ @$pvariant->productVariant->product_price }}</span></span>
                            </div>
                            <!-- End .price-box -->
                            <div class="product-desc">
                            @foreach($msg as $msgs)
                                <ul>
                                    <li>{{$msgs->title}}
                                    </li>
                                    
                                </ul>
                                @endforeach
                            </div>
                            <!-- End .product-desc -->

                        </div>
                        <!-- End .product-single-details -->
                    </div>
                    <!-- End .col-lg-5 -->
                    <div class="product-action mt-3">
                        <div class="product_price">
                            <span>MXN$<span
                                        class="sizePrice">{{ @$pvariant->productVariant->product_price }}</span></span>
                                        @foreach($msg as $msgs)
                            <span>{{$msgs->title}}</span>
                            @endforeach
                        </div>
                        <!-- End .product-single-qty -->
                        <a class="paction add-cart addtocart" title="Add to Cart" data-url="{{ route('add.cart') }}"
                           data-pid="{{ $pvariant->id }}">
                            <span><img class="crt" src="{{ asset('assets/img/inferior.png') }}"/></span>
                            <span>carrito</span>
                        </a>
                        <a href="{{ route('cart.list') }}" class="paction add-compare" title="Add to compare">
                            <span>comprar</span>
                        </a>
                    </div>
                    <!-- End .product-action -->
                </div>
                <!-- End .row -->
            </div>

            <!-- End .product-single-container -->
            <div class="product-single-tabs row">
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="product-tab-desc" data-toggle="tab" href="#product-desc-content"
                           role="tab" aria-controls="product-desc-content" aria-selected="true">Description</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="product-tab-spec" data-toggle="tab" href="#product-spec-content"
                           role="tab" aria-controls="product-spec-content" aria-selected="false">specification</a>
                    </li>
                </ul>
                <div class="tab-content">

                    <div class="tab-pane fade show active" id="product-desc-content" role="tabpanel"
                         aria-labelledby="product-tab-desc">
                        <div class="product-desc-content">
                            <?php echo $pvariant->description;?>
                            @foreach($productImage as $pimage)
                                {{--                                    <div class="img">--}}
                                <img src="{{URL::asset('storage/images/'. $pimage->image) }}" class="img-fluid">
                                {{--                                    </div>--}}
                            @endforeach
                        </div>
                        <!-- End .product-desc-content -->
                    </div>
                    <!-- End .tab-pane -->
                    <div class="tab-pane fade" id="product-spec-content" role="tabpanel"
                         aria-labelledby="product-tab-spec">
                        <div class="product-spec-content">
                            <?php echo $pvariant->specification;?>
                        </div>
                        <!-- End .product-spec-content -->
                    </div>
                    <!-- End .tab-pane -->
                </div>
                <!-- End .tab-content -->
            </div>
            <!-- End .product-single-tabs -->
        </div>
    </div>
</div>


<!-- popup -->
<div class="overlayer" style="display: none;">
    <div class="popup">
        <div class="login">
            <section>
                <div class="header-middle">
                    <div class="container">
                        <div class="row align-items-center">
                            <div class="col-4">
                                <div class="header-left">
                                    <i class='fas fa-times ml-2 ' id="hide" style="font-size: 20px;"></i>
                                </div>
                            </div>
                            <div class="col-8">
                                <a href="{{ url('/') }}" class="logo">
                                    <img src="{{ asset('assets/img/logo.png') }}" alt="Logo">
                                </a>
                            </div>
                        </div>

                        <!-- End .header-left -->

                        <!-- End .header-right -->
                    </div>
                    <!-- End .container-fluid -->
                </div>
                <!-- End .header-middle -->
            </section>
            <div class="container-fluid mt-3">

                <div class="row">
                    <div class="col-12">
                        <ul class="nav nav-tabs tab_1">
                            <li class="active"><a data-toggle="tab" href="#home">INGRESAR</a></li>
                            <li><a data-toggle="tab" href="#menu1">REGISTRARSE</a></li>
                        </ul>

                        <div class="tab-content">
                            <div id="home" class="tab-pane in active">
                                <form class="form">
                                    <div class="row">
                                        <div class="col-12">
                                            <input type="text" class="form-control mb-2 mr-sm-2"
                                                   id="inlineFormInputName2" placeholder="Correo electronico">
                                        </div>
                                        <div class="col-12">
                                            <div class="input-group mb-2 mr-sm-2">
                                                <input type="text" class="form-control"
                                                       id="inlineFormInputGroupUsername2" placeholder="Contrasena">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row my-3">
                                        <div class="col-12 text-center">
                                            <a href="#" class="orange">Iniciar sesion</a>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div id="menu1" class="tab-pane">
                                <form class="form">
                                    <div class="row">
                                        <div class="col-12">
                                            <input type="text" class="form-control mb-2 mr-sm-2"
                                                   id="inlineFormInputName2" placeholder="Correo electronico">
                                        </div>
                                        <div class="col-12">
                                            <div class="input-group mb-2 mr-sm-2">
                                                <input type="text" class="form-control"
                                                       id="inlineFormInputGroupUsername2" placeholder="Contrasena">
                                            </div>
                                        </div>

                                    </div>
                                    <div class="row my-3">
                                        <div class="col-12 text-center">
                                            <a href="#" class="orange">REGISTRARSE</a>
                                        </div>
                                    </div>
                                </form>
                            </div>

                        </div>

                        <div class="row my-3">
                            <div class="col-12 text-center line">
                                <p><span class="pull-left"></span>o<span class="pull-right"></span></p>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="col-12 text-center">
                                <a href="#" class="facebook"><i class="fab fa-facebook-square"></i> Iniciar sesion con
                                    Facebook</a>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="col-12 text-center">
                                <a href="#" class="gmail">
                                    <img src="assets/img/gmail.png" width="30px" style="margin-top: -6px;">
                                    <span>Iniciar sesion con Gmail</span>
                                </a>
                            </div>
                        </div>
                        <div class="row my-3">
                            <div class="col-12 text-center grey">
                                <p>AI inciar sesion usted acepta <br> los <span class="red"><a href="#">treminos y condiciones y politicas de privacidad</a></span>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>
<!-- popup -->
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/popper.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/js/number.js') }}"></script>
<script src="{{ asset('assets/js/toastr.min.js') }}"></script>
<script>
    $(document).ready(function () {
        scrollerData();
    });
</script>

<script>
    var scroller = true;
    $(document).ready(function () {
        var i = 2;
        $(window).scroll(function () {
            if ($(window).scrollTop() >= $('.scrolldown').offset().top + $('.scrolldown').outerHeight() - window.innerHeight) {
// ajax call get data from server and append to the div
// if (scroller) {
                var rout = '{!! Route::currentRouteName() !!}';
                var lastId = $('#lastId').val();
                var previousID = $('#previousID').val();
// alert(rout);
                if (lastId) {
                    if (previousID != lastId) {
                        $('#previousID').val(lastId);
                        $.ajax(
                            {
                                url: '{{ route('get.more.data.category') }}',
                                type: 'GET',
                                data: {
                                    "page": i,
                                    'currentriute': rout,
                                    'lastId': lastId
                                },
                                success: function (result) {
                                    if (result.success) {
// $('.productslisting').html();

// toastr.success('Cart updated successfully');
                                        $('.productslisting').append(result.data);
                                        $('#lastId').val(result.lastProductId);

                                    } else {
                                        return false;
                                    }
                                }
                            });
                        i = parseInt(i) + 1;
                    }
                } else {
                    return false;
                }
            } else {
                return false;
            }
// }
        });
    });
    // function scrollerupdate() {
    //     scroller = false;
    // }
</script>
<script>
    $('.image_data').off('click').on('click', function (e) {
        var _that = $(this);
        var _url = _that.attr('data-url');
        var imageid = _that.attr('data-imageid');
        $.ajax(
            {
                url: _url,
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    "imageid": imageid,
                },
                success: function (result) {
                    if (result.success) {
                        $('.replace_image').html('');
                        // var mapData = JSON.parse(result.data);
                        $('.replace_image').html(result.data);
                    } else {
                        return false;
                    }
                }
            });
    })

    $('.video_data').off('click').on('click', function (e) {
        var _that = $(this);
        var _url = _that.attr('data-url');
        var videoid = _that.attr('data-videoid');
        $.ajax(
            {
                url: _url,
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    "videoid": videoid,
                },
                success: function (result) {
                    if (result.success) {
                        $('.replace_image').html('');
                        // var mapData = JSON.parse(result.data);
                        $('.replace_image').html(result.data);
                    } else {
                        return false;
                    }
                }
            });
    })

    $('.product_colors').off('change').on('change', function (e) {
        var _that = $(this);
        var _url = _that.attr('data-url');
        var productId = _that.attr('data-productId');
        var color = _that.val();
        $.ajax(
            {
                url: _url,
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    "color": color,
                    "productId": productId,
                },
                success: function (result) {
                    if (result.success) {
                        $('.color_size').html('');
                        // var mapData = JSON.parse(result.data);
                        $('.color_size').html(result.data);
                    } else {
                        return false;
                    }
                }
            });
    })

    $('.addtocart').off('click').on('click', function (e) {
        var _that = $(this);
        var _url = _that.attr('data-url');
        var productId = _that.attr('data-pid');
        var colorid = $('.product_colors').val();
        var colorsize = $('.color_size').val();
        var quanti = $('#quantity').val();
        if (colorid == 'Selecciona') {
            toastr.error('Please select color');
            return false;
        }
        if (colorsize == 'Select talla' || colorsize == 'Select size' || colorsize == 'Selecciona') {
            toastr.error('Please select size');
            return false;
        }
        $.ajax(
            {
                url: _url,
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    "colorId": colorid,
                    "colorSize": colorsize,
                    "quanti": quanti,
                    "productId": productId,
                },
                success: function (result) {
                    if (result.success) {
                        updateCart();
                        toastr.success('Cart add successfully');
                        // $('.size_details').html('');
                        // // var mapData = JSON.parse(result.data);
                        // $('.size_details').html(result.data);
                    } else {
                        toastr.error('You can add only ' + result.quantity + ' quantity for this .');
                        return false;
                    }
                }
            });
    });
    $('.color_size').off('change').on('change', function () {
        var size = $(this).val();
        var _url = $(this).attr('data-url');
        var productId = $(this).attr('data-productId');
        var colorid = $('.product_colors').val();
        $.ajax(
            {
                url: _url,
                type: 'POST',
                data: {
                    _token: "{{ csrf_token() }}",
                    "colorid": colorid,
                    "productId": productId,
                    "size": size,
                },
                success: function (result) {
                    if (result.success) {
                        $('.sizePrice').html('');
                        // var mapData = JSON.parse(result.data);
                        $('.sizePrice').html(result.data);
                    } else {
                        return false;
                    }
                }
            });

    })
</script>
</body>
</html>