<option disabled selected>Select Branch</option>
@foreach($branches as $branch)
    @if($branch->id == 6)
    <option value="{{ $branch->id }}" {{ Session::get('branch_id') == $branch->id ? 'selected' :'' }} data-url="{{ route('get.branch.product',$branch->name) }}">{{ $branch->name }}</option>
    @endif
@endforeach