<!DOCTYPE html>
<html dir="ltr" lang="en">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <!-- Tell the browser to be responsive to screen width -->
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content="wrappixel, admin dashboard, html css dashboard, web dashboard, bootstrap 4 admin, bootstrap 4, css3 dashboard, bootstrap 4 dashboard, xtreme admin bootstrap 4 dashboard, frontend, responsive bootstrap 4 admin template, material design, material dashboard bootstrap 4 dashboard template">
      <meta name="description" content="Xtreme is powerful and clean admin dashboard template, inpired from Google's Material Design">
      <meta name="robots" content="noindex,nofollow">
      <title>Xtreme Admin Template by WrapPixel</title>
      <link rel="canonical" href="https://www.wrappixel.com/templates/xtremeadmin/" />
      <!-- Favicon icon -->
      <link rel="icon" type="image/png" sizes="16x16" href="../../assets/images/favicon.png">
      <!-- Custom CSS -->
      <link href="../../assets/extra-libs/c3/c3.min.css" rel="stylesheet">
      <!-- Custom CSS -->
      <link href="../../dist/css/style.min.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="{{ asset('css/toastr.css') }}">
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.min.css">

      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
      <!-- ============================================================== -->
      <!-- Preloader - style you can find in spinners.css -->
      <!-- ============================================================== -->
      <div class="preloader">
         <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
         </div>
      </div>
      <!-- ============================================================== -->
      <!-- Main wrapper - style you can find in pages.scss -->
      <!-- ============================================================== -->
      <div id="main-wrapper">
         @yield('content')
      </div>
      <!-- ============================================================== -->
      <!-- End Wrapper -->
      <!-- ============================================================== -->
      <!-- ============================================================== -->
      <!-- customizer Panel -->
      <!-- ============================================================== -->
      @include('layouts.customiser')
      <div class="chat-windows"></div>
      <!-- ============================================================== -->
      <!-- All Jquery -->
      <!-- ============================================================== -->
      <script src="{{asset('assets/libs/jquery/dist/jquery.min.js')}}"></script>
      <!-- Bootstrap tether Core JavaScript -->
      <script src="{{asset('assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
      <script src="{{asset('assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
      <!-- apps -->
      <script src="{{asset('dist/js/app.min.js')}}"></script>
      <script src="{{asset('dist/js/app.init.boxed.js')}}"></script>
      <script src="{{asset('dist/js/app-style-switcher.js')}}"></script>
      <!-- slimscrollbar scrollbar JavaScript -->
      <script src="{{asset('assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>
      <script src="{{asset('assets/extra-libs/sparkline/sparkline.js')}}"></script>
      <!--Wave Effects -->
      <script src="{{asset('dist/js/waves.js')}}"></script>
      <!--Menu sidebar -->
      <script src="{{asset('dist/js/sidebarmenu.js')}}"></script>
      <!--Custom JavaScript -->
      <script src="{{asset('dist/js/feather.min.js')}}"></script>
      <script src="{{asset('dist/js/custom.min.js')}}"></script>
      <!--This page JavaScript -->
      <!--Morris JavaScript -->
      <!--c3 charts -->
      <script src="{{asset('assets/extra-libs/c3/d3.min.js')}}"></script>
      <script src="{{asset('assets/extra-libs/c3/c3.min.js')}}"></script>
      <script src="{{asset('dist/js/pages/da        shboards/dashboard5.js')}}"></script>
      <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
      <script src="{{ asset('assets/js/toastr.min.js') }}"></script>

   </body>
</html>