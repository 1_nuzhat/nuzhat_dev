@extends('layouts.master')
@section('content')
@include('layouts.header')
@include('layouts.sidebar')


<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<a class="btn waves-effect waves-light btn-secondary" style="
margin-left: 776px;" href="banner/create">Add Banner</a>

<form method="get" action="{{url('banner')}}">



<div class="page-breadcrumb">
<div class="row">
<div class="col-5 align-self-center">
    <h4 class="page-title">Product List</h4>
    <div class="d-flex align-items-center">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page">Library</li>
            </ol>
        </nav>
    </div>
</div>
</div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->

<!-- Container fluid  -->
<!-- ============================================================== -->


<div class="container-fluid">
<!-- ============================================================== -->
<!-- Start Page Content -->
<!-- ============================================================== -->

<div class="row">
<!-- Column -->
<div class="col-lg-12">
    <div class="card">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table product-overview" id="datatable">
                    <thead>
                <tr>
                   
                    <th>NAME</th>
                    <th>DESCRIPTION</th>
                    <th>IMAGES</th>
                    <th>STATUS</th>
                 <th>ACTION</th>
                </tr>
                    </thead>
                    <tbody>
                        @foreach($banner as $banners)
                      
                        <tr>
                          
                            <!-- <td>
                                @if(!empty($product->productVariant))<img src="{{URL::asset('storage/images/'. @$product->productVariant->productImage['images']) }}" width="100" height="auto">
                                @else
                                {{__('N/A')}}
                                 @endif
                                
                            </td> -->

                            <td>{{$banners->name}}</td>
                            <td>{!! $banners->description !!}</td>
                            <td>
                                @if(!empty($banners->images))<img src="{{URL::asset('storage/images/'. $banners->images) }}" width="100" height="auto">
                                @else
                                {{__('N/A')}}
                                 @endif
                                
                            </td>
                            <td> @if($banners->status ==1)
                                Active
                            @else
                                In-active
                            @endif 
                            </td>
                           
                                 <td>
                                    @if(!$banners->deleted_at)<a href="banner/{{$banners->id}}/edit" class="btn btn-success" data-toggle="tooltip" title="edit" >
                            <i class="fa fa-edit"></i>
                        </a>
                                     <span class="btn btn-danger trashBanner" data-url="{{ route('delete.banner') }}" data-bannerid="{{ $banners->id }}" data-type="delete" data-toggle="tooltip" title="delete" >
                                         <i class="fa fa-trash"></i>
                                     </span>
                                        @else
                                         <span class="btn btn-secondary trashBanner" data-url="{{ route('delete.banner') }}" data-bannerid="{{ $banners->id }}" data-type="restore" data-toggle="tooltip" title="restore" >
                                         Restore
                                     </span>
                                     @endif
                                 </td>
                        
                        @endforeach
                      </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- Column -->
</div>
</div> 
</form>
<!-- ============================================================== -->
<!-- End Container fluid  -->
</div>


@include('layouts.footer')
{{--<script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>--}}
{{--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>--}}

<script>
$(document).ready(function() {
$('#datatable').DataTable({"bLengthChange" : false});
});
$('.trashBanner').off('click').on('click',function () {
var _that = $(this);
var _url = _that.attr('data-url');
var bannerId = _that.attr('data-bannerid');
var type = _that.attr('data-type');

if(confirm("Are you sure you want to "+ type+ " this banner ?"))
{
$.ajax({
    url: _url,
    type: 'POST',
    data: {_token: "{{ csrf_token() }}" ,bannerId:bannerId, type :type},
    success: function (data) {
        if (data['success']) {
            window.location.reload(true);

        }
    },

});
}
})
</script>
@endsection

