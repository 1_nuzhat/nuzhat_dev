@extends('layouts.master')
@section('content')
@include('layouts.header')
@include('layouts.sidebar')
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
   <div class="row">
      <div class="col-5 align-self-center">
         <h4 class="page-title">Dashboard</h4>
         <div class="d-flex align-items-center">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Library</li>
               </ol>
            </nav>
         </div>
      </div>
      <div class="col-7 align-self-center">
         <div class="d-flex no-block justify-content-end align-items-center">
            <div class="mr-2">
               <div class="lastmonth"></div>
            </div>
            <div class="">
               <small>LAST MONTH</small>
               <h4 class="text-info mb-0 font-medium">$58,256</h4>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
   <!-- ============================================================== -->
   <!-- Earnings -->
   <!-- ============================================================== -->
   <div class="row">
      <!-- Column -->
      <div class="col-sm-12 col-lg-4">
         <div class="card">
            <div class="card-body">
               <h4 class="card-title">GANGABOX SALES TODAY</h4>
               <h2 class="font-medium">${{$price}}</h2>
               <h5 class="card-subtitle">{{$qty}} Orders</h5>
            </div>
            
         </div>
      </div>
      <!-- Column -->
    <div class="col-sm-12 col-lg-4">
         <div class="card">
            <div class="card-body">
               <h4 class="card-title">MONTERREY SALES TODAY</h4>
                <h2 class="font-medium">${{$mont_price}}</h2>
                <h5 class="card-subtitle">{{$mont_qty}} Orders</h5>
              
            </div>
            
         </div>
      </div>
      <div class="col-sm-12 col-lg-4">
         <div class="card">
            <div class="card-body">
               <h4 class="card-title">PUEBLA SALES TODAY</h4>
               <h2 class="font-medium">${{$puebla_price}}</h2>
               <h5 class="card-subtitle">{{$puebla_qty}} Orders</h5>
               
            </div>
            
         </div>
      </div>
   </div>
   <div class="row">
      <!-- Column -->
      <div class="col-sm-12 col-lg-4">
         <div class="card">
            <div class="card-body">
               <h4 class="card-title">GUADALAZARA SALES TODAY</h4>
                 <h2 class="font-medium">${{$guadalazara_price}}</h2>
               <h5 class="card-subtitle">{{$guadalazara_qty}} Orders</h5>
            </div>
            
         </div>
      </div>
      <!-- Column -->
    <div class="col-sm-12 col-lg-4">
         <div class="card">
            <div class="card-body">
               <h4 class="card-title">CDMX SALES TODAY</h4>
               <h2 class="font-medium">${{$cdmx_price}}</h2>
               <h5 class="card-subtitle">{{$cdmx_qty}} Orders</h5>
            </div>
            
         </div>
      </div>
      <div class="col-sm-12 col-lg-4">
         <div class="card">
            <div class="card-body">
               <h4 class="card-title">TODAY AVERAGE TICKETS</h4>
               
               <h2 class="font-medium">N/A</h2>
            </div>
            
         </div>
      </div>
   </div>
   <div class="row">
      <!-- Column -->
      <div class="col-sm-12 col-lg-3">
         <div class="card">
            <div class="card-body">
               <h4 class="card-title">SESSIONS</h4>
               <h2 class="font-medium"></h2>
            </div>
            
         </div>
      </div>
      <!-- Column -->
    <div class="col-sm-12 col-lg-3">
         <div class="card">
            <div class="card-body">
               <h4 class="card-title">APP DOWNLOADS</h4>
               <h2 class="font-medium">N/A</h2>
            </div>
            
         </div>
      </div>
      <div class="col-sm-12 col-lg-3">
         <div class="card">
            <div class="card-body">
               <h4 class="card-title">CONVERSION RATE</h4>
               
               <h2 class="font-medium">N/A</h2>
            </div>
            
         </div>
      </div>
      <div class="col-sm-12 col-lg-3">
         <div class="card">
            <div class="card-body">
               <h4 class="card-title">LIVE SESSIONS</h4>
               
               <h2 class="font-medium"></h2>
            </div>
            
         </div>
      </div>
   </div>
   <!-- ============================================================== -->
   <!-- Product Sales -->
   <!-- ============================================================== -->

   <!-- ============================================================== -->
   <!-- Orders -->
   <!-- ============================================================== -->
   <div class="row">
      <div class="col-sm-12 col-lg-8">
         <div class="card">
            <div class="card-body">
               <!-- title -->
               <div class="d-md-flex align-items-center">
                  <div>
                     <h4 class="card-title">LATEST SALES TOP 10 PRODUCTS</h4>
                  </div>
                
               </div>
               <!-- title -->
               <div class="table-responsive scrollable mt-2" style="height:400px;">
                  <table class="table v-middle">
                     <tbody>
                          @foreach($latest_product as $latest_products)
                        <tr>
                          
                           <td>
                             
                              <div class="d-flex align-items-center">
                                 <div class="mr-2"><img src="{{URL::asset('storage/images/'. $latest_products->images) }}" alt="user" class="circle" width="45" /></div>
                                 <div class="">
                                    <h5 class="mb-0 font-16 font-medium">{{$latest_products->product_sku}}</h5>
                                 </div>
                              </div>
                            
                           </td>
                            
                        </tr>
                         @endforeach
                      
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
      <!-- <div class="col-sm-12 col-lg-4">
         <div class="card">
            <div class="card-body border-bottom">
               <h4 class="card-title">Order Stats</h4>
               <h5 class="card-subtitle">Overview of orders</h5>
               <div class="status mt-4" style="height:280px; width:100%"></div>
            </div>
            <div class="card-body">
               <div class="row">
                  <div class="col-4">
                     <i class="fa fa-circle text-primary"></i>
                     <h3 class="mb-0 font-medium">5489</h3>
                     <span>Success</span>
                  </div>
                  <div class="col-4">
                     <i class="fa fa-circle text-info"></i>
                     <h3 class="mb-0 font-medium">954</h3>
                     <span>Pending</span>
                  </div>
                  <div class="col-4">
                     <i class="fa fa-circle text-orange"></i>
                     <h3 class="mb-0 font-medium">736</h3>
                     <span>Failed</span>
                  </div>
               </div>
            </div>
         </div>
      </div> -->
   </div>
   <!-- ============================================================== -->
   <!-- Review -->
   <!-- ============================================================== -->
   
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
@include('layouts.footer')
@endsection