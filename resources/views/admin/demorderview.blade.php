@extends('layouts.master')
@section('content')
@include('layouts.header')
@include('layouts.sidebar')
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<style>
          p {
              margin: 0;
          }
          .box_1_txt {
            width: 50px;
    height: 50px;
    border: 1px solid #b7b4b4;
    border-radius: 5px;
          }
          .txt_2 {
            background: #e1e1e1;
    position: absolute;
    padding: 0px 8px;
    border-radius: 50%;
    top: -10px;
    right: -10px;
          }
          .img_1 {
            position: relative;
    display: inline-block;
          }
          .divider {
            background: #e1e1e1;
    width: 100%;
    height: 2px;
    margin-top: 20px;
          }
       </style>
<div class="page-wrapper">
   <!-- ============================================================== -->
   <!-- ============================================================== -->
   <!-- Bread crumb and right sidebar toggle -->
   <!-- ============================================================== -->
   <div class="row">
      <!-- Column -->
      <div class="col-lg-12">
         <div class="card">
            <div class="card-body">
            </div>
         </div>
      </div>
   </div>
   <div class="page-breadcrumb">
      <div class="row">
         <div class="col-5 align-self-center">
           <!--  <h4 class="page-title">View Order</h4> -->
            <div class="d-flex align-items-center">
               <nav aria-label="breadcrumb">
                  <!-- <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="#">Home</a></li>
                     <li class="breadcrumb-item active" aria-current="page">Library</li>
                  </ol> -->
               </nav>
            </div>
         </div>
      </div>
   </div>
   <!-- ============================================================== -->
   <!-- End Bread crumb and right sidebar toggle -->
   <!-- Container fluid  -->
   <!-- ============================================================== -->
    <div class="container">
          <div class="row">
              <div class="col-lg-12">
                 <div class="card">
               <div class="card-body">
                <div class="form">
                    <div class="form-group ">
                      <h3 class="card-title">{{$order_number->order_number}}
                  </h3>
                  <p>
                     {{date('F d, Y h:i A', strtotime($order_number->order_date)) }}
                  </p>
                    
                    <!-- <h4>No preparado</h4> -->
                  @foreach($orders as $order)
                   @if($order->orderDetails)
                    @foreach($order->orderDetails->product as $value) 
                       
                       <div class="row mt-4">
                          <div class="col-2">
                              <div class="img_1">
                                <div class="box_1_txt">
                                    @if(!empty($value->ProductVariant))
                                        <img src="{{URL::asset('storage/images/'. $value->ProductVariant->productImage->images)}}" width="50" height="auto">
                                        @endif
                                </div>
                                <p class="txt_2">{{$order->orderDetails['quantity']}}</p>
                              </div>
                             
                          </div>
                          <div class="col-6">
                             <p>{{$value['name']}}</p>
                             <p>SKU : {{$order->orderDetails['product_sku']}}</p>
                          </div>
                          <div class="col-2">
                              <p>${{$order->orderDetails['price']}}.00 x {{$order->orderDetails['quantity']}}</p>
                          </div>
                          <div class="col-2 text-right ">
                             <p>${{$order->orderDetails['quantity'] * $order->orderDetails['price']}}.00</p>
                          </div>
                       </div>
                     @endforeach
                    @endif
                  @endforeach
                    </div>
                  
                 </div>
              </div>
            
          </div>
      </div>
    </div>
     
          <div class="row">
              <div class="col-lg-12">
                <div class="card">
               <div class="card-body">
                <div class="form">
                    <div class="form-group ">
                     <!--   <h4>Pendiente</h4> -->
                       
                       
                       <div class="row mt-4">
                          <div class="col-2">
                             <p>Subtotal</p>
                          </div>
                          <div class="col-6">
                             <!-- <p>{{$order->orderDetails['quantity'] + $order->orderDetails['quantity']}} quantity</p> -->
                          </div>
                          <div class="col-4 text-right ">
                             <p>${{$subTotal}}.00</p>
                          </div>
                       </div>
                       <div class="row">
                          <div class="col-2">
                             <p>Shipments</p>
                          </div>
                          <div class="col-6">
                             <p>Delivery for Saturday during the day</p>
                          </div>
                          <div class="col-4 text-right ">
                             <p> ${{$orderShipping}}.00</p>
                          </div>
                       </div>
                       <!-- <div class="row">
                          <div class="col-2">
                             <p>Impuesto</p>
                          </div>
                          <div class="col-6">
                             <p>VAT 16%</p>
                          </div>
                          <div class="col-4 text-right ">
                             <p>$25.66</p>
                          </div>
                       </div> -->
                       <div class="row">
                          <div class="col-8">
                             <p><b>Total</b></p>
                          </div>
                          <div class="col-4 text-right ">
                             <p><b> ${{$orderTotal}}.00</b></p>
                          </div>
                       </div>
                       <!-- <div class="border"></div>
                        <div class="border"></div> -->
                       
                    </div>
                 </div>
              </div>
            
          </div>
      </div>
    </div>
    <div class="row">
              <div class="col-lg-12">
                <div class="card">
               <div class="card-body">
                <div class="form">
                    <div class="form-group ">
                     <!--   <h4>Pendiente</h4> -->
                       
                       
                       <div class="row mt-4">
                          <div class="col-2">
                             <p>Email Id</p>
                          </div>
                          <div class="col-6">
                             <p>  {{$orderAddress['email']}}</p>
                          </div>
                          
                       </div>
                       <div class="row">
                          <div class="col-2">
                             <p>Address</p>
                          </div>
                          <div class="col-6">
                             <p>{{$orderAddress['exteriorNumber']}}
                              {{$orderAddress['streetNumber']}}
                              {{$orderAddress['postalCode']}}<br/>
                              {{$orderAddress['state']}}</p>
                          </div>
                         
                       </div>
                       <div class="row">
                          <div class="col-2">
                             <p>Contact Number</p>
                          </div>
                          <div class="col-6">
                             <p>
                              {{$orderAddress['mobile']}}</p>
                          </div>
                         
                       </div>
                       
                    </div>
                 </div>
              </div>
            
          </div>
      </div>
    </div>
       
    </div>
   <!-- ============================================================== -->
   <!-- End Container fluid  -->
</div>
@include('layouts.footer')
@endsection