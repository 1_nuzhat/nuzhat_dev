@extends('layouts.master')
@section('content')
@include('layouts.header')
@include('layouts.sidebar')


<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
    <a class="btn waves-effect waves-light btn-secondary" style="
    margin-left: 776px;" href="payment_details/create">Add</a>
               
         <form method="get" action="{{url('payment_details')}}">
         
   
    
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-5 align-self-center">
                    <h4 class="page-title">PAYMENT DETAIL LIST</h4>
                    <div class="d-flex align-items-center">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Library</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->

        <!-- Container fluid  -->
        <!-- ============================================================== -->
      
           
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            
            <div class="row">
                <!-- Column -->
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table product-overview" id="zero_config">
                                    <thead>
                                <tr>
                                    <th>ONLINE TITLE</th>
                                    <th>ONLINE DESCRIPTION</th>
                                    <th>COD TITLE</th>
                                    <th>ACTION</th>
                                </tr>
                                    </thead>
                                    <tbody>
                                       @foreach($payment_detail as $payment_details)
                                      
                                        <tr>
                                            <td>{{$payment_details->online_pay_title}}</td>
                                            <td>{{$payment_details->online_pay_description}}</td>
                                            <td>{{$payment_details->cod_title}}</td>
                                              <td>
                                       
                                            <a href="payment_details/{{$payment_details->id}}/edit" class="btn btn-success" data-toggle="tooltip" title="edit" ><i class="fa fa-edit"></i>
                                        </a></td>
                                        </tr>
                                       @endforeach
                                      </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
            </div>
        </div> 
        </form>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
    </div>


@include('layouts.footer')
@endsection

