@extends('layouts.master')
@section('content')
@include('layouts.header')
@include('layouts.sidebar')

<style type="text/css">
   #user1 {
   padding: 15px;
   border: 1px solid #666;
   background: #fff;
   display: none;
   }
   #formButton {
   display: block;
   margin-right: auto;
   margin-left: auto;
   }

  .multiselect {
  width: 200px;
}

.selectBox {
  position: relative;
}

.selectBox select {
  width: 100%;
  font-weight: bold;
}

.overSelect {
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
}

#checkboxes {
  display: none;
  border: 1px #dadada solid;
}

#checkboxes label {
  display: block;
}

#checkboxes label:hover {
  background-color: #1e90ff;
}
</style>

<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
   <div class="row">
      <div class="col-5 align-self-center">
         <h4 class="page-title">ADD PAYMENT DETAILS</h4>
         <div class="d-flex align-items-center">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Library</li>
               </ol>
            </nav>
         </div>
      </div>
   </div>
   <!-- Container fluid  -->
   <!-- ============================================================== -->
   <div class="container-fluid">
      <!-- ============================================================== -->
      <!-- Start Page Content -->
      <!-- ============================================================== -->
      <!-- Row -->
      <div class="row">
         @if (session('status'))
         <div class="alert alert-success alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session('status') }}
         </div>
         @endif
         @if (session('success'))
         <div class="alert alert-success alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session('success') }}
         </div>
         @endif
         @if (session('error'))
         <div class="alert alert-danger alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session('error') }}
         </div>
         @endif   
         <div class="col-12">
            <div class="card">
               <form class="form-horizontal" action="{{url('/payment_details')}}" method="POST"  enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <div class="card-body">
                     <h4 class="card-title"> PAYMENT DETAILS
                     </h4>
                       <div class="form-group row">
                        <label for="name" class="col-sm-3 text-right control-label col-form-label">ONLINE TITLE:</label>
                        <div class="col-sm-9">
                           <input type="text" class="form-control" id="online_pay_title" name="online_pay_title" placeholder="title" >
                           @if($errors->has('online_pay_title'))
                           <span class="text-danger">{{ $errors->first('online_pay_title') }}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row">
                        <label for="name" class="col-sm-3 text-right control-label col-form-label">ONLINE DESCRIPTION:</label>
                        <div class="col-sm-9">
                           <input type="text" class="form-control" id="online_pay_description" name="online_pay_description" placeholder="description" >
                           @if($errors->has('online_pay_description'))
                           <span class="text-danger">{{ $errors->first('online_pay_description') }}</span>
                           @endif
                        </div>
                     </div>

                     <div class="form-group row">
                        <label for="name" class="col-sm-3 text-right control-label col-form-label">COD TITLE:</label>
                        <div class="col-sm-9">
                           <input type="text" class="form-control" id="cod_title" name="cod_title" placeholder="title" >
                           @if($errors->has('cod_title'))
                           <span class="text-danger">{{ $errors->first('cod_title') }}</span>
                           @endif
                        </div>
                     </div>
                     
                         <div class="card-body">
                            <div class="form-group mb-0 text-right">
                               <button type="submit" id="sub" class="btn btn-info waves-effect waves-light">Submit</button>
                            </div>
                         </div>
                 </form>
                </div>
               </div>
               </div>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- End Row -->
</div>  

     
@include('layouts.footer')
@endsection