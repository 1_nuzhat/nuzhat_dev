@extends('layouts.master')
@section('content')
@include('layouts.header')
@include('layouts.sidebar')
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
<!-- ============================================================== -->
<!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title"> EDIT MESSAGE</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
            </div>
           
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                <!-- Row -->
                <div class="row">
                     @if (session('status'))
                        <div class="alert alert-success alert-dismissable">
                           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           {{ session('status') }}
                        </div>
                    @endif

                    @if (session('success'))
                        <div class="alert alert-success alert-dismissable">
                           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           {{ session('success') }}
                        </div>
                    @endif

            @if (session('error'))
                <div class="alert alert-danger alert-dismissable">
                   <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                   {{ session('error') }}
                </div>
            @endif   
                    <div class="col-12">
                        <div class="card">
                            <form class="form-horizontal" action="/message/{{$message_edit->id}}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                {{ method_field('PUT') }}
                                <div class="card-body">
                                    <h4 class="card-title">EDIT MESSAGE</h4>
                                     
                                    
                                    <div class="form-group row">
                                        <label for="cat_name" class="col-sm-3 text-right control-label col-form-label">TITLE:</label>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control" value="{{$message_edit->title}}" name="title">
                                              @if($errors->has('message_edit'))
                    <span class="text-danger">{{ $errors->first('message_edit') }}</span>
                    @endif
                                        </div>
                                    </div>

                                   
                                <hr>   
                                <div class="card-body">
                                    <div class="form-group mb-0 text-right">
                                        <button type="submit" class="btn btn-info waves-effect waves-light">Save</button>
                                    </div>
                                </div>
                            </form>
                               
                        </div>
                    </div>
                </div>
                <!-- End Row -->
  
            </div>
            
@include('layouts.footer')
@endsection

