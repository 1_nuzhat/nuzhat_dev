@extends('layouts.master')
@section('content')
@include('layouts.header')
@include('layouts.sidebar')

<style type="text/css">
   #user1 {
   padding: 15px;
   border: 1px solid #666;
   background: #fff;
   display: none;
   }
   #formButton {
   display: block;
   margin-right: auto;
   margin-left: auto;
   }

  .multiselect {
  width: 200px;
}

.selectBox {
  position: relative;
}

.selectBox select {
  width: 100%;
  font-weight: bold;
}

.overSelect {
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
}

#checkboxes {
  display: none;
  border: 1px #dadada solid;
}

#checkboxes label {
  display: block;
}

#checkboxes label:hover {
  background-color: #1e90ff;
}
</style>

<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
   <div class="row">
      <div class="col-5 align-self-center">
         <h4 class="page-title">ADD PRODUCT</h4>
         <div class="d-flex align-items-center">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Library</li>
               </ol>
            </nav>
         </div>
      </div>
   </div>
   <!-- Container fluid  -->
   <!-- ============================================================== -->
   <div class="container-fluid">
      <!-- ============================================================== -->
      <!-- Start Page Content -->
      <!-- ============================================================== -->
      <!-- Row -->
      <div class="row">
         @if (session('status'))
         <div class="alert alert-success alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session('status') }}
         </div>
         @endif
         @if (session('success'))
         <div class="alert alert-success alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session('success') }}
         </div>
         @endif
         @if (session('error'))
         <div class="alert alert-danger alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session('error') }}
         </div>
         @endif   
         <div class="col-12">
            <div class="card">
               <form class="form-horizontal" action="{{url('/banner')}}" method="POST"  enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <div class="card-body">
                     <h4 class="card-title"> ADD BANNER
                     </h4>
                     
                     <div class="form-group row">
                        <label for="name" class="col-sm-3 text-right control-label col-form-label">NAME:</label>
                        <div class="col-sm-9">
                           <input type="text" class="form-control" id="name" name="name" placeholder="NAME" >
                           @if($errors->has('name'))
                           <span class="text-danger">{{ $errors->first('name') }}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row">
                        <label for="description" class="col-sm-3 text-right control-label col-form-label">DESCRIPTION:</label>
                        <div class="col-sm-9">
                           <textarea class="ckeditor form-control" name="description"></textarea>
                           @if($errors->has('description'))
                           <span class="text-danger">{{ $errors->first('description') }}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row">
                        <label for="product_id" class="col-sm-3 text-right control-label col-form-label">IMAGE:</label>
                        <div class="col-sm-9">
                           <input type="file" name="images" id="images"> 
                             @if($errors->has('images'))
                           <span class="text-danger">{{ $errors->first('images') }}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row">
                        <label for="" class="col-sm-3 text-right control-label col-form-label">STATUS:</label>
                        <div class="col-sm-9">
                           <select id="status" name="status" class="form-control @error('status') is-invalid @enderror">
                              <option disabled selected>--- Select Status---</option>
                              <option value="1">Yes</option>
                              <option value="0">No</option>       
                           </select>
                           @if($errors->has('status'))
                           <span class="text-danger">{{ $errors->first('status') }}</span>
                           @endif
                        </div>
                     </div>
                    
                    
                         <div class="card-body">
                            <div class="form-group mb-0 text-right">
                               <button type="submit" id="sub" class="btn btn-info waves-effect waves-light">Submit</button>
                            </div>
                         </div>
                 </form>
                </div>
               </div>
               </div>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- End Row -->
</div>  

     
@include('layouts.footer')

<script src="{{ asset('ckeditor/ckeditor.js')}}"></script>
<script>CKEDITOR.replace('article-ckeditor');</script>
@endsection