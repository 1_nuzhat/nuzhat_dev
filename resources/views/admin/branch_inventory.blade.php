@extends('layouts.master')
@section('content')
    @include('layouts.header')
    @include('layouts.sidebar')


    <!-- ============================================================== -->
    <!-- Page wrapper  -->
    <!-- ============================================================== -->
    <div class="page-wrapper">
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <!--  <div class="row"> -->
        <!-- Column -->
    <!--  <div class="col-lg-6">
            <div class="card">
                <div class="card-body">

        <button class="btn waves-effect waves-light btn-success active_all" data-url="{{ url('/activeAll') }}">Active</button>
        <button class="btn waves-effect waves-light btn-danger inactive_all" data-url="{{ url('/inactiveAll') }}">In-Active</button>
    </div>
    </div>
    </div> -->
        <!-- /div> -->
    <!--  <form method="get" action="{{url('product_tag')}}"> -->
        <div class="row">
            <!-- Column -->
            <!-- <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                       
                        <br/>

                        <a class="btn waves-effect waves-light btn-secondary" href="{{ route('invorder') }}">Export Inventory</a>



                    </div>
                </div>
            </div> -->
        </div>
        <div class="row">
            @if (session('status'))
                <div class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ session('status') }}
                </div>
            @endif
        </div>


        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-5 align-self-center">
                    <h4 class="page-title">Inventory List</h4>
                    <div class="d-flex align-items-center">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Library</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->

        <!-- Container fluid  -->
        <!-- ============================================================== -->


        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->

            <div class="row">
                <!-- Column -->
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table product-overview" id="datatable">
                                    <thead>
                                    <tr>
                                        <!-- <th>
                                            <input type="checkbox" id="CheckAll" >
                                        </th> -->
                                        <th>IMAGE</th>
                                        <th>NAME</th>
                                        <th>STATUS</th>
                                        <th>INVENTORY</th>
                                        <th>PRODUCT SKU</th>
                                        <th>GSALES QTY</th>
                                        <th>GSALES$</th>
                                        <th>SALE DATE</th>
                                        <th>INITIAL STOCK</th>
                                        <th>ROTATION</th>
                                        <th>IN TRANSIT</th>
                                        <th>OUT OF STOCK DATE</th>
                                        <th>ORDER ON</th>
                                    </tr>
                                    </thead>
                                    <tbody id="inventoryData">
                                    @foreach($product_variant as $product_variants)
                                        <?php $transit = \App\Http\Controllers\InventoryController::productTransist($product_variants->variant_id) ?>
                                        <tr>
                                            <!-- <th><input type="checkbox" class="row_check" name="row_check[]" value=""></th> -->
                                            <td>
                                                @if(!empty($product_variants))<img src="{{URL::asset('storage/images/'. $product_variants->images) }}" width="100" height="auto">
                                                @else
                                                    {{__('N/A')}}
                                                @endif
                                            </td>

                                            <td>
                                                {{$product_variants->name}}
                                            </td>

                                            <td>
                                                @if($product_variants->p_status ==1)
                                                    Active
                                                @else
                                                    In-active
                                                @endif
                                            </td>

                                            <td>
                                                {{$product_variants->in_stock}}
                                            </td>

                                            <td>
                                                {{!empty($product_variants->product_sku) && $product_variants->product_sku ? $product_variants->product_sku : 'N/A'}}
                                            </td>


                                            <td> {{$product_variants->sale_qty}} </td>
                                            <td> ${{$product_variants->sale_cost}} </td>
                                            <td>
                                                {{$product_variants->sale_date}}
                                            </td>
                                        <!-- td>
                                        {{!empty($product_variants->quantity) && $product_variants->quantity ? $product_variants->quantity : 'N/A'}}
                                                </td> -->
                                            <td>
                                                {{$product_variants->initial_stock}}
                                            </td>
                                            <td>  </td>
                                            <td>  {{ $transit > 0 ? $transit :'N/A' }}  </td>
                                            <td> @if($product_variants->quantity < 1) {{ date('d-M-Y',strtotime($product_variants->outofstock)) }} @else Available @endif </td>
                                            <td> {{$product_variants->sale_date}}  </td>

                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
            </div>
        </div>
        <!--  </form> -->
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
    </div>
{{--    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>--}}
    <script>
        $(document).ready(function() {
            $('#datatable').DataTable({"bLengthChange" : false});
        });
        $('#check_all').change(function(){
            $('.row_check').prop("checked", $(this).prop("checked"));
        });
    </script>
    <script>
        function validateForm() {
            var x = document.forms["myForm"]["file"].value;
            if (x == "") {
                alert("Please Choose file");
                return false;
            }
        }
    </script>
    @include('layouts.footer')
@endsection

