@extends('layouts.master')
@section('content')
@include('layouts.header')
@include('layouts.sidebar')



<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
<!-- ============================================================== -->
<!-- ============================================================== -->
 <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
          
           
            <div class="row">
                    <!-- Column -->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">

                    </div>
                </div>
            </div>
        </div>

       
        
        
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->

            <!-- Container fluid  -->
            <!-- ============================================================== -->

           <div class="row">
                <!-- Column -->
              <div class="col-lg-12">
                <div class="card">
                  <div class="card-body">

                
               <!--  <button class="btn waves-effect waves-light btn-danger" id="t_id">{{__('Remove tag')}}</button> -->

                    </div>
                </div>
            </div>
        </div>
         <input type="button" value="PRINT" onclick="PrintDiv();" />
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                
                <div class="row">
                    <!-- Column -->
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body">
                                <div class="table-responsive" id="divToPrint">
                                    <table class="table product-overview" id="zero_config">
                                        <thead>
                                    <tr>
                                        
                                        <th>ORDER NUMBER</th>
                                        <th>DATE</th>
                                        <th>CLIENT EMAIL</th>
                                        <th>TOTAL</th>
                                        <th>PAYMENT</th>
                                        <th>STATUS</th>
                                    </tr>
                                        </thead>
                                        <tbody>
                                              @foreach($orders as $order)
                                            <tr>
                                                <td>{{$order->order_number}}</td>
                                                <td>{{$order->order_date}} </td>
                                               <td>{{$order->guestUser->email}}</td>
                                             <td>${{$order->orderDetails->total}}
                                                <input type="hidden" name="" value="{{$order->id}}"></td>

                                               <td>{{$order->orderDetails->pay_by}}
                                            </td>
                                                <td> @if($order->status ==1)
                                               COMPLETED

                                            @elseif($order->status ==2)
                                               PREPARED

                                            @elseif($order->status ==3)
                                               FIRST DELIVERY ATTEMPT

                                            @elseif($order->status ==4)
                                               SECOND DELIVERY ATTEMPT

                                            @elseif($order->status ==5)
                                               CANCELLED

                                            @else
                                               CREATED

                                            @endif</td>
                                             </tr>
                                           
                                            
                                        </tbody>
                                           @endforeach
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
            </div> 
           
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
        </div>
       

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<!-- <SCRIPT LANGUAGE="JavaScript"> 
if (window.print) {
document.write('<form><input type=button name=print value="Print" onClick="window.print()"></form>');
}
</script> -->

<script type="text/javascript">     
    function PrintDiv() {    
       var divToPrint = document.getElementById('divToPrint');
       var popupWin = window.open('', '_blank');
       popupWin.document.open();
       popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
        popupWin.document.close();
            }
 </script>


@include('layouts.footer')
@endsection



