@extends('layouts.master')
@section('content')
@include('layouts.header')
@include('layouts.sidebar')


<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
    <a class="btn waves-effect waves-light btn-secondary" style="
    margin-left: 776px;" href="branch/create">Add Branch</a>
               
         <form method="get" action="{{url('branch')}}">
         
   
    
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-5 align-self-center">
                    <h4 class="page-title">Branch List</h4>
                    <div class="d-flex align-items-center">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Library</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->

        <!-- Container fluid  -->
        <!-- ============================================================== -->
      
           
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            
            <div class="row">
                <!-- Column -->
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table product-overview" id="zero_config">
                                    <thead>
                                <tr>
                                    <th>COUNTRY NAME</th>
                                    <th>BRANCH NAME</th>
                                    <th>ACTION</th>
                                </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($branches as $branch)
                                      
                                        <tr>
                                            <td>{{$branch->country['name']}}</td>
                                            <td>{{$branch->name}}</td>
                                              <td>
                                            <a href="branch/{{$branch->id}}/edit" class="btn btn-success" data-toggle="tooltip" title="edit" >
                                            <i class="fa fa-edit"></i>
                                        </a></td>
                                        
                                        @endforeach
                                      </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
            </div>
        </div> 
        </form>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
    </div>


@include('layouts.footer')
@endsection

