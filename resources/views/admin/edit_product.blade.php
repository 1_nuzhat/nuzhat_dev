@extends('layouts.master')
@section('content')
@include('layouts.header')
@include('layouts.sidebar')

<style type="text/css">
   #user1 {
   padding: 15px;
   border: 1px solid #666;
   background: #fff;
   display: none;
   }
   #formButton {
   display: block;
   margin-right: auto;
   margin-left: auto;
   }

  .multiselect {
  width: 200px;
}

.selectBox {
  position: relative;
}

.selectBox select {
  width: 100%;
  font-weight: bold;
}

.overSelect {
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
}

#checkboxes {
  display: none;
  border: 1px #dadada solid;
}

#checkboxes label {
  display: block;
}

#checkboxes label:hover {
  background-color: #1e90ff;
}
   .AClass{
       right:0px;
       position: absolute;
   }
</style>

<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
   <div class="row">
      <div class="col-5 align-self-center">
         <h4 class="page-title">Edit PRODUCT</h4>
         <div class="d-flex align-items-center">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Library</li>
               </ol>
            </nav>
         </div>
      </div>
   </div>
   <!-- Container fluid  -->
   <!-- ============================================================== -->
   <div class="container-fluid">
      <!-- ============================================================== -->
      <!-- Start Page Content -->
      <!-- ============================================================== -->
      <!-- Row -->
      <div class="row">
         @if (session('status'))
         <div class="alert alert-success alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session('status') }}
         </div>
         @endif
         @if (session('success'))
         <div class="alert alert-success alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session('success') }}
         </div>
         @endif
         @if (session('error'))
         <div class="alert alert-danger alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session('error') }}
         </div>
         @endif   
         <div class="col-12">
            <div class="card">
               <form class="form-horizontal" action="{{ route('update.project') }}" method="POST" id="user_form" enctype="multipart/form-data">
                  {{ csrf_field() }}
                  <div class="card-body">
                     <h4 class="card-title"> Edit PRODUCT</h4>
                      <input type="hidden" name="product_id" value="{{ $product->id }}">
                     <div class="form-group row" id="list1" >
                        <label for="cat_name" class="col-sm-3 text-right control-label col-form-label anchor">CATEGORIES:</label>
                         <div class="multiselect">
                         <div class="selectBox" onclick="showCheckboxes()">
                           <select>
                             <option>Select Category</option>
                           </select>
                           <div class="overSelect"></div>
                         </div>
                          
                         <div id="checkboxes">
                            @foreach($categories as $category)
                                <?php $catExit = \App\Http\Controllers\ProductController::getChecked($category->id,$product->id);?>
                            <input type="checkbox" id="cat_name"  value="{{$category->id}}" name="cat_name[]" {{ $catExit ? 'checked' : ''}} />&nbsp; {{$category->cat_name}}<br/>
                          @endforeach
                                @if($errors->has('cat_name'))
                                    <span class="text-danger">{{ $errors->first('cat_name') }}</span>
                                @endif
                         </div>
                         
                       </div>
                      <!--   <div class="col-sm-9 items">
                           <select class="form-control @error('cat_name') is-invalid @enderror" name="cat_name" multiple>
                              <option disabled selected>{{__('Please select Category')}}</option>
                              @foreach($categories as $category)
                              <option value="{{$category->id}}" {{$category->id == old('category') ? 'selected' : ''}}>{{$category->cat_name}}</option>
                              @endforeach
                           </select>
                           @if($errors->has('cat_name'))
                           <span class="text-danger">{{ $errors->first('cat_name') }}</span>
                           @endif
                        </div> -->
                     </div>
                     <div class="form-group row">
                        <label for="name" class="col-sm-3 text-right control-label col-form-label">NAME:</label>
                        <div class="col-sm-9">
                           <input type="text" class="form-control" id="name" name="name" placeholder="NAME" value="{{ $product->name }}">
                           @if($errors->has('name'))
                           <span class="text-danger">{{ $errors->first('name') }}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row">
                        <label for="description" class="col-sm-3 text-right control-label col-form-label">PRODUCT DESCRIPTION:</label>
                        <div class="col-sm-9">
                           <textarea class="ckeditor form-control" name="description">{{ $product->description }}</textarea>
                           @if($errors->has('description'))
                           <span class="text-danger">{{ $errors->first('description') }}</span>
                           @endif
                        </div>
                     </div>
                      <div class="form-group row">
                          <label for="description" class="col-sm-3 text-right control-label col-form-label">PRODUCT IMAGES<br>(You choose multiple image):</label>
                          <div class="col-sm-9">
                              <input type="file" name="product_image[]" class="form-control" multiple>
                              @if($errors->has('product_image'))
                                  <span class="text-danger">{{ $errors->first('product_image') }}</span>
                              @endif
                              @foreach($productImages as $productImage)
                                  <div style="position:relative;">

                                  <span class="close AClass"  onclick="removeImage({{$productImage->id}})">
                                      <span>&times;</span>
                                  </span>
                              <img src="{{URL::asset('storage/images/'. @$productImage->image) }}" width="100" height="auto">
                                  </div>
                                  @endforeach
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="description" class="col-sm-3 text-right control-label col-form-label">IMAGE SIZE FORMAT AVAILABLE:</label>
                          <div class="col-sm-9">
                              <input type="text" class="form-control" name="size_desc" value="{{ $product->size_desc }}">
                              @if($errors->has('size_desc'))
                                  <span class="text-danger">{{ $errors->first('size_desc') }}</span>
                              @endif
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="description" class="col-sm-3 text-right control-label col-form-label">IMAGE DESCRIPTION:</label>
                          <div class="col-sm-9">
                              <textarea class="ckeditor form-control" name="image_desc">{{ $product->image_desc }}</textarea>
                              @if($errors->has('image_desc'))
                                  <span class="text-danger">{{ $errors->first('image_desc') }}</span>
                              @endif
                          </div>
                      </div>
                     <div class="form-group row">
                        <label for="product_id" class="col-sm-3 text-right control-label col-form-label">PRODUCT ID:</label>
                        <div class="col-sm-9">
                           <input type="product_id" class="form-control" id="product_id" name="p_id" placeholder="PRODUCT ID" value="{{ $product->p_id }}">
                           @if($errors->has('p_id'))
                           <span class="text-danger">{{ $errors->first('p_id') }}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row">
                        <label for="" class="col-sm-3 text-right control-label col-form-label">PRODUCT STATUS:</label>
                        <div class="col-sm-9">
                           <select id="p_status" name="p_status" class="form-control @error('p_status') is-invalid @enderror">
                              <option disabled selected>--- Select Product Status---</option>
                              <option value="1" {{$product->p_status == '1' ? 'selected' : ''}}>Yes</option>
                              <option value="0" {{$product->p_status == '0' ? 'selected' : ''}}>No</option>
                           </select>
                           @if($errors->has('p_status'))
                           <span class="text-danger">{{ $errors->first('p_status') }}</span>
                           @endif
                        </div>
                     </div>
                      <div class="form-group row">
                          <label for="description" class="col-sm-3 text-right control-label col-form-label">PRODUCT SPECIFICATION:</label>
                          <div class="col-sm-9">
                              <textarea class="ckeditor form-control" name="specification">{{ $product->specification }}</textarea>
                              @if($errors->has('specification'))
                                  <span class="text-danger">{{ $errors->first('specification') }}</span>
                              @endif
                          </div>
                      </div>

                      <div class="form-group row">
                          <label for="description" class="col-sm-3 text-right control-label col-form-label">VIDEO LINK:</label>
                          <div class="col-sm-9">
                              <input type="text" id="videos" class="form-control"  name="videos" placeholder="Video Link" value="{{ $product->video_link }}">
                              @if($errors->has('videos'))
                                  <span class="text-danger">{{ $errors->first('videos') }}</span>
                              @endif
                          </div>
                      </div>
                     <div class="form-group row">
                        <label for="" class="col-sm-3 text-right control-label col-form-label">PRODUCT TAGS:</label>
                        <div class="col-sm-9">
                            <input type="hidden" name="product_name" value="{{ @$productTag->tags->id }}">
                           <input type="text" class="form-control" id="title" name="title" placeholder="PPRODUCT TAGS" value="{{ @$productTag->tags->title }}">
                           @if($errors->has('title'))
                           <span class="text-danger">{{ $errors->first('title') }}</span>
                           @endif
                        </div>
                     </div>
                     <div class="form-group row">
                        <label for="" class="col-sm-3 text-right control-label col-form-label">TAG STATUS:</label>
                        <div class="col-sm-9">
                           <select id="t_status" name="t_status" class="form-control @error('t_status') is-invalid @enderror">
                              <option disabled selected>--- Select Tag Status---</option>
                              <option value="1" {{ @$productTag->tags->t_status == 1 ? 'selected':'' }}>Yes</option>
                              <option value="0" {{ @$productTag->tags->t_status == 0 ? 'selected':'' }}>No</option>
                           </select>
                           @if($errors->has('t_status'))
                           <span class="text-danger">{{ $errors->first('t_status') }}</span>
                           @endif
                        </div>
                     </div>
                  </div>
                  <hr>
                  <div class="card-body">
                     <h4 class="card-title"> VARIANT</h4>
                     <div class="container-fluid">
                        <div class="row">
                           <div class="col-10">
                              <!-- Column -->
                              <div class="card">
                                 <div class="card-body">
                                    <button type="button" id="formButton">Add Variants</button>
                                    <div >
                                       <div class="table-responsive">
                                          <table class="table table-striped table-bordered" id="user_data">
                                             <tr>
                                                <th>COLOR</th>
                                                <th>SIZE</th>
                                                <th>BRANCH</th>
                                                <th>IMAGE</th>
                                                <th>IMAGE2</th>
                                                <th>SKU</th>
                                                <th>PRICE</th>
                                                <th>COMPARE PRICE</th>
                                                <th>COST</th>
                                                 <th>QUANTITY</th>
                                                <th>STATUS</th>
                                                <th>ACTION</th>
                                             </tr>
                                              @foreach($pro_variant as $pro_variants)
                                                  <input type="hidden" name="variantid[]" value="{{ $pro_variants->id }}">

                                              <tr id="row_edit{{$pro_variants->id}}">

                                                      <td >{{ $pro_variants->color[0]->color_name }} <input type="hidden" class="form-control" name="color_name_edit[]" id="color_name_edit{{$pro_variants->id}}" value="{{ $pro_variants->color[0]->id }}"> </td>
                                                  <td>{{ $pro_variants->size }} <input type="hidden" name="size_edit[]" id="size_edit{{$pro_variants->id}}" value="{{ $pro_variants->size }}"></td>
                                                  <td>{{ $pro_variants->branch ? $pro_variants->branch->name :''  }} <input type="hidden" name="branch_name_edit[]" id="branch_name_edit{{$pro_variants->id}}" value="{{ $pro_variants->branch ? $pro_variants->branch->id :'' }}"></td>
                                                  <td> <input id="images_edit{{$pro_variants->id}}" type="file" name="images_edit[]" value="{{URL::asset('storage/images/'. @$pro_variants->productImage->images) }}"> <img src="{{URL::asset('storage/images/'. @$pro_variants->productImage->images) }}" id="newimage{{$pro_variants->id}}" width="100" height="auto"></td>
                                                  <td> <input id="images_dual_edit{{$pro_variants->id}}" type="file" name="images_dual_edit[]" > <img src="{{URL::asset('storage/images/'. @$pro_variants->productImage->images_dual) }}" id="newimage_dual{{$pro_variants->id}}" width="100" height="auto"></td>
                                                  <td>{{ $pro_variants->product_sku }} <input type="hidden" name="product_sku_edit[]" id="product_sku_edit{{$pro_variants->id}}" value="{{ $pro_variants->product_sku }}"></td>
                                                  <td>{{ $pro_variants->product_price }} <input type="hidden" name="product_price_edit[]" id="product_price_edit{{$pro_variants->id}}" value="{{ $pro_variants->product_price }}"></td>
                                                  <td> {{ $pro_variants->product_compare_price }}<input type="hidden" name="product_compare_price_edit[]" id="product_compare_price_edit{{$pro_variants->id}}" value="{{ $pro_variants->product_compare_price }}"></td>
                                                  <td> {{ $pro_variants->product_cost }}<input type="hidden" name="product_cost_edit[]" id="product_cost_edit{{$pro_variants->id}}" value="{{ $pro_variants->product_cost }}"></td>
                                                  <td>{{ $pro_variants->quantity }} <input type="hidden" name="quantity_edit[]" id="product_quantity_edit{{$pro_variants->id}}" value="{{ $pro_variants->quantity }}"></td>
                                                  <td>{{ $pro_variants->status }} <input type="hidden" name="status_edit[]" id="status{{$pro_variants->id}}" value="{{ $pro_variants->status }}"></td>
                                                  <td><button type="button" name="remove_details" class="btn btn-danger btn-xs js-remove_details" data-url="{{ route('remove.variant') }}" data-variantId="{{$pro_variants->id}}">Remove</button>
                                                  <button type="button" name="edit_deatils" class="btn btn-primary btn-xs edit_deatils" data-obj="{{ $pro_variants }}" >Edit</button>
                                                  </td>
                                              </tr>
                                                  @endforeach
                                          </table>
                                       </div>
                                       <div align="center">
                                       </div>
                                    </div>
                                 </div>
                                 <div id="user1" class="emptyuser">
                                    <b>Color: </b>
                                    <select id="color_name" class="form-control">
                                       <option disabled selected>--- Select Color---</option>
                                       @foreach($colors as $color)
                                       <option value="{{$color->id}}">{{$color->color_name}}</option>
                                       @endforeach
                                    </select>
                                    <span id="error_color_name" class="text-danger"></span>
                                      
                                    <b>Size: </b>
                                    <select id="size" class="form-control">
                                       <option disabled selected>--- Select Size---</option>
                                       <option value="XS">XS</option>
                                       <option value="S">S</option>
                                       <option value="M">M</option>
                                       <option value="L" >L</option>
                                       <option value="XL">XL</option>
                                       <option value="XXL">XXL</option>
                                       <option value="3XL">3XL</option>
                                    </select>
                                    <span id="error_size" class="text-danger"></span>
                                     <b>Branch: </b>
                                     <select id="branch_name" class="form-control">
                                         <option disabled selected>--- Select Branch---</option>
                                         @foreach($branch as $branches)
                                             <option value="{{$branches->id}}">{{$branches->name}}</option>
                                         @endforeach
                                     </select>
                                    <span id="images" name="images[]" />
                                    <span id="error_images" class="text-danger"></span>
                                    <br/>
                                    <b>SKU: </b><input type="text" id="product_sku" class="form-control" />
                                    <span id="error_product_sku" class="text-danger"></span>
                                    <b>PRICE: </b><input type="text"  id="product_price" class="form-control" />
                                    <span id="error_product_price" class="text-danger"></span>
                                    <b>COMPARE PRICE: </b><input type="text" id="product_compare_price" class="form-control" />
                                    <span id="error_product_compare_price" class="text-danger"></span>
                                    <b>COST: </b><input type="text"  id="product_cost" class="form-control" />
                                    <span id="error_product_cost" class="text-danger"></span>
                                     <b>QUANTITY: </b>
                                     <input type="text" id="quantity" class="form-control" />
                                     <span id="error_quantity" class="text-danger"></span>
                                    <b>STATUS: </b>
                                    <select id="status" class="form-control">
                                       <option disabled selected>--- Select Variant Status---</option>
                                       <option value="1">Yes</option>
                                       <option value="0">No</option>
                                    </select>
                                    <span id="error_status" class="text-danger"></span>
                                    <button type="button" name="addnew" id="addnew" class="btn btn-info">Save</button>
                                 </div>
                                 <div id="action_alert" title="Action">
                                 </div>
                                 <hr>
                                 <div class="card-body">
                                    <div class="form-group mb-0 text-right">
                                       <button type="submit" id="sub" class="btn btn-info waves-effect waves-light">Submit</button>
                                    </div>
                                 </div>
                              </div>
                         </form>
               </div>
               </div>
               </div>
               </div>
               </div>
            </div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<!-- End Row -->
<script>
   $(document).ready(function() {

   var count = 0;
   $("#formButton").click(function() {
    
   
   $("#user1").toggle();
   $('#color_name').val('');
   $('#size').val('');
   $('#branch_name').val('');
   $('#images').prop('file')[0];
   $('#imagesdual').prop('file')[0];
   $('#product_sku').val('');
   $('#product_price').val('');
   $('#product_compare_price').val('');
   $('#product_cost').val('');
   $('#quantity').val('');
       $('#status').val('');
   
   
   
   $('#error_color_name').text('');
   $('#error_size').text('');
   $('#error_branch_name').text('');
   $('#error_images').file('');
   $('#error_product_sku').text('');
   $('#error_product_price').text('');
   $('#error_product_compare_price').text('');
   $('#error_product_cost').text('');
   $('#error_quantity').text('');
   $('#error_status').text('');
   
   
   // $('#addnew').text('Save');
   });

       $(".edit_deatils").click(function() {
           var _that = $(this);

           var objdata1 = _that.attr('data-obj');
           var usedata = JSON.parse(objdata1);
           $("#user1").toggle();
           $('#color_name').val(usedata.color[0].id);
           $('#size').val(usedata.size);
           if(usedata.branch) {
               $('#branch_name').val(usedata.branch.id);
           }else{
               $('#branch_name').val('');
           }
            $('#product_sku').val(usedata.product_sku);
           $('#product_price').val(usedata.product_price);
           $('#product_compare_price').val(usedata.product_compare_price);
           $('#product_cost').val(usedata.product_cost);
           $('#quantity').val(usedata.quantity);
           $('#status').val(usedata.status);



           $('#error_color_name').text('');
           $('#error_size').text('');
           // $('#error_images').file('');
           $('#error_product_sku').text('');
           $('#error_product_price').text('');
           $('#error_product_compare_price').text('');
           $('#error_product_cost').text('');
           $('#error_quantity').text('');
           $('#error_status').text('');


           $('#addnew').html('Edit');
       });



   $('#addnew').click(function(){
   var error_color_name = '';
   var error_size = '';
   var error_branch_name = '';
   var error_images = '';
   var error_product_sku = '';
   var error_product_price = '';
   var error_product_compare_price = '';
   var error_product_cost = '';
   var error_quantity = '';
   var error_status = '';
   
   
   var color_name = '';
   var size = '';
   var branch_name = '';
   var images = '';
   var imagesdual = '';
   var product_sku = '';
   var product_price = '';
   var product_compare_price = '';
   var product_cost = '';
   var quantity = '';
   var status = '';
   
   // if($('#color_name').val() == '')
   // {
   // error_color_name = 'Color Name is required';
   // $('#error_color_name').text(error_color_name);
   // $('#color_name').css('border-color', '#cc0000');
   // color_name = '';
   // }
   // else
   // {
   // error_color_name = '';
   // $('#error_color_name').text(error_color_name);
   // $('#color_name').css('border-color', '');
   color_name = $('#color_name').val();
    color_text = $('#color_name option:selected').text();
   // }

  
   size = $('#size').val();
   branch_name = $('#branch_name').val();
   branch_text = $('#branch_name option:selected').text();
 
   images = $('#images').attr('src');
  
   product_sku = $('#product_sku').val();
  
   product_price = $('#product_price').val();
   
   product_compare_price = $('#product_compare_price').val();
 
   product_cost = $('#product_cost').val();

   quantity = $('#quantity').val();
   
   status = $('#status').val();

   
   if(error_color_name != '' || error_size != ''|| error_branch_name != ''|| error_images != ''|| error_product_sku != ''|| error_product_price != ''|| error_product_compare_price != ''|| error_product_cost != ''|| error_quantity != '' || error_status != '')
   {
   return false;
   }
   else
   {
    
   if($('#addnew').text() == 'Save')
   {
       // alert('sd');
    var pro_color = color_name == null ? '' : color_name;
    var pro_size = size == null ? '' : size;
    var pro_status = status == null ? '1' : status;   
    count = count + 1;
    // alert(count);
    output = '<tr id="row_'+count+'">';
    
    output += '<td>'+color_text+' <input type="hidden" class="form-control" name="color_name[]" id="color_name'+count+'" value="'+pro_color+'" /></td>';
    output += '<td>'+pro_size+' <input type="hidden" name="size[]" id="size'+count+'" value="'+pro_size+'" /></td>';
    output += '<td>'+branch_text+' <input type="hidden" name="branch_name[]" id="branch_name'+count+'" value="'+branch_name+'" /></td>';

       // output += '<td>'+img+'<img src= "" class="productImg"><input type="hidden" value="'+img+'" name="images[]"/></td>';
    output += '<td> <input id="images" type="file" name="images[]" /></td>';
    output += '<td> <input id="images_dual" type="file" name="images_dual[]" /></td>';
    output += '<td>'+product_sku+' <input type="hidden" name="product_sku[]" id="product_sku'+count+'" value="'+product_sku+'" /></td>';
    output += '<td>'+product_price+' <input type="hidden" name="product_price[]" id="product_price'+count+'" value="'+product_price+'" /></td>';
   
    output += '<td>'+product_compare_price+' <input type="hidden" name="product_compare_price[]" id="product_compare_price'+count+'" value="'+product_compare_price+'" /></td>';
   
    output += '<td>'+product_cost+' <input type="hidden" name="product_cost[]" id="product_cost'+count+'" value="'+product_cost+'" /></td>';
       output += '<td>'+quantity+' <input type="hidden" name="quantity[]" id="quantity'+count+'" value="'+quantity+'" /></td>';

       output += '<td>'+pro_status +' <input type="hidden" name="status[]" id="status'+count+'" value="'+pro_status+'" /></td>';
    output += '<td><button type="button" name="remove_details" class="btn btn-danger btn-xs remove_details" id="'+count+'">Remove</button></td>';
    output += '</tr>';
    $('#user_data').append(output);
   }
   else
   {
       var pro_color = color_name == null ? '' : color_name;

       var row_id = $('#hidden_row_id').val();

    var veriant = $('#veriantId').val();
    var image12 = images = $('#newimage'+veriant).attr('src');
    var imagedual12 = $('#newimage_dual'+veriant).attr('src');
       var pro_status = status == null ? '1' : status;
       output = '';
    output += '<td>'+color_text+' <input type="hidden" name="color_name_edit[]" id="color_name_edit'+veriant+'" value="'+pro_color+'" /></td>';
    output += '<td>'+size+' <input type="hidden" name="size_edit[]" id="size_edit'+veriant+'" value="'+size+'" /></td>';
    // output += '<td>'+branch_text+' <input type="hidden" name="branch_name[]" id="branch_name'+count+'" value="'+branch_name+'" /></td>';
       output += '<td>'+branch_text+' <input type="hidden" name="branch_name_edit[]" id="branch_name'+count+'" value="'+branch_name+'" /></td>';

       output += '<td><input id="images" type="file" name="images_edit[]" value="'+image12+'"/><img src= "'+image12+'"   width="100" height="100"></td>';
    output += '<td><input id="images_dual" type="file" name="images_dual_edit[]" value="'+imagedual12+'" /><img src= "'+imagedual12+'"  width="100" height="100"></td>';
    output += '<td>'+product_sku+' <input type="hidden" name="product_sku_edit[]" id="product_sku_edit'+veriant+'" value="'+product_sku+'" /></td>';
    output += '<td>'+product_price+' <input type="hidden" name="product_price_edit[]" id="product_price_edit'+veriant+'" value="'+product_price+'" /></td>';
    output += '<td>'+product_compare_price+' <input type="hidden" name="product_compare_price_edit[]" id="product_compare_price_edit'+veriant+'" value="'+product_compare_price+'" /></td>';
    output += '<td>'+product_cost+' <input type="hidden" name="product_cost_edit[]" id="product_cost_edit'+veriant+'" value="'+product_cost+'" /></td>';
    output += '<td>'+quantity+' <input type="hidden" name="quantity_edit[]" id="quantity'+count+'" value="'+quantity+'" /></td>';
       output += '<td>'+status+' <input type="hidden" name="status_edit[]" id="status_edit'+veriant+'" value="'+pro_status+'" /></td>';


    output += '<td><button type="button" name="remove_details" class="btn btn-danger btn-xs remove_details" id="'+veriant+'">Remove</button></td>';
       $('#row_edit'+veriant+'').replaceWith(output);
       // $('#row_'+row_id+'').remove();
       // $(this).parent().replaceWith(output);
    // $('#user_data').append(output);
   }
   $("#user1").toggle('close');
   }
   // $(".emptyuser").clear();
   });

   $(document).on('click', '.remove_details', function(){
   var row_id = $(this).attr("id");
   if(confirm("Are you sure you want to remove this row data?"))
   {
   $('#row_'+row_id+'').remove();
   }
   else
   {
   return false;
   }
   });


   
   $('#action_alert').toggle({
   autoOpen:false
   });
   
   });
   
</script>
<script>
    function removeImage(id)
    {
        var x = confirm("Are you sure you want to delete?");
        var imageId =id;

        if (x)

            $.ajax(
                {
                    url: '/remove_image',
                    type: 'POST',
                    data: {
                        _token: "{{ csrf_token() }}",
                        idval : imageId,
                    },
                    success: function (result) {
                        // showNotification({type: 'success', msg: result.message});
                        window.location.reload();
                    }
                });
        else
            return false;

    }
</script>
<script>

   var checkList = document.getElementById('list1');
checkList.getElementsByClassName('anchor')[0].onclick = function(evt) {
  if (checkList.classList.contains('visible'))
    checkList.classList.remove('visible');
  else
    checkList.classList.add('visible');
}
</script>
<script type="text/javascript">
    var expanded = false;

function showCheckboxes() {
  var checkboxes = document.getElementById("checkboxes");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}
</script>
<script>
    $('.js-remove_details').off('click').on('click',function () {
        var _that = $(this);
        var _url = _that.attr('data-url');
        var variantId = _that.attr('data-variantId');

        if(confirm("Are you sure you want to remove this Variant ?"))
        {
            $.ajax({
                url: _url,
                type: 'POST',
                data: {_token: "{{ csrf_token() }}" ,variantId:variantId},
                success: function (data) {
                    if (data['success']) {
                        $('#row_edit'+variantId+'').remove();
                        toastr.success('Variant delete successfully');
                        // window.location.reload(true);

                    }
                },

            });
        }
        else
        {
            return false;
        }
    })
</script>
@include('layouts.footer')
<script src="{{ asset('ckeditor/ckeditor.js')}}"></script>
<script>CKEDITOR.replace('article-ckeditor');</script>
@endsection