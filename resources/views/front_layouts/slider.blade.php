<!-- slider -->
  
    <div id="myCarousel" class="carousel slide">
      
        <!-- <ul class="carousel-indicators">
          <li data-target="#demo" data-slide-to="0" class="active"></li>
          <li data-target="#demo" data-slide-to="1"></li>
          <li data-target="#demo" data-slide-to="2"></li>
        </ul> -->
         <ul class="carousel-indicators">
            <li class="item1 active"></li>
            <li class="item2"></li>
            <li class="item3"></li>
        </ul>
        
        <div class="carousel-inner">
           @foreach($banners as $banner)
          <div class="carousel-item active">
       <img src="{{asset('storage/images/'.$banner->images)}}">
    </div>
     @endforeach
         <!--  @foreach ($banners as $key => $banner)
            <div class="item{{ $key == 0 ? ' active' : '' }}">
               <img src="{{asset('storage/images/'.$banner->images)}}" width="100%" height="300">
            </div>
          @endforeach -->
        <!--    @foreach($banners as $banner)
          <div class="carousel-item active">
        
          <img src="{{asset('storage/images/'.$banner->images)}}" width="100%" height="300">
       
            <div class="carousel-caption">
              <h3>Los Angeles</h3>
              <p>We had such a great time in LA!</p>
            </div>   
          </div>
           @endforeach -->
          <!-- <div class="carousel-item">
            <img src="assets/img/banner.png" alt="Chicago" width="100%" height="300">
            <div class="carousel-caption">
              <h3>Chicago</h3>
              <p>Thank you, Chicago!</p>
            </div>   
          </div> -->
         <!--  <div class="carousel-item">
            <img src="assets/img/banner.png" alt="New York" width="100%" height="300">
            <div class="carousel-caption">
              <h3>New York</h3>
              <p>We love the Big Apple!</p>
            </div>   
          </div> -->
        </div>
       
        <a class="carousel-control-prev" href="#myCarousel">
          <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#myCarousel">
          <span class="carousel-control-next-icon"></span>
        </a>
         
      </div>
    
    <!-- slider -->

    <script>
$(document).ready(function(){
  // Activate Carousel
  $("#myCarousel").carousel();
    
  // Enable Carousel Indicators
  $(".item1").click(function(){
    $("#myCarousel").carousel(0);
  });
  $(".item2").click(function(){
    $("#myCarousel").carousel(1);
  });
  $(".item3").click(function(){
    $("#myCarousel").carousel(2);
  });
    
  // Enable Carousel Controls
  $(".carousel-control-prev").click(function(){
    $("#myCarousel").carousel("prev");
  });
  $(".carousel-control-next").click(function(){
    $("#myCarousel").carousel("next");
  });
});
</script>

    