<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMobileCodeGuestUserInfoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('guest_user_info', function (Blueprint $table) {
            $table->string('mobile_code')->nullable();
        });
        Schema::table('order_details', function (Blueprint $table) {
            $table->string('pay_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('guest_user_info', function (Blueprint $table) {
            $table->dropColumn('mobile_code');
        });
        Schema::table('order_details', function (Blueprint $table) {
            $table->dropColumn('pay_by');
        });
    }
}
