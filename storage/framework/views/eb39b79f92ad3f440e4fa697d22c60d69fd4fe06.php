<?php echo $__env->make('front_layouts.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<!-- For mobile -->















































<!-- End .header -->
<!-- end -->
<!-- banner -->
<div class="banner text-center mt-62">
    <img src="<?php echo e(asset('assets/img/empty.png')); ?>" class="img-fluid">
    <p>El carrito esta vacio</p>
    <a href="<?php echo e(route('product.list')); ?>" class="btn">ve a buscar algo que te guste.</a>
    <p class="mt-4">Supongo que te gusta</p>
</div>
<!-- banner -->

<section class="container-fluid item-product">
    <div class="row row-sm">
        <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <?php $productId = \App\Traits\CommonTrait::encodeId($product->pid); ?>
        <div class="col-6 col-md-4 col-lg-3 col-xl-2">
            <a href="<?php echo e(route('product.description',$productId)); ?>">
            <div class="product-default">
                <figure>
                    <a href="<?php echo e(route('product.description',$productId)); ?>">
                        <img src="<?php echo e(asset('storage/images/'.$product->images)); ?>">
                    </a>
                </figure>
                <div class="product-details">
                    <h2 class="product-title">
                        <a style="color: #000000;"><?php echo e($product->product_name); ?></a>
                    </h2>
                    <div class="price-box">
                        <span class="product-price"><i>MXN $<?php echo e($product->product_price); ?></i> </span>
                    </div>
                    <!-- End .price-box -->
                    <div class="category-wrap">
                        <div class="category-list">
                            <?php $__currentLoopData = $msg; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $msgs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <p style="color: #b7b0b0;"><?php echo e($msgs->title); ?></p>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </div>
                    </div>
                </div>
                <!-- End .product-details -->
            </div>
            </a>
        </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>



    </div>
</section>
<div class="mobile-menu-overlay"></div>
<!-- End .mobil-menu-overlay -->
<div class="mobile-menu-container">
    <div class="mobile-menu-wrapper">
        <div class="menu-top"> <span><img src="assets/img/logo.png"></span><span class="mobile-menu-close"><img src="assets/img/close.png"></span></div>
        <nav class="mobile-nav">
            <ul class="mobile-menu">
                <li class="active"><a href="listing.html">Categorías</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <span class="caret">Más vendido</span></a>
                    <ul class="dropdown-menu">
                        <li><a href="listing.html">submenu</a></li>
                        <li><a href="listing.html">submenu</a></li>
                        <li><a href="listing.html">submenu</a></li>
                    </ul>
                </li>
                <li>
                    <a href="listing.html">Liquidacíon</a>
                </li>
                <li>
                    <a href="listing.html">Contactanos</a>
                </li>
            </ul>
        </nav>
        <!-- End .mobile-nav -->
    </div>
    <!-- End .mobile-menu-wrapper -->
</div>
<!-- End .mobile-menu-container -->
<script src="<?php echo e(asset('js/jquery.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/popper.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/number.js')); ?>"></script>
<script>
    $('.mobile-menu-toggler').on('click', function (e) {
        $('body').toggleClass('mmenu-active');
        $(this).toggleClass('active');
        e.preventDefault();
    });

    $('.mobile-menu-overlay, .mobile-menu-close').on('click', function (e) {
        $('body').removeClass('mmenu-active');
        e.preventDefault();
    });
</script>
</body>
</html><?php /**PATH C:\xampp\htdocs\ganga_new\ganga_box_new\resources\views/cart/empty_cart.blade.php ENDPATH**/ ?>