<?php if($enabled): ?>
    <!-- Facebook Pixel Code -->
    <script <?php if($nonce): ?>nonce="<?php echo e($nonce()); ?>"<?php endif; ?>>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
            n.callMethod.apply(n,arguments):n.queue.push(arguments)};
            if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
            n.queue=[];t=b.createElement(e);t.async=!0;
            t.src=v;s=b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t,s)}(window, document,'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        <?php $__currentLoopData = $id; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            fbq('init', '<?php echo e($i); ?>');
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        fbq('track', 'PageView');
    </script>
    <?php $__currentLoopData = $id; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $i): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <noscript><img height="1" width="1" style="display:none"
                   src="https://www.facebook.com/tr?id=<?php echo e($i); ?>&ev=PageView&noscript=1"
        /></noscript>
    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <!-- End Facebook Pixel Code -->
<?php endif; ?><?php /**PATH C:\xampp\htdocs\ganga_new\ganga_box_new\vendor\weblagence\laravel-facebook-pixel\src/../resources/views/head.blade.php ENDPATH**/ ?>