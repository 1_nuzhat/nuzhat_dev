
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layouts.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
">

<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
     <?php if(session('status')): ?>
                        <div class="alert alert-success alert-dismissable">
                           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           <?php echo e(session('status')); ?>

                        </div>
                    <?php endif; ?>
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        <div class="row">

                    
               
                <!-- Column -->
        <div class="col-lg-6">
            <div class="card">
                <div class="card-body">

        <button class="btn waves-effect waves-light btn-success active_all" data-url="<?php echo e(url('/activeAll')); ?>">Active</button>
        <button class="btn waves-effect waves-light btn-danger inactive_all" data-url="<?php echo e(url('/inactiveAll')); ?>">In-Active</button>
    </div>
    </div>
    </div>
    </div>
         <form method="post" action="<?php echo e(route('product.tag')); ?>" onsubmit=" return getData()">
             <?php echo csrf_field(); ?>
        <div class="row">
                <!-- Column -->
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">

                <button class="btn waves-effect waves-light btn-primary"><?php echo e(__('Product tag')); ?></button>
               <!--  <button class="btn waves-effect waves-light btn-danger" id="t_id"><?php echo e(__('Remove tag')); ?></button> -->
                <a class="btn waves-effect waves-light btn-secondary" href="<?php echo e(route('export')); ?>">Export Data</a>
                <a class="btn waves-effect waves-light btn-secondary" href="<?php echo e(route('fbexport')); ?>">Export Catalogue Data</a>
               </div>
            </div>
        </div>
    </div>
   
    
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-5 align-self-center">
                    <h4 class="page-title">Product List</h4>
                    <div class="d-flex align-items-center">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Library</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->

        <!-- Container fluid  -->
        <!-- ============================================================== -->
      
           
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            
            <div class="row">
                <!-- Column -->
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table product-overview" id="datatable">
                                    <thead>
                                <tr>
                                    <th><input type="checkbox" id="CheckAll" ></th>
                                    <th>IMAGE</th>
                                    <th>NAME</th>
                                    <th>STATUS</th>
                                    <th>INVENTORY</th>
                                    <th>PRODUCT ID</th>
                                    <th>GLOBAL SALES QTY</th>
                                    <th>GLOBAL SALES MONEY</th>
                                    <th>EDIT</th>
                                    <th>DELETE</th>
                                </tr>
                                    </thead>
                                    <tbody>
                                        <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                       <?php $inventory = \App\Http\Controllers\InventoryController::inventoryProject($product->id) ?>
                                        <tr>
                                            <th><input type="checkbox" class="selectitem"  data-id="<?php echo e($product->id); ?>" name="selectitem[]" value="<?php echo e($product->id); ?>"></th>
                                            <td>

                                                <?php if(!empty($product->images)): ?><img src="<?php echo e(URL::asset('storage/images/'. $product->images)); ?>" width="100" height="auto">
                                                <?php else: ?>
                                                <?php echo e(__('N/A')); ?>

                                                 <?php endif; ?>

                                            </td>

                                            <td><?php echo e($product->name); ?></td>
                                            <td> <?php if($product->p_status ==1): ?>
                                                Active
                                            <?php else: ?>
                                                In-active
                                            <?php endif; ?>
                                            </td>
                                            <td><?php echo e($inventory); ?></td>
                                            <td><?php echo e($product->p_id); ?></td>
                                            <td><?php $globalQnt = \App\Http\Controllers\ProductController::getGlobalQuty($product->id);?><?php echo e($globalQnt->globalquantity); ?></td>
                                            <td> <?php $global = \App\Http\Controllers\ProductController::getGlobalPrice($product->id) ?><?php echo e($global->globalprice); ?></td>
                                       
                                            <td>
                                                <a class="btn btn-success mb-6" href="<?php echo e(route('edit-project', $product->id)); ?>" style=" margin-bottom: 10px;margin-top: 10px;"><i class="fa fa-edit"></i></a>
                                             </td>
                                         </form>
                                          <td>
                                          <form  action="<?php echo e(url('/product/'.$product->id)); ?>" method="POST">
                                        <?php echo csrf_field(); ?>
                                      <?php echo method_field('delete'); ?>
                                        
                                            <button type="submit" name="button" class="btn btn-danger">delete</button>
                                     
                                    
                                                
                                            </td>
                                            </form>
                                        </tr>
                                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                      </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
            </div>
        </div> 
       <!--  </form> -->
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
    </div>



<script>
    $(document).ready(function() {
        $('#datatable').DataTable({"bLengthChange" : false});
    });
</script>
<script>
     $(document).ready(function () {
        $("#CheckAll").click(function () {
            $(".selectitem").attr('checked', this.checked);
        });
     });
</script>

<script>
        $(document).ready(function(){
            $('#t_id').attr('disabled', true);
        });
        $('#selectall').click(function(){
            $("input[type='checkbox']").prop('checked',this.checked);

        });

        function checkSelected()
        {
            var checkboxes = $('[name="selectitem[]"]:checked').length;
            if(checkboxes > 0) {
                $('#t_id').attr('disabled', false);
            } else {
                $('#t_id').attr('disabled', true);
            }  
        }
        
        $('.selectitem').click(function(){
            checkSelected();
            
        });

        $('#selectall').click(function(){
            checkSelected();
            
        });

        $('#remove_button').click(function(){
            $(this).val('active');
        });

</script>

<script type="text/javascript">
      $(document).ready(function () {

        $('.active_all').on('click', function(e) {


            var allVals = [];
            $(".selectitem:checked").each(function() {
                allVals.push($(this).attr('data-id'));
            });


            if(allVals.length <=0)
            {
                alert("Please select row.");
            }  else {


                var check = confirm("Are you sure you want to Active this Product?");
                if(check == true){


                    var join_selected_values = allVals.join(",");

    //alert(join_selected_values);
                    $.ajax({
                        url: $(this).data('url'),
                        type: 'GET',
                        headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                        data: 'ids='+join_selected_values,
                        success: function (data) {
                            if (data['success']) {
                                $(".sub_chk:checked").each(function() {
                                    
                                });
                                window.location.reload(true); 
                                 $('#output_code').html(data['success']);
                               
                            } 
                        },
                        
                    });


                
                }
            }
        });
    });
</script>

<script type="text/javascript">
  $(document).ready(function () {

    $('.inactive_all').on('click', function(e) {


        var allVals = [];
        $(".selectitem:checked").each(function() {
            allVals.push($(this).attr('data-id'));
        });


        if(allVals.length <=0)
        {
            alert("Please select row.");
        }  else {


            var check = confirm("Are you sure you want to In-Active this Product?");
            if(check == true){


                var join_selected_values = allVals.join(",");


                $.ajax({
                    url: $(this).data('url'),
                    type: 'GET',
                    headers: {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                    data: 'ids='+join_selected_values,
                    success: function (data) {
                        if (data['success']) {
                            $(".selectitem:checked").each(function() {
                                
                            });
                            window.location.reload(true); 
                             $('#output_code').html(data['success']);
                           
                        } 
                    },
                    
                });


            
            }
        }
    });
});
</script>
<script>
    $('.deleteProduct').off('click').on('click',function () {
        var _that = $(this);
        var _url = _that.attr('data-url');
        var productId = _that.attr('data-productId');
        var type = _that.attr('data-type');

        if(confirm("Are you sure you want to "+ type+ " this product ?"))
        {
            $.ajax({
                url: _url,
                type: 'POST',
                data: {_token: "<?php echo e(csrf_token()); ?>" ,productId:productId, type :type},
                success: function (data) {
                    if (data['success']) {
                        window.location.reload(true);

                    }
                },

            });
        }
        else
        {
            return false;
        }
    })
</script>
<script>
    function getData() {
        var checkboxes = $('[name="selectitem[]"]:checked').length;
        if(checkboxes > 0)
        {
          return true;
        }else{
            toastr.error('Please check at least one product.');
            return false;
        }

    }
</script>
<!-- The core Firebase JS SDK is always required and must be listed first -->
<script src="https://www.gstatic.com/firebasejs/8.3.1/firebase-app.js"></script>

<!-- TODO: Add SDKs for Firebase products that you want to use
     https://firebase.google.com/docs/web/setup#available-libraries -->
<script src="https://www.gstatic.com/firebasejs/8.3.1/firebase-analytics.js"></script>

<script>
  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  var firebaseConfig = {
    apiKey: "AIzaSyB1xTvY4bhGOms9gPM-pg5S3KGBdOKBkt8",
    authDomain: "gangabox-b56b2.firebaseapp.com",
    projectId: "gangabox-b56b2",
    storageBucket: "gangabox-b56b2.appspot.com",
    messagingSenderId: "1039820844628",
    appId: "1:1039820844628:web:f9d8aa9fc328a5356747c1",
    measurementId: "G-SBWE8V6MHE"
  };
  // Initialize Firebase
  firebase.initializeApp(firebaseConfig);
  firebase.analytics();
  var storage = firebase.storage();
  console.log(firebase.analytics());
</script>
<?php echo $__env->make('layouts.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ganga_new\ganga_box_new\resources\views/admin/product_list.blade.php ENDPATH**/ ?>