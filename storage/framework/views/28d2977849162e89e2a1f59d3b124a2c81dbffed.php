<?php echo $__env->make('front_layouts.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<style>
    .cate h4 {
        font-size :17px}
    .cate {
        font-size: 15px;
    }
    .paint {
        position:absolute;
        top:0;
        left:0;
        height:30px;
        width:30px;
        background:#000;
        display: none;
    }
    .ajax-loader-container{
        position: fixed;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        z-index: 10;
    }
    .ajax-loader{
        position: relative;
        margin: 0 auto;
        width: 60px;
        height: 100%;
        z-index: 9999;
    }
</style>
         






















         
         <!-- infinite scroll  -->
            <section class="scrolldown">
                <div class="black">
                    <ul>
                        <li class="active"><img src="<?php echo e(asset('assets/img/FLOATING-BAR3.jpg')); ?>" alt="icon"></li>

                        <li><img src="<?php echo e(asset('assets/img/FLOATING-BAR2.jpg')); ?>" alt="icon"></li>

                        <li><img src="<?php echo e(asset('assets/img/FLOATING-BAR4.jpg')); ?>" alt="icon"></li>

                    </ul>
                </div>
               <div class="container-fluid my-3 infinite">
                    <div class="row">
                      <div class="col-2">
                          <div class="fixed">
                              <?php $__currentLoopData = $familySubcats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $familySub): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <div class="cate">
                                <h4><b><?php echo e($familySub['fname']); ?></b></h4>
                                    <ul>
                                        <?php $__currentLoopData = $familySub['subcat']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subcat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php $subId = \App\Traits\CommonTrait::encodeId($subcat->categories_id) ?>
                                            <li><a href="<?php echo e(route('subcat.products',$subId)); ?>" ><p style="color: black;"><?php echo e($subcat->category->cat_name); ?></p></a></li>

                                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

                                    </ul>
                              </div>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </div>
                          
                          
                      </div>
                        <input type="hidden" id="lastId" value="<?php echo e($lastProductId); ?>">
                        <input type="hidden" id="previousID" >
                        <div class="col-10">
                            <div class="row productslisting branchReplace" id="productslisting" >
                                <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php $productId = \App\Traits\CommonTrait::encodeId($product->pid); ?>
                                <div class="col-sm-3">
                                    <div class="thumbnail">
                                        <a href="<?php echo e(route('product.description',$productId)); ?>">
                                    <img class="lazy" src="<?php echo e(URL::asset('assets/img/PRELOADER.jpg')); ?>" data-src="<?php echo e(URL::asset('storage/images/'. $product->images)); ?>" alt="Fjords" style="width:100%">
                                    <div class="caption">
                                        <p style="color: #000000;"><?php echo e($product->pname); ?></p>
                                        <p >$<?php echo e($product->product_price); ?><span>MXN</span></p>
                                        <p >Recibelo Mañana</p>
                                    </div></a>
                                    </div>
                                </div>
                                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            </div>








                            <!-- <div class="row my-5">
                                <div class="col-12">
                                <div class="text-center">
                                    <button class="see_more">Ver mas</button>
                                </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
               </div>
            </section>
         <!-- infinite scroll  -->
      <!-- </div> -->

       

      <!-- dekstop-view  -->

<div class="mobile_view">
    <div id="myHeader" class="category_top">
        <div class="category_title">
            <div><a class="back" href="<?php echo e(url('/')); ?>"><img src="<?php echo e(asset('assets/img/back.png')); ?>"></a></div>
            <div>
                <h2>Almacenamiiento y organizacion</h2>
                <span class="total product">11 Productors</span>
            </div>
            <div>
                <div class="dropdown cart-dropdown">
                    <a href="<?php echo e(route('cart.list')); ?>" class="dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-display="static">
                        <img src="<?php echo e(asset('assets/img/2 icon gray.png')); ?>">
                        <!--<span class="cart-count">2</span>-->
                    </a>
                </div>
                <!-- End .dropdown -->
            </div>
        </div>
        <section class="filter_Sec">
            <div class="select-custom">
                <select name="orderby" class="form-control">
                    <option value="menu_order" selected="selected">ordenar</option>
                    <option value="popularity">1</option>
                    <option value="rating">2</option>
                    <option value="date">3</option>
                    <option value="price">4</option>
                    <option value="price-desc">5</option>
                </select>
            </div>



        </section>
    </div>

    <section class="container-fluid item-product" style="display:block;">
        <div class="row row-sm">
            <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <?php $productId = \App\Traits\CommonTrait::encodeId($product->pid); ?>
                <div class="col-6 col-md-4 col-lg-3 col-xl-2">
                    <div class="product-default">
                        <figure>
                            <a href="<?php echo e(route('product.description',$productId)); ?>">
                                <img class="lazy" src="<?php echo e(URL::asset('assets/img/PRELOADER.jpg')); ?>" data-src="<?php echo e(URL::asset('storage/images/'. $product->images)); ?>">
                            </a>
                        </figure>
                        <div class="product-details">
                            <h2 class="product-title">
                                <a href=""><?php echo e($product->pname); ?></a>
                            </h2>
                            <div class="price-box">
                                <span class="product-price"><i>$<?php echo e($product->product_price); ?></i> MXN</span>
                            </div>
                            <!-- End .price-box -->
                            <div class="category-wrap">
                                <div class="category-list">
                                    <p >Recibelo Manana</p>
                                </div>
                            </div>
                        </div>
                        <!-- End .product-details -->
                    </div>
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>

        </div>
    </section>
    <div class="filter-overlay"></div>
</div>


      <script src="<?php echo e(asset('js/jquery.min.js')); ?>"></script>
      <script src="<?php echo e(asset('js/popper.min.js')); ?>"></script>
      <script src="<?php echo e(asset('js/bootstrap.min.js')); ?>"></script>
      <script src="<?php echo e(asset('assets/js/number.js')); ?>"></script>

<script>
    $(document).ready(function () {
        scrollerData();
    });
</script>
<script>
    var scroller = true;
    $(document).ready(function(){
        var i =2;
        $(window).scroll(function() {
            if($(window).scrollTop() >= $('.scrolldown').offset().top + $('.scrolldown').outerHeight() - window.innerHeight) {
                // ajax call get data from server and append to the div
                // if (scroller) {
                var rout = '<?php echo Route::currentRouteName(); ?>';
                var lastId = $('#lastId').val();
                var previousID = $('#previousID').val();
                // alert(rout);
                if (lastId) {
                    if (previousID != lastId) {
                        $('#previousID').val(lastId);
                        $.ajax(
                            {
                                url: '<?php echo e(route('get.more.data.category')); ?>',
                                type: 'GET',
                                data: {
                                    "page": i,
                                    'currentriute': rout,
                                    'lastId': lastId
                                },
                                success: function (result) {
                                    if (result.success) {
                                        // $('.productslisting').html();

                                        // toastr.success('Cart updated successfully');
                                        $('.productslisting').append(result.data);
                                        $('#lastId').val(result.lastProductId);

                                    } else {
                                        return false;
                                    }
                                }
                            });
                        i = parseInt(i) + 1;
                    }
                }
                else{
                    return false;
                }
            }
            else{
                return false;
            }
            // }
        });
    });
    // function scrollerupdate() {
    //     scroller = false;
    // }
</script>




























   </body>
</html><?php /**PATH C:\xampp\htdocs\ganga_new\ganga_box_new\resources\views/product/product_list.blade.php ENDPATH**/ ?>