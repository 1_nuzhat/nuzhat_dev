
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layouts.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<style>
          p {
              margin: 0;
          }
          .box_1_txt {
            width: 50px;
    height: 50px;
    border: 1px solid #b7b4b4;
    border-radius: 5px;
          }
          .txt_2 {
            background: #e1e1e1;
    position: absolute;
    padding: 0px 8px;
    border-radius: 50%;
    top: -10px;
    right: -10px;
          }
          .img_1 {
            position: relative;
    display: inline-block;
          }
          .divider {
            background: #e1e1e1;
    width: 100%;
    height: 2px;
    margin-top: 20px;
          }
       </style>
<div class="page-wrapper">
   <!-- ============================================================== -->
   <!-- ============================================================== -->
   <!-- Bread crumb and right sidebar toggle -->
   <!-- ============================================================== -->
   <div class="row">
      <!-- Column -->
      <div class="col-lg-12">
         <div class="card">
            <div class="card-body">
            </div>
         </div>
      </div>
   </div>
   <div class="page-breadcrumb">
      <div class="row">
         <div class="col-5 align-self-center">
           <!--  <h4 class="page-title">View Order</h4> -->
            <div class="d-flex align-items-center">
               <nav aria-label="breadcrumb">
                  <!-- <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="#">Home</a></li>
                     <li class="breadcrumb-item active" aria-current="page">Library</li>
                  </ol> -->
               </nav>
            </div>
         </div>
      </div>
   </div>
   <!-- ============================================================== -->
   <!-- End Bread crumb and right sidebar toggle -->
   <!-- Container fluid  -->
   <!-- ============================================================== -->
    <div class="container">
          <div class="row">
              <div class="col-lg-12">
                 <div class="card">
               <div class="card-body">
                <div class="form">
                    <div class="form-group ">
                      <h3 class="card-title"><?php echo e($order_number->order_number); ?>

                  </h3>
                  <p>
                     <?php echo e(date('F d, Y h:i A', strtotime($order_number->order_date))); ?>

                  </p>
                    
                  <!--   <h4>No preparado</h4> -->
                  <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                   <?php if($order->orderDetails): ?>
                    <?php $__currentLoopData = $order->orderDetails->product; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $value): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?> 
                       
                       <div class="row mt-4">
                          <div class="col-2">
                              <div class="img_1">
                                <div class="box_1_txt">
                                    <?php if(!empty($value->ProductVariant)): ?>
                                        <img src="<?php echo e(URL::asset('storage/images/'. $value->ProductVariant->productImage->images)); ?>" width="50" height="auto">
                                        <?php endif; ?>
                                </div>
                                <p class="txt_2"><?php echo e($order->orderDetails['quantity']); ?></p>
                              </div>
                             
                          </div>
                          <div class="col-6">
                             <p><?php echo e($value['name']); ?></p>
                             <p>SKU : <?php echo e($order->orderDetails['product_sku']); ?></p>
                          </div>
                          <div class="col-2">
                              <p>$<?php echo e($order->orderDetails['price']); ?>.00 x <?php echo e($order->orderDetails['quantity']); ?></p>
                          </div>
                          <div class="col-2 text-right ">
                             <p>$<?php echo e($order->orderDetails['quantity'] * $order->orderDetails['price']); ?>.00</p>
                          </div>
                       </div>
                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    <?php endif; ?>
                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                    </div>
                  
                 </div>
              </div>
            
          </div>
      </div>
    </div>
     
          <div class="row">
              <div class="col-lg-12">
                <div class="card">
               <div class="card-body">
                <div class="form">
                    <div class="form-group ">
                     <!--   <h4>Pendiente</h4> -->
                       
                       
                       <div class="row mt-4">
                          <div class="col-2">
                             <p>Subtotal</p>
                          </div>
                          <div class="col-6">
                             <p><?php echo e($subQuant); ?> quantity</p>
                          </div>
                          <div class="col-4 text-right ">
                             <p>$<?php echo e($subTotal); ?>.00</p>
                          </div>
                       </div>
                       <div class="row">
                          <div class="col-2">
                             <p>Shipments</p>
                          </div>
                          <div class="col-6">
                             <p>Delivery for Saturday during the day</p>
                          </div>
                          <div class="col-4 text-right ">
                             <p> $<?php echo e($orderShipping); ?>.00</p>
                          </div>
                       </div>
                       <!-- <div class="row">
                          <div class="col-2">
                             <p>Impuesto</p>
                          </div>
                          <div class="col-6">
                             <p>VAT 16%</p>
                          </div>
                          <div class="col-4 text-right ">
                             <p>$25.66</p>
                          </div>
                       </div> -->
                       <div class="row">
                          <div class="col-8">
                             <p><b>Total</b></p>
                          </div>
                          <div class="col-4 text-right ">
                             <p><b> $<?php echo e($orderTotal); ?>.00</b></p>
                          </div>
                       </div>
                       <!-- <div class="border"></div>
                        <div class="border"></div> -->
                       
                    </div>
                 </div>
              </div>
            
          </div>
      </div>
    </div>
    <div class="row">
              <div class="col-lg-12">
                <div class="card">
               <div class="card-body">
                <div class="form">
                    <div class="form-group ">
                     <!--   <h4>Pendiente</h4> -->
                       
                       
                       <div class="row mt-4">
                          <div class="col-2">
                             <p>Name</p>
                             <p>Email Id</p>
                          </div>
                          <div class="col-6">
                             <p>  <?php echo e($orderAddress['name']); ?>  </p>
                             <p>  <?php echo e($orderAddress['email']); ?></p>
                          </div>
                          
                       </div>
                       <div class="row">
                          <div class="col-2">
                             <p>Address</p>
                          </div>
                          <div class="col-6">
                             <p><?php echo e($orderAddress['street']); ?>

                                <?php echo e($orderAddress['exteriorNumber']); ?>

                                <?php echo e($orderAddress['streetNumber']); ?>

                                <?php echo e($orderAddress['postalCode']); ?><br/>
                                <?php echo e($orderAddress['state']); ?>

                                <?php echo e($orderAddress['reference']); ?>

                              </p>
                          </div>
                         
                       </div>
                       <div class="row">
                          <div class="col-2">
                             <p>Contact Number</p>
                          </div>
                          <div class="col-6">
                             <p>
                              <?php echo e($orderAddress['mobile']); ?></p>
                          </div>
                         
                       </div>
                       
                    </div>
                 </div>
              </div>
            
          </div>
      </div>
    </div>
       
    </div>
   <!-- ============================================================== -->
   <!-- End Container fluid  -->
</div>
<?php echo $__env->make('layouts.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ganga_new\ganga_box_new\resources\views/admin/order_view.blade.php ENDPATH**/ ?>