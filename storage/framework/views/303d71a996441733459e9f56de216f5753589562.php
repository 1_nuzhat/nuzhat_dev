<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layouts.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

<style type="text/css">
   #user1 {
   padding: 15px;
   border: 1px solid #666;
   background: #fff;
   display: none;
   }
   #formButton {
   display: block;
   margin-right: auto;
   margin-left: auto;
   }

  .multiselect {
  width: 200px;
}

.selectBox {
  position: relative;
}

.selectBox select {
  width: 100%;
  font-weight: bold;
}

.overSelect {
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
}

#checkboxes {
  display: none;
  border: 1px #dadada solid;
}

#checkboxes label {
  display: block;
}

#checkboxes label:hover {
  background-color: #1e90ff;
}
</style>

<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
   <div class="row">
      <div class="col-5 align-self-center">
         <h4 class="page-title">ADD PRODUCT</h4>
         <div class="d-flex align-items-center">
            <nav aria-label="breadcrumb">
               <ol class="breadcrumb">
                  <li class="breadcrumb-item"><a href="#">Home</a></li>
                  <li class="breadcrumb-item active" aria-current="page">Library</li>
               </ol>
            </nav>
         </div>
      </div>
   </div>
   <!-- Container fluid  -->
   <!-- ============================================================== -->
   <div class="container-fluid">
      <!-- ============================================================== -->
      <!-- Start Page Content -->
      <!-- ============================================================== -->
      <!-- Row -->
      <div class="row">
         <?php if(session('status')): ?>
         <div class="alert alert-success alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo e(session('status')); ?>

         </div>
         <?php endif; ?>
         <?php if(session('success')): ?>
         <div class="alert alert-success alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo e(session('success')); ?>

         </div>
         <?php endif; ?>
         <?php if(session('error')): ?>
         <div class="alert alert-danger alert-dismissable">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <?php echo e(session('error')); ?>

         </div>
         <?php endif; ?>   
         <div class="col-12">
            <div class="card">
               <form class="form-horizontal" action="<?php echo e(url('/product')); ?>" method="POST" id="user_form" enctype="multipart/form-data">
                  <?php echo e(csrf_field()); ?>

                  <div class="card-body">
                     <h4 class="card-title"> ADD PRODUCT</h4>
                     <div class="form-group row" id="list1" >
                        <label for="cat_name" class="col-sm-3 text-right control-label col-form-label anchor">CATEGORIES:</label>
                         <div class="multiselect">
                         <div class="selectBox" onclick="showCheckboxes()">
                           <select>
                             <option>Select Category</option>
                           </select>
                           <div class="overSelect"></div>
                         </div>
                          
                         <div id="checkboxes">
                            <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <input type="checkbox" id="cat_name"  value="<?php echo e($category->id); ?>" name="cat_name[]" />&nbsp; <?php echo e($category->cat_name); ?><br/>
                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                         </div>
                         
                       </div>
                      <!--   <div class="col-sm-9 items">
                           <select class="form-control <?php $__errorArgs = ['cat_name'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>" name="cat_name" multiple>
                              <option disabled selected><?php echo e(__('Please select Category')); ?></option>
                              <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                              <option value="<?php echo e($category->id); ?>" <?php echo e($category->id == old('category') ? 'selected' : ''); ?>><?php echo e($category->cat_name); ?></option>
                              <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                           </select>
                           <?php if($errors->has('cat_name')): ?>
                           <span class="text-danger"><?php echo e($errors->first('cat_name')); ?></span>
                           <?php endif; ?>
                        </div> -->
                     </div>
                     <div class="form-group row">
                        <label for="name" class="col-sm-3 text-right control-label col-form-label">NAME:</label>
                        <div class="col-sm-9">
                           <input type="text" class="form-control" id="name" name="name" placeholder="NAME" >
                           <?php if($errors->has('name')): ?>
                           <span class="text-danger"><?php echo e($errors->first('name')); ?></span>
                           <?php endif; ?>
                        </div>
                     </div>
                     <div class="form-group row">
                        <label for="description" class="col-sm-3 text-right control-label col-form-label">PRODUCT DESCRIPTION:</label>
                        <div class="col-sm-9">
                           <textarea class="ckeditor form-control" name="description"></textarea>
                           <?php if($errors->has('description')): ?>
                           <span class="text-danger"><?php echo e($errors->first('description')); ?></span>
                           <?php endif; ?>
                        </div>
                     </div>
                      <div class="form-group row">
                          <label for="description" class="col-sm-3 text-right control-label col-form-label">PRODUCT IMAGES<br>(You choose multiple image):</label>
                          <div class="col-sm-9">
                             <input type="file" name="product_image[]" class="form-control" multiple>
                              <?php if($errors->has('product_image')): ?>
                                  <span class="text-danger"><?php echo e($errors->first('product_image')); ?></span>
                              <?php endif; ?>
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="description" class="col-sm-3 text-right control-label col-form-label">IMAGE SIZE FORMAT AVAILABLE:</label>
                          <div class="col-sm-9">
                              <input type="text" class="form-control" name="size_desc">
                              <?php if($errors->has('size_desc')): ?>
                                  <span class="text-danger"><?php echo e($errors->first('size_desc')); ?></span>
                              <?php endif; ?>
                          </div>
                      </div>
                      <div class="form-group row">
                          <label for="description" class="col-sm-3 text-right control-label col-form-label">IMAGE DESCRIPTION:</label>
                          <div class="col-sm-9">
                              <textarea class="ckeditor form-control" name="image_desc"></textarea>
                              <?php if($errors->has('image_desc')): ?>
                                  <span class="text-danger"><?php echo e($errors->first('image_desc')); ?></span>
                              <?php endif; ?>
                          </div>
                      </div>
                    
                     <div class="form-group row">
                        <label for="product_id" class="col-sm-3 text-right control-label col-form-label">PRODUCT ID:</label>
                        <div class="col-sm-9">
                           <input type="text" class="form-control" id="product_id" name="p_id" placeholder="PRODUCT ID">
                           <?php if($errors->has('p_id')): ?>
                           <span class="text-danger"><?php echo e($errors->first('p_id')); ?></span>
                           <?php endif; ?>
                        </div>
                     </div>
                     <div class="form-group row">
                        <label for="" class="col-sm-3 text-right control-label col-form-label">PRODUCT STATUS:</label>
                        <div class="col-sm-9">
                           <select id="p_status" name="p_status" class="form-control <?php $__errorArgs = ['p_status'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>">
                              <option disabled selected>--- Select Product Status---</option>
                              <option value="1">Yes</option>
                              <option value="0">No</option>       
                           </select>
                           <?php if($errors->has('p_status')): ?>
                           <span class="text-danger"><?php echo e($errors->first('p_status')); ?></span>
                           <?php endif; ?>
                        </div>
                     </div>
                      <div class="form-group row">
                          <label for="description" class="col-sm-3 text-right control-label col-form-label">PRODUCT SPECIFICATION:</label>
                          <div class="col-sm-9">
                              <textarea class="ckeditor form-control" name="specification"></textarea>
                              <?php if($errors->has('specification')): ?>
                                  <span class="text-danger"><?php echo e($errors->first('specification')); ?></span>
                              <?php endif; ?>
                          </div>
                      </div>
                         <div class="form-group row">
                          <label for="description" class="col-sm-3 text-right control-label col-form-label">VIDEO LINK:</label>
                           <div class="col-sm-9">
                             <input type="text" id="videos" class="form-control"  name="videos" placeholder="Video Link">
                             <?php if($errors->has('videos')): ?>
                             <span class="text-danger"><?php echo e($errors->first('videos')); ?></span>
                             <?php endif; ?>
                        </div>
                      </div>
                     <div class="form-group row">
                        <label for="" class="col-sm-3 text-right control-label col-form-label">PRODUCT TAGS:</label>
                        <div class="col-sm-9">
                           <input type="text" class="form-control" id="title" name="title" placeholder="PPRODUCT TAGS" >
                           <?php if($errors->has('title')): ?>
                           <span class="text-danger"><?php echo e($errors->first('title')); ?></span>
                           <?php endif; ?>
                        </div>
                     </div>
                     <div class="form-group row">
                        <label for="" class="col-sm-3 text-right control-label col-form-label">TAG STATUS:</label>
                        <div class="col-sm-9">
                           <select id="t_status" name="t_status" class="form-control <?php $__errorArgs = ['t_status'];
$__bag = $errors->getBag($__errorArgs[1] ?? 'default');
if ($__bag->has($__errorArgs[0])) :
if (isset($message)) { $__messageOriginal = $message; }
$message = $__bag->first($__errorArgs[0]); ?> is-invalid <?php unset($message);
if (isset($__messageOriginal)) { $message = $__messageOriginal; }
endif;
unset($__errorArgs, $__bag); ?>">
                              <option disabled selected>--- Select Tag Status---</option>
                              <option value="1">Yes</option>
                              <option value="0">No</option>       
                           </select>
                           <?php if($errors->has('t_status')): ?>
                           <span class="text-danger"><?php echo e($errors->first('t_status')); ?></span>
                           <?php endif; ?>
                        </div>
                     </div>
                  </div>
                  <hr>
                  <div class="card-body">
                     <h4 class="card-title"> VARIANT</h4>
                     <div class="container-fluid">
                        <div class="row">
                           <div class="col-10">
                              <!-- Column -->
                              <div class="card">
                                 <div class="card-body">
                                    <button type="button" id="formButton">Add Variants</button>
                                    <div >
                                       <div class="table-responsive">
                                          <table class="table table-striped table-bordered" id="user_data">
                                             <tr>
                                                <th>COLOR</th>
                                                <th>SIZE</th>
                                                <th>BRANCH</th>
                                                <th>IMAGE</th>
                                                 <th>IMAGE2</th>
                                                <th>SKU</th>
                                                <th>PRICE</th>
                                                <th>COMPARE PRICE</th>
                                                <th>COST</th>
                                                <th>QUANTITY</th>
                                                <th>STATUS</th>
                                                <th>ACTION</th>
                                             </tr>
                                          </table>
                                       </div>
                                       <div align="center">
                                       </div>
                                    </div>
                                 </div>
                                 <div id="user1" class="emptyuser">
                                    <b>Color: </b>
                                    <select id="color_name" class="form-control">
                                       <option disabled selected>--- Select Color---</option>
                                       <?php $__currentLoopData = $colors; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $color): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                       <option value="<?php echo e($color->id); ?>"><?php echo e($color->color_name); ?></option>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <span id="error_color_name" class="text-danger"></span>

                                      
                                    <b>Size: </b>
                                    <select id="size" class="form-control">
                                       <option disabled selected>--- Select Size---</option>
                                       <option value="XS">XS</option>
                                       <option value="S">S</option>
                                       <option value="M">M</option>
                                       <option value="L" >L</option>
                                       <option value="XL">XL</option>
                                       <option value="XXL">XXL</option>
                                       <option value="3XL">3XL</option>
                                       <option value="XCH">XCH</option>
                                       <option value="CH">CH</option>
                                       <option value="G">G</option>
                                       <option value="XG">XG</option>
                                       <option value="Tamaño único">Tamaño único</option>
                                    </select>
                                    <span id="error_size" class="text-danger"></span>
                                  
                                   <b>Branch: </b>
                                    <select id="branch_name" class="form-control">
                                       <option disabled selected>--- Select Branch---</option>
                                       <?php $__currentLoopData = $branch; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $branches): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                       <option value="<?php echo e($branches->id); ?>"><?php echo e($branches->name); ?></option>
                                       <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                    </select>
                                    <span id="error_name" class="text-danger"></span>

                                    <span id="images" name="images[]" />
                                    <span id="error_images" class="text-danger"></span>
                                    <br/>
                                    <b>SKU: </b><input type="text" id="product_sku" class="form-control" />
                                    <span id="error_product_sku" class="text-danger"></span>
                                    <b>PRICE: </b><input type="text"  id="product_price" class="form-control" />
                                    <span id="error_product_price" class="text-danger"></span>
                                    <b>COMPARE PRICE: </b><input type="text" id="product_compare_price" class="form-control" />
                                    <span id="error_product_compare_price" class="text-danger"></span>

                                    <b>COST: </b><input type="text"  id="product_cost" class="form-control" />
                                    <span id="error_product_cost" class="text-danger"></span>

                                    <b>QUANTITY: </b>
                                    <input type="text" id="quantity" class="form-control" />
                                     <span id="error_quantity" class="text-danger"></span>
                                   
                                    
                                    <b>STATUS: </b>
                                    <select id="status" class="form-control">
                                       <option disabled selected>--- Select Variant Status---</option>
                                       <option value="1">Yes</option>
                                       <option value="0">No</option>
                                    </select>
                                    <span id="error_status" class="text-danger"></span>
                                    <button type="button" name="addnew" id="addnew" class="btn btn-info">Save</button>
                                 </div>
                                 <div id="action_alert" title="Action">
                                 </div>
                                 <hr>
                                 <div class="card-body">
                                    <div class="form-group mb-0 text-right">
                                       <button type="submit" id="sub" class="btn btn-info waves-effect waves-light">Submit</button>
                                    </div>
                               </div>
               </form>
               </div>
               </div>
               </div>
               </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <!-- End Row -->
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>  
<script>  
   $(document).ready(function() {
   
   var count = 0;
   $("#formButton").click(function() {
    
   
   $("#user1").toggle();
   $('#color_name').val('');
   $('#size').val('');
   $('#branch_name').val('');
   $('#images').prop('file')[0];
   $('#imagesdual').prop('file')[0];
   $('#product_sku').val('');
   $('#product_price').val('');
   $('#product_compare_price').val('');
   $('#product_cost').val('');
   $('#quantity').val('');
   $('#status').val('');
   
   
   
   $('#error_color_name').text('');
   $('#error_size').text('');
   $('#error_branch_name').text('');
   $('#error_images').file('');
   $('#error_product_sku').text('');
   $('#error_product_price').text('');
   $('#error_product_compare_price').text('');
   $('#error_product_cost').text('');
   $('#error_quantity').text('');
   $('#error_status').text('');
   
   
   $('#addnew').text('Save');
   });
   
   $('#addnew').click(function(){
   var error_color_name = '';
   var error_size = '';
   var error_branch_name = '';
   var error_images = '';
   var error_product_sku = '';
   var error_product_price = '';
   var error_product_compare_price = '';
   var error_product_cost = '';
   var error_quantity = '';
   var error_status = '';
   
   
   var color_name = '';
   var size = '';
   var branch_name = '';
   var images = '';
   var imagesdual = '';
   var product_sku = '';
   var product_price = '';
   var product_compare_price = '';
   var product_cost = '';
   var quantity = '';
   var status = '';
   
   // if($('#color_name').val() == '')
   // {
   // error_color_name = 'Color Name is required';
   // $('#error_color_name').text(error_color_name);
   // $('#color_name').css('border-color', '#cc0000');
   // color_name = '';
   // }
   // else
   // {
   // error_color_name = '';
   // $('#error_color_name').text(error_color_name);
   // $('#color_name').css('border-color', '');
    color_name = $('#color_name').val();
    color_text = $('#color_name option:selected').text();
   // }

  
   size = $('#size').val();
   
   branch_name = $('#branch_name').val();
   branch_text = $('#branch_name option:selected').text();

   images = $('#images').attr('src');
  
   product_sku = $('#product_sku').val();
  
   product_price = $('#product_price').val();
   
   product_compare_price = $('#product_compare_price').val();
 
   product_cost = $('#product_cost').val();
 
   quantity = $('#quantity').val();
   
   status = $('#status').val();

   
   if(error_color_name != '' || error_size != ''|| error_branch_name != ''|| error_images != ''|| error_product_sku != ''|| error_product_price != ''|| error_product_compare_price != ''|| error_product_cost != ''|| error_quantity != '' ||  error_status != '')
   {
   return false;
   }
   else

  ``
   {
    
   if($('#addnew').text() == 'Save')
   {
    var pro_color = color_name == null ? '' : color_name;
    var pro_size = size == null ? '' : size;
    var pro_status = status == null ? '1' : status;   
    count = count + 1;
    output = '<tr id="row_'+count+'">';
    
    output += '<td>'+color_text+' <input type="hidden" class="form-control" name="color_name[]" id="color_name'+count+'" value="'+pro_color+'" /></td>';

    output += '<td>'+pro_size+' <input type="hidden" name="size[]" id="size'+count+'" value="'+pro_size+'" /></td>';

    output += '<td>'+branch_text+' <input type="hidden" name="branch_name[]" id="branch_name'+count+'" value="'+branch_name+'" /></td>';
    
    output += '<td> <input id="images" type="file" name="images[]" /></td>';

    output += '<td> <input id="imagesdual" type="file" name="imagesdual[]" /></td>';

    output += '<td>'+product_sku+' <input type="hidden" name="product_sku[]" id="product_sku'+count+'" value="'+product_sku+'" /></td>';
    output += '<td>'+product_price+' <input type="hidden" name="product_price[]" id="product_price'+count+'" value="'+product_price+'" /></td>';
   
    output += '<td>'+product_compare_price+' <input type="hidden" name="product_compare_price[]" id="product_compare_price'+count+'" value="'+product_compare_price+'" /></td>';
   
    output += '<td>'+product_cost+' <input type="hidden" name="product_cost[]" id="product_cost'+count+'" value="'+product_cost+'" /></td>';
   
    output += '<td>'+quantity+' <input type="hidden" name="quantity[]" id="quantity'+count+'" value="'+quantity+'" /></td>';

   //  output += '<td>'+product_qty_guadalajara+' <input type="hidden" name="product_qty_guadalajara[]" id="product_qty_guadalajara'+count+'" value="'+product_qty_guadalajara+'" /></td>';
   
   //  output += '<td>'+product_qty_monterrey+' <input type="hidden" name="product_qty_monterrey[]" id="product_qty_monterrey'+count+'" value="'+product_qty_monterrey+'" /></td>';
   
   // output += '<td>'+product_qty_puebla+' <input type="hidden" name="product_qty_puebla[]" id="product_qty_puebla'+count+'" value="'+product_qty_puebla+'" /></td>';

   output += '<td>'+pro_status +' <input type="hidden" name="status[]" id="status'+count+'" value="'+pro_status+'" /></td>';
    output += '<td><button type="button" name="remove_details" class="btn btn-danger btn-xs remove_details" id="'+count+'">Remove</button></td>';
    output += '</tr>';
    $('#user_data').append(output);
   }
   else
   {
    var row_id = $('#hidden_row_id').val();
    
    output += '<td>'+color_name+' <input type="hidden" name="color_name[]" id="color_name'+row_id+'" value="'+color_name+'" /></td>';
    output += '<td>'+size+' <input type="hidden" name="size[]" id="size'+row_id+'" value="'+size+'" /></td>';

    output += '<td>'+branch_text+' <input type="hidden" name="branch_name[]" id="branch_name'+count+'" value="'+branch_name+'" /></td>';

    output += '<td><img src= "/storage/images'+images+' type="hidden" name="images[]"></td>';
    output += '<td><img src= "/storage/images'+imagesdual+' type="hidden" name="imagesdual[]"></td>';
    output += '<td>'+product_sku+' <input type="hidden" name="product_sku[]" id="product_sku'+row_id+'" value="'+product_sku+'" /></td>';
    output += '<td>'+product_price+' <input type="hidden" name="product_price[]" id="product_price'+row_id+'" value="'+product_price+'" /></td>';
    output += '<td>'+product_compare_price+' <input type="hidden" name="product_compare_price[]" id="product_compare_price'+row_id+'" value="'+product_compare_price+'" /></td>';
    output += '<td>'+product_cost+' <input type="hidden" name="product_cost[]" id="product_cost'+row_id+'" value="'+product_cost+'" /></td>';

    output += '<td>'+quantity+' <input type="hidden" name="quantity[]" id="quantity'+count+'" value="'+quantity+'" /></td>';
   
    // output += '<td>'+product_qty_guadalajara+' <input type="hidden" name="product_qty_guadalajara[]" id="product_qty_guadalajara'+row_id+'" value="'+product_qty_guadalajara+'" /></td>';
   
    // output += '<td>'+product_qty_monterrey+' <input type="hidden" name="product_qty_monterrey[]" id="product_qty_monterrey'+row_id+'" value="'+product_qty_monterrey+'" /></td>';
   
    // output += '<td>'+product_qty_puebla+' <input type="hidden" name="product_qty_puebla[]" id="product_qty_puebla'+row_id+'" value="'+product_qty_puebla+'" /></td>';
   
     output += '<td>'+status+' <input type="hidden" name="status[]" id="status'+row_id+'" value="'+status+'" /></td>';
   
   
    output += '<td><button type="button" name="remove_details" class="btn btn-danger btn-xs remove_details" id="'+row_id+'">Remove</button></td>';
    $('#row_'+row_id+'').html(output);
   }
   $("#user1").toggle('close');
   }
   $(".emptyuser").clear();
   });
   
   $(document).on('click', '.remove_details', function(){
   var row_id = $(this).attr("id");
   if(confirm("Are you sure you want to remove this row data?"))
   {
   $('#row_'+row_id+'').remove();
   }
   else
   {
   return false;
   }
   });
   
   $('#action_alert').toggle({
   autoOpen:false
   });
   
   });
   
</script>
<script>

   var checkList = document.getElementById('list1');
checkList.getElementsByClassName('anchor')[0].onclick = function(evt) {
  if (checkList.classList.contains('visible'))
    checkList.classList.remove('visible');
  else
    checkList.classList.add('visible');
}
</script>
<script type="text/javascript">
    var expanded = false;

function showCheckboxes() {
  var checkboxes = document.getElementById("checkboxes");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}
</script>     
<?php echo $__env->make('layouts.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo e(asset('ckeditor/ckeditor.js')); ?>"></script>
<script>CKEDITOR.replace('article-ckeditor');</script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ganga_new\ganga_box_new\resources\views/admin/add_product.blade.php ENDPATH**/ ?>