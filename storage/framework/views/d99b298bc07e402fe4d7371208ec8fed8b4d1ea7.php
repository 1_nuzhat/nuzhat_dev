
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layouts.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


 <!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
<!-- ============================================================== -->
<!-- ============================================================== -->
 <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
          
           
            <div class="row">
                    <!-- Column -->
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">

                    </div>
                </div>
            </div>
        </div>
       
        
            <div class="page-breadcrumb">
                <div class="row">
                    <div class="col-5 align-self-center">
                        <h4 class="page-title">Order List</h4>
                        <div class="d-flex align-items-center">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Library</li>
                                </ol>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->

            <!-- Container fluid  -->
            <!-- ============================================================== -->

           <div class="row">
                <!-- Column -->
              <div class="col-lg-12">
                <div class="card">
                  <div class="card-body">

                <form name="myForm" action="<?php echo e(route('statusImport')); ?>" method="POST" enctype="multipart/form-data"  onsubmit="return validateForm()">
                <?php echo csrf_field(); ?>
                <input type="file" name="file" class="form-control">
                <br>
                <button type="submit" class="btn waves-effect waves-light btn-secondary">Import Order Status</button>
                </form>
          
                <span class="btn waves-effect waves-light btn-secondary" id="export_data">Export Data</span>
                <span class="btn waves-effect waves-light btn-secondary" id="export_status">Export Order Status</span> 
               <button> <a href="<?php echo e(url('/printview')); ?>"target="_blank"class="btnprv btn">Print Preview</a>
               </button>
               <!-- <input type="button" value="PRINT" onclick="PrintDiv();" /> -->
               
                   
                   </div>
                </div>
            </div>
        </div>
       <!--  <button><a href="javascript:window.print()">Click to Print This Page</a></button> -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
                
                <div class="row">
                     <?php if(session('status')): ?>
                        <div class="alert alert-success alert-dismissable">
                           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           <?php echo e(session('status')); ?>

                        </div>
                    <?php endif; ?>

                    <?php if(session('success')): ?>
                        <div class="alert alert-success alert-dismissable">
                           <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                           <?php echo e(session('success')); ?>

                        </div>
                    <?php endif; ?>
                    <!-- Column -->
                    <div class="col-lg-12">
                        <div class="card">
                            
                            <div class="card-body">
                                <div class="table-responsive" id="divToPrint">
                                    <table class="table product-overview" id="zero_config">
                                        <thead>
                                    <tr>
                                        <th><input id="check_all" type="checkbox"></th>
                                        <th>ORDER NUMBER</th>
                                        <th>DATE</th>
                                        <th>CLIENT EMAIL</th>
                                        <th>TOTAL</th>
                                        <th>PAYMENT</th>
                                        <th>STATUS</th>
                                        <th>CHANGE ORDER STATUS</th>
                                        <th>VIEW ORDER DETAILS</th>
                                    </tr>
                                        </thead>
                                        <tbody>
                                              <?php $__currentLoopData = $orders; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $order): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <tr>
                                            
                                              <th><input type="checkbox" class="row_check" name="row_check[]" value="<?php echo e($order->id); ?>"></th>
                                              <td><?php echo e($order->order_number); ?></td>
                                            <td><?php echo e($order->order_date); ?> </td>
                                            <td><?php echo e($order->email); ?></td>
                                            <td>$<?php echo e($order->total); ?>

                                            <input type="hidden" name="" value="<?php echo e($order->id); ?>"></td>

                                           <td><?php echo e($order->pay_by); ?>

                                            </td>
                                            <td> 
                                            <?php if($order->status ==1): ?>
                                               COMPLETED

                                            <?php elseif($order->status ==2): ?>
                                               PREPARED

                                            <?php elseif($order->status ==3): ?>
                                               FIRST DELIVERY ATTEMPT

                                            <?php elseif($order->status ==4): ?>
                                               SECOND DELIVERY ATTEMPT

                                            <?php elseif($order->status ==5): ?>
                                               CANCELLED

                                            <?php else: ?>
                                               CREATED

                                            <?php endif; ?> </td>
                                               <form class="form-horizontal" action="/order_status/<?php echo e($order->id); ?>" method="POST" enctype="multipart/form-data">
                                 <?php echo e(csrf_field()); ?>

                              <?php echo e(method_field('PUT')); ?>

                                                <td>

                           <select id="status" name="status" class="form-control">
                              <option disabled selected>--- Select Order Status---</option>
                              <option value="0">CREATED</option>
                              <option value="1">COMPLETED</option>
                              <option value="2">PREPARED</option>
                              <option value="3">FIRST DELIVERY ATTEMPT</option>
                              <option value="4">SECOND DELIVERY ATTEMPT</option>
                              <option value="5">CANCELLED</option>

                           </select>
                          
                            
                         </td>
                                               
                            <td><a href="<?php echo e(url('orders/'.$order->id)); ?>" target="_blank"> <span class="label label-success font-weight-100">View</span></a><br>
                            </td>
                            <td>
                            <button type="submit" class="btn btn-primary waves-effect waves-light"><?php echo e(__('Status Update')); ?></button>
                          </td>
                          </form>
                                            </tr>
                                           
                                            
                                        </tbody>
                                           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                            
                                    </table>
                                   
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Column -->
                </div>
            </div> 
           
            <!-- ============================================================== -->
            <!-- End Container fluid  -->
        </div>
       
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script>
    $('#check_all').change(function(){
        $('.row_check').prop("checked", $(this).prop("checked"));
    });
</script>

<script type="text/javascript">
$(document).ready(function(){
$('.btnprv').printPage();
});
</script>

<script type="text/javascript">     
    function PrintDiv() {    
       var divToPrint = document.getElementById('divToPrint');
       var popupWin = window.open('', '_blank');
       popupWin.document.open();
       popupWin.document.write('<html><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
        popupWin.document.close();
            }

    $('#export_data').click(function(){

        if(!$('.row_check').is(':checked')){
            alert('Please select atleast one item');
            return false;
        }
        var valueArray = $('.row_check:checked').map(function() {
            return this.value;
        }).get();
        window.location.assign('/exportorder?ids='+valueArray);
    });

    $('#export_status').click(function(){

        if(!$('.row_check').is(':checked')){
            alert('Please select atleast one item');
            return false;
        }
        var valueArray = $('.row_check:checked').map(function() {
            return this.value;
        }).get();
        window.location.assign('/export_status?ids='+valueArray);
    });
 </script>

<script>
  function validateForm() {
    var x = document.forms["myForm"]["file"].value;
    if (x == "") {
      alert("Please Choose file");
      return false;
    }
  }
</script>
<?php echo $__env->make('layouts.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ganga_new\ganga_box_new\resources\views/admin/order_list.blade.php ENDPATH**/ ?>