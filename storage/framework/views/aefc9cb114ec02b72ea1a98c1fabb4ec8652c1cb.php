
<?php echo $__env->make('front_layouts.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>

      <!-- End .header -->
      <!-- end -->
   <!-- slider -->

   <!--  <div id="myCarousel" class="carousel slide" data-ride="carousel">>

        <ul class="carousel-indicators">
          <li data-target="#demo" data-slide-to="0" class="active"></li>
          <li data-target="#demo" data-slide-to="1"></li>
          <li data-target="#demo" data-slide-to="2"></li>
        </ul>

        <div class="carousel-inner">

        <?php $__currentLoopData = $banners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $banner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <div class="carousel-item <?php echo e($loop->first ? 'active' : ''); ?>">
         <img class="d-block img-fluid" src="<?php echo e(asset('storage/images/'.$banner->images)); ?>" >
            <div class="carousel-caption">
               <h3><?php echo e($banner->name); ?></h3>
                <p><?php echo e($banner->description); ?></p>
            </div>
      </div> -->
        <!-- <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
           <?php $__currentLoopData = $banners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $banner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <div class="carousel-item active">

          <img src="<?php echo e(asset('storage/images/'.$banner->images)); ?>" width="100%" height="300">

            <div class="carousel-caption">
              <h3><?php echo e($banner->name); ?></h3>
              <p><?php echo e($banner->description); ?></p>
            </div>
          </div>
           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?> -->
         <!--  <div class="carousel-item">
            <img src="assets/img/banner.png" alt~="Chicago" width="100%" height="300">
            <div class="carousel-caption">
              <h3>Chicago</h3>
              <p>Thank you, Chicago!</p>
            </div>
          </div> -->
         <!--  <div class="carousel-item">
            <img src="assets/img/banner.png" alt="New York" width="100%" height="300">
            <div class="carousel-caption">
              <h3>New York</h3>
              <p>We love the Big Apple!</p>
            </div>
          </div> -->
       <!--  </div>

        <a class="carousel-control-prev" href="#myCarousel">
          <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#myCarousel">
          <span class="carousel-control-next-icon"></span>
        </a>

      </div> -->

    <!-- slider -->
   <!-- slider -->
      <section class="mobile slider ">
          <!-- Modal -->

      <div id="demo" class="carousel slide d-sm-block d-md-none" data-ride="carousel" >
         <ul class="carousel-indicators">
             <?php $k=0 ?>
             <?php $__currentLoopData = $banners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $banner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <li data-target="#demo" data-slide-to="<?php echo e($k); ?>" class="<?php echo e($k==0 ?'active':''); ?>"></li>
                 <?php $k++; ?>
           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
         </ul>
         <div class="carousel-inner">
             <?php $l=0 ?>
             <?php $__currentLoopData = $banners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $banner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
          <div class="carousel-item <?php echo e($l== 0 ? 'active':''); ?>">

          <img src="<?php echo e(asset('storage/images/'.$banner->images)); ?>" width="100%" height="300">

            <!-- <div class="carousel-caption">
              <h3><?php echo e($banner->name); ?></h3>
              <p><?php echo e($banner->description); ?></p>
            </div>  -->
          </div>
                 <?php $l++; ?>
           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>


         </div>
         <a class="carousel-control-prev" href="#demo" data-slide="prev">
         <span class="carousel-control-prev-icon"></span>
         </a>
         <a class="carousel-control-next" href="#demo" data-slide="next">
         <span class="carousel-control-next-icon"></span>
         </a>
      </div>

      <div class="black">
         <ul>
           <li class="active"><img src="<?php echo e(asset('assets/img/FLOATING-BAR3.jpg')); ?>" alt="icon"></li>

           <li><img src="<?php echo e(asset('assets/img/FLOATING-BAR2.jpg')); ?>" alt="icon"></li>

           <li><img src="<?php echo e(asset('assets/img/FLOATING-BAR4.jpg')); ?>" alt="icon"></li>

         </ul>
       </div>
   </section>
      <!-- slider -->
      <!-- sidebar  -->
      <!-- sidebar  -->


    <section class="listing left-nav mb-2">
      <div class="container">
          <div class="row">
            <div class="col-lg-12 mt-2">
               <div class="tab">
                <?php $__currentLoopData = $families; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $family): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php $fid = \App\Traits\CommonTrait::encodeId($family->id) ?>
                <a href="<?php echo e(route('family.products',$fid)); ?>"><button class="tablinks" ><i class="icon"></i><?php echo e($family->name); ?><i class="fa fa-angle-right" ></i></button></a>
                   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                 </div>
                 <div id="London" class="tabcontent">
                     <?php $i =0; ?>



                         <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel" style="display: flow-root;">
                             <ol class="carousel-indicators">
                                 <?php $__currentLoopData = $banners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $banner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                 <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo e($i); ?>" class="<?php echo e($i ==0 ?'active':''); ?>"></li>
                                     <?php $i++; ?>
                                     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                             </ol>
                             <div class="carousel-inner">
                                 <?php $j=0;?>
                                 <?php $__currentLoopData = $banners; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $banner): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                 <div class="carousel-item <?php echo e($j ==0 ?'active':''); ?>">
                                     <img class="d-block w-100" src="<?php echo e(asset('storage/images/'.$banner->images)); ?>" alt="First slide">
                                 </div>
                                     <?php $j++; ?>
<?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                             </div>
                             <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                 <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                 <span class="sr-only">Previous</span>
                             </a>
                             <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                 <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                 <span class="sr-only">Next</span>
                             </a>
                         </div>
                 </div>
           </div>
          </div>
      </div>
  </section>
  <!-- sidebar  -->
      <!-- sidebar  -->

      <section class="footer-dsk">
         <div class="container">
            <div class="row">
               <div class="col-lg-12">
                  <div class="text-center" id="">
                     <p><img src="<?php echo e(asset('assets/img/PRODUCT LIST BAR.jpg')); ?>" style="width: 100%;"></p>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <!-- infinite scroll  -->
<div class="scrolldown">
      <section >
        <div id ="sectionid">
         <div class="container my-3 infinite">
          
            <div class="row p_id branchReplace" id="p_id">

                <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <?php $productId = \App\Traits\CommonTrait::encodeId($product->pid); ?>
                    <div class="col-sm-3">
                        <div class="thumbnail">
                            <a href="<?php echo e(route('product.description',$productId)); ?>">
                                <img class="lazy" src="<?php echo e(URL::asset('assets/img/PRELOADER.jpg')); ?>" data-src="<?php echo e(asset('storage/images/'.$product->images)); ?>" style="width: 100%">
                                <div class="caption">
                                    <p style="color: #000000;"><?php echo e($product->product_name); ?></p>
                                    <p >$<?php echo e($product->product_price); ?><span>MXN</span></p>
                                    <?php $__currentLoopData = $msg; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $msgs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <p ><?php echo e($msgs->title); ?></p>
                                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                </div>
                            </a>
                        </div>
                    </div>
                    <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <input type="hidden" id="lastId" value="<?php echo e($lastProductId); ?>">
                <input type="hidden" id="previousId" >















             </div>

            </div>

         </div>

      </section>

</div>
      <!-- infinite scroll  -->
      <!-- popup -->
         <div class="overlayer" style="display: none;">
            <div class="popup">
               <div class="login">
                  <section >
                     <div class="header-middle">
                        <div class="container">
                           <div class="row align-items-center">
                              <div class="col-4">
                                 <div class="header-left">
                                    <i class='fas fa-times ml-2 ' id="hide" style="font-size: 20px;"></i>
                                 </div>
                              </div>
                              <div class="col-8">
                                 <a href="index.html" class="logo">
                                    <img src="assets/img/logo.png" alt="Logo">
                                    </a>
                              </div>
                           </div>

                           <!-- End .header-left -->

                           <!-- End .header-right -->
                        </div>
                        <!-- End .container-fluid -->
                     </div>
                    <!-- End .header-middle -->
                  </section>
                  <div class="container-fluid mt-3">

                     <div class="row">
                        <div class="col-12">
                            <ul class="nav nav-tabs tab_1">
                                <li class="active"><a data-toggle="tab" href="#home">INGRESAR</a></li>
                                <li><a data-toggle="tab" href="#menu1">REGISTRARSE</a></li>
                              </ul>

                              <div class="tab-content">
                                <div id="home" class="tab-pane in active">
                                    <form class="form">
                                        <div class="row">
                                            <div class="col-12">
                                                <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="Correo electronico">
                                            </div>
                                            <div class="col-12">
                                                <div class="input-group mb-2 mr-sm-2">
                                                <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Contrasena">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row my-3">
                                            <div class="col-12 text-center">
                                                <a href="#" class="orange">Iniciar sesion</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div id="menu1" class="tab-pane">
                                    <form class="form">
                                        <div class="row">
                                            <div class="col-12">
                                                <input type="text" class="form-control mb-2 mr-sm-2" id="inlineFormInputName2" placeholder="Correo electronico">
                                            </div>
                                            <div class="col-12">
                                                <div class="input-group mb-2 mr-sm-2">
                                                <input type="text" class="form-control" id="inlineFormInputGroupUsername2" placeholder="Contrasena">
                                                </div>
                                            </div>

                                        </div>
                                        <div class="row my-3">
                                            <div class="col-12 text-center">
                                                <a href="#" class="orange">REGISTRARSE</a>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                              </div>

                              <div class="row my-3">
                                <div class="col-12 text-center line">
                                    <p><span class="pull-left"></span>o<span class="pull-right"></span></p>
                                </div>
                            </div>
                            <div class="row my-3">
                                <div class="col-12 text-center">
                                    <a href="#" class="facebook"><i class="fab fa-facebook-square"></i> Iniciar sesion con Facebook</a>
                                </div>
                            </div>
                            <div class="row my-3">
                                <div class="col-12 text-center">
                                    <a href="#" class="gmail">
                                        <img src="assets/img/gmail.png" width="30px" style="margin-top: -6px;">
                                        <span>Iniciar sesion con Gmail</span>
                                    </a>
                                </div>
                            </div>
                            <div class="row my-3">
                                <div class="col-12 text-center grey">
                                    <p>AI inciar sesion usted acepta <br> los <span class="red"><a href="#">treminos y condiciones y politicas de privacidad</a></span> </p>
                                </div>
                            </div>
                        </div>
                     </div>
                  </div>
               </div>

            </div>
         </div>
      <!-- popup -->
      <!-- footer  -->
      <footer>
         <div class="container">
            <div class="row">
               <div class="col-lg-6 col-12">
                  <p>Rasterio de Predidos y dudas sobre nuestros articulos</p>
                  <p>Mensaje de WhatsApp: 55 8732<br>2760</p>
               </div>
               <div class="col-lg-6 col-12">
                  <img src="assets/img/app-store.png" width="150px">
                  <img src="assets/img/google.png" width="150px">
               </div>
            </div>
            <div class="row">
               <div class="col-lg-6"></div>
               <div class="col-lg-6">
                  <p>Menu Inferior</p>
                  <ul>
                     <li><a href="#">Busquesda</a></li>
                     <li><a href="#">Preguntas Frecuentes</a></li>
                     <li><a href="#">Politica de privacidad</a></li>
                     <li><a href="#">Envio Y devoluciones</a></li>
                     <li><a href="#">Quiesnes somos</a></li>
                  </ul>
               </div>
            </div>
            <div class="row">
               <div class="col-12">
                  <p>Atencion Al Cliente (Cambios)</p>
                  <p>Mensaje de WhatsApp: 55 8029<br>8963</p>
               </div>
            </div>
         </div>
      </footer>
      <!-- footer  -->
      <section class="container-fluid item-product fproductlist">
         <div class="row row-sm ">
             <?php $__currentLoopData = $products; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $product): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                 <?php $productId = \App\Traits\CommonTrait::encodeId($product->pid); ?>
                 
         <div class="col-6 col-md-4 col-lg-3 col-xl-2">
             <a href="<?php echo e(route('product.description',$productId)); ?>">

             <div class="product-default">
              
               <figure>
                   <a href="<?php echo e(route('product.description',$productId)); ?>">
                   <img class="lazy" src="<?php echo e(URL::asset('assets/img/PRELOADER.jpg')); ?>" data-src="<?php echo e(asset('storage/images/'.$product->images)); ?>" style="width: 100%">
                  </a>
               </figure>
               <div class="product-details">
                  <h2 class="product-title">
                     <a style="color: #000000;"><?php echo e($product->product_name); ?></a>
                  </h2>
                  <div class="price-box">
                     <span class="product-price"><i>$<?php echo e($product->product_price); ?></i> MXN</span>
                  </div>
                  <!-- End .price-box -->
                  <div class="category-wrap">
                     <div class="category-list">
                     <?php $__currentLoopData = $msg; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $msgs): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                         <p style="color: #b7b0b0;"><?php echo e($msgs->title); ?></p>
                         <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                     </div>
                  </div>
               </div>
               <!-- End .product-details -->
             
            </div>
         </a>
            
         </div>
           <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </div>
         
        
      </section>


     <!--  <div class="app-noti">
         <a><i class="fa fa-times" aria-hidden="true"></i></a><span><img src="assets/img/app-logo.png"></span>
         <div class="">
            <h4>Gangabox App</h4>
            <span class="sml">Descuentos exclusivos! Solo para app</span><span><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i> (31,046)</span>
         </div>
         <span><a href="" class="app-btn">descargar</a></span>
      </div> -->

      <!-- End .mobile-menu-container -->
<script src="<?php echo e(asset('js/jquery.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/popper.min.js')); ?>"></script>
<script src="<?php echo e(asset('js/bootstrap.min.js')); ?>"></script>
<script src="<?php echo e(asset('assets/js/number.js')); ?>"></script>
<script>
    // $('#exampleModal').modal('toggle');
    // var scroller = true;
    var scroller= true;

    // $('modal').toggle()

    $(document).ready(function(){
        scrollerData();
        // scrollerupdate();
        var i =2;
        $(window).scroll(function () {
                if ($(window).scrollTop() >= $('.scrolldown').offset().top + $('.scrolldown').outerHeight() - window.innerHeight) {
                    // ajax call get data from server and append to the div
                    // if(scroller) {
                    var lastID = $('#lastId').val();
                    var previousId = $('#previousId').val();
                    if (lastID) {
                        if (previousId != lastID) {
                            $('#previousId').val(lastID);
                            $.ajax(
                                {
                                    url: '<?php echo e(route('get.more.data')); ?>',
                                    type: 'GET',
                                    data: {
                                        "page": i,
                                        "lastId": lastID
                                    },
                                    success: function (result) {
                                        if (result.success) {
                                            // $('.p_id').html();
                                            // toastr.success('Cart updated successfully');
                                            $('.p_id').append(result.data);
                                            $('#lastId').val(result.lastProductId);
                                        } else {
                                            $('#lastId').val(result.lastProductId);
                                            return false;
                                        }
                                    }
                                });
                            i = parseInt(i) + 1;
                        }else{
                            return false;
                        }
                    }
                    else{
                        return false;
                    }
                }else{
                    return false;
                }
                // }
            });
    });
    scrollerData();

    //     a blank.gif as the src of images, and include the width and height of the final image.
    //
    // <img src="blank.gif" class="lazy" data-src="/images/full-size.jpg" width="240" height="152">
    //     /* lazyload.js (c) Lorenzo Giuliani
    //      * MIT License (http://www.opensource.org/licenses/mit-license.html)
    //      *
    //      * expects a list of:
    //      * `<img src="blank.gif" data-src="my_image.png" width="600" height="400" class="lazy">`
    //      */



    // function scrollerupdate() {
    //     scroller = false;
    // }
</script>






























<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ganga_new\ganga_box_new\resources\views/index.blade.php ENDPATH**/ ?>