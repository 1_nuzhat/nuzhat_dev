<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="<?php echo e(asset('css/font-awesome.min.css')); ?>">
    <link rel="stylesheet" href="<?php echo e(asset('css/bootstrap.min.css')); ?>">
    <?php if(Route::currentRouteName() == "product.list" || Route::currentRouteName() == "cart.list" || Route::currentRouteName() == "family.product.list" || Route::currentRouteName() == "family.products" || Route::currentRouteName() == "subcat.products" || Route::currentRouteName() == "search.data" || Route::currentRouteName() == "get.branch.product" ): ?>
    <link rel="stylesheet" href="<?php echo e(asset('css/style1.css')); ?>">
    <?php else: ?>
        <link rel="stylesheet" href="<?php echo e(asset('css/style.css')); ?>">

    <?php endif; ?>
    <link rel="stylesheet" type="text/css" href="<?php echo e(asset('css/toastr.css')); ?>">
    <script src='<?php echo e(asset('css/kit.js')); ?>'></script>

    <title>Home</title>
    <?php echo $__env->make('facebook-pixel::head', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s)
{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};
if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];
s.parentNode.insertBefore(t,s)}(window, document,'script',
'https://connect.facebook.net/en_US/fbevents.js');
fbq('init', '306188363328953');
fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=306188363328953&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

</head>
<?php echo $__env->make('facebook-pixel::body', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<body class="index">
<section class="sticky-top">
    <div class="container desk-header ">
        <div class="row">
            <div class="col-lg-3 col-4 pr-0 ">
                <button class="mobile-menu-toggler active" type="button">
                    <img src="<?php echo e(asset('assets/img/mob-menu.png')); ?>">
                </button>
                <a href="<?php echo e(url('/')); ?>"> <img src="<?php echo e(asset('assets/img/logo.png')); ?>" width="150px"></a>
            </div>
            <div class="col-lg-5 col-6">
                <form action="<?php echo e(route('search.data')); ?>" method="post">
                    <?php echo csrf_field(); ?>
                <div class="search-box">
                    <input class="input-text" type="text" name="search" id="searchData" placeholder="Busca tus productos favoritos...">
                    <button class="search-btn js-search-global"><i class="fa fa-search"></i></button>
                </div>
                </form>
                <p class="txt_2">Palabras populares: Relojes  &nbsp;<span>Organizacion</span>  &nbsp;<span>Mascarillas</span></p>
            </div>
            <div class="col-lg-3 col-2 cent">
                <div class="text-right">

                    <ul class="list">
                        <li><img src="<?php echo e(asset('assets/img/Iconos y Banner-08.jpg')); ?>" class="inherit">
<!-- 
 -->
                            <select class="currency-selector branches sele1 minimal" name="branch">
                                <option selected>CDMX</option>
                                <!-- <option >ABC</option> -->
                            </select>
<!--  -->
                        </li>
                        <li>
                            <img src="<?php echo e(asset('assets/img/mobi.gif')); ?>" class="inherit">
                            <select class="currency-selector sele2 minimal">
                                <option selected>App</option>
                                <option >ABC</option>
                            </select>
                        </li>
                        <li>
                            <div id="counter" class="counters">
                                <span class="count"></span>
                            </div>
                            <a href="<?php echo e(url('/cart')); ?>"> <img src="<?php echo e(asset('assets/img/2 icon gray.png')); ?>" ><p style="    margin-bottom: initial;font-size: 17px;color: #949399;font-weight: bold;">Carrito</p></a></li>
<!--  -->
<!--  -->
                    </ul>
                </div>

            </div>
            <div class="col-lg-4 col-4 d-lg-none d-block">
                    <div class="header-right">
                        <ul class="list">
                            <li>
                                <div id="counter" class="counters">
                                    <span class="count">0</span>
                                </div>
                                <a href="<?php echo e(url('/cart')); ?>">  <img src="<?php echo e(asset('assets/img/2 icon gray.png')); ?>" ><p style="font-size: 17px;font-weight: bold;color: #949399;">Carrito</p></a></li>
                        </ul>
                    </div>
                </div>
        </div>
    </div>


</section>
 <!-- For mobile -->

<div class="mobile-menu-overlay">

</div>
<div class="popup_show" style="margin-top: 50%">
<div class="overlayer">

    <div class="popup">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <?php if(env('APP_ENV') == 'local'): ?>
            <a href="<?php echo e(env('Mobile_Site_Local')); ?>" target="_blank"> <button class="btn btn-primary">Ir a version mobile</button></a>
        <?php else: ?>
            <a href="<?php echo e(env('Mobile_Site_Server')); ?>" target="_blank"> <button class="btn btn-primary">Ir a version mobile</button></a>
        <?php endif; ?>
    </div>
    </div>
</div>





















<!-- End .mobil-menu-overlay -->
<div class="mobile-menu-container" >
    <div class="mobile-menu-wrapper">
        <div class="menu-top"> <span><img src="<?php echo e(asset('assets/img/logo.png')); ?>"></span><span class="mobile-menu-close"><img src="<?php echo e(asset('assets/img/close.png')); ?>"></span></div>
        <nav class="mobile-nav minimal" id="submenu">
            <ul class="mobile-menu">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <span class="caret">Categorías</span></a>
                    <ul class="dropdown-menu">
                        <li><a href="listing.html">submenu</a></li>
                        <li><a href="listing.html">submenu</a></li>
                        <li><a href="listing.html">submenu</a></li>
                    </ul>
                </li>
                <li>
                    <a href="listing.html">Más vendido</a>
                </li>

            </ul>
        </nav>
        <!-- End .mobile-nav -->
    </div>
    <!-- End .mobile-menu-wrapper -->
</div>
<script src="<?php echo e(asset('js/jquery.min.js')); ?>"></script>
<script>
    updateCart();
    getFamilySub();
    $('.close').off('click').on('click',function () {
        $('.overlayer').css('display','none');

    });
   function updateCart() {

       $.ajax(
           {
               url: ' <?php echo e(route('get.details')); ?> ',
               type: 'get',
               success: function (result) {
                   if (result.success) {
                       $('.branches').html('');
                       $('.count').text(result.cartTotal.cartcount);
                       // var mapData = JSON.parse(result.data);
                       $('.branches').html(result.data);
                   } else {
                       return false;
                   }
               }
           });


   }
   function getFamilySub()
   {
       $.ajax(
           {
               url: ' <?php echo e(route('get.family.sub')); ?> ',
               type: 'get',
               success: function (result) {
                   if (result.success) {
                       $('#submenu').html('');
                       // $('.count').text(result.cartTotal.cartcount);
                       // var mapData = JSON.parse(result.data);
                       $('#submenu').html(result.data);
                   } else {
                       return false;
                   }
               }
           });
   }

   $('.branches').off('change').on('change',function () {
       var _that = $(this);
       var branch = $('.branches').val();
       var url = $(this).children(":selected").data("url");
       // var _url = _that.attr('data-url');
       window.location = url;
       // $('form#branchedwiseData').submit();
   })
</script>
<script>
    function categoryMobileClick(id) {
        var categoryId = id;
        $('#p_id').empty();
        $.ajax({
            _token: "<?php echo e(csrf_token()); ?>",
            url: "/get_family",
            type: "POST",
            data: {id : categoryId, _token: "<?php echo e(csrf_token()); ?>"},
            success: function(response) {
                $('.fproductlist').html();
                $('.fproductlist').html(response.data);
            }
        });
    }



</script>
<script>
   function scrollerData() {

       !function(window){
           var $q = function(q, res){
                   if (document.querySelectorAll) {
                       res = document.querySelectorAll(q);
                   } else {
                       var d=document
                           , a=d.styleSheets[0] || d.createStyleSheet();
                       a.addRule(q,'f:b');
                       for(var l=d.all,b=0,c=[],f=l.length;b<f;b++)
                           l[b].currentStyle.f && c.push(l[b]);

                       a.removeRule(0);
                       res = c;
                   }
                   return res;
               }
               , addEventListener = function(evt, fn){
                   window.addEventListener
                       ? this.addEventListener(evt, fn, false)
                       : (window.attachEvent)
                       ? this.attachEvent('on' + evt, fn)
                       : this['on' + evt] = fn;
               }
               , _has = function(obj, key) {
                   return Object.prototype.hasOwnProperty.call(obj, key);
               }
           ;

           function loadImage (el, fn) {
               var img = new Image()
                   , src = el.getAttribute('data-src');
               img.onload = function() {
                   if (!! el.parent)
                       el.parent.replaceChild(img, el)
                   else
                       el.src = src;

                   fn? fn() : null;
               }
               img.src = src;
           }

           function elementInViewport(el) {
               var rect = el.getBoundingClientRect()

               return (
                   rect.top    >= 0
                   && rect.left   >= 0
                   && rect.top <= (window.innerHeight || document.documentElement.clientHeight)
               )
           }

           var images = new Array()
               , query = $q('img.lazy')
               , processScroll = function(){
                   for (var i = 0; i < images.length; i++) {
                       if (elementInViewport(images[i])) {
                           loadImage(images[i], function () {
                               images.splice(i, i);
                           });
                       }
                   };
               }
           ;
           // Array.prototype.slice.call is not callable under our lovely IE8
           for (var i = 0; i < query.length; i++) {
               images.push(query[i]);
           };

           processScroll();
           addEventListener('scroll',processScroll);

       }(this);

   }
</script>

















      <!-- End .header -->
      <!-- end --><?php /**PATH C:\xampp\htdocs\ganga_new\ganga_box_new\resources\views/front_layouts/header.blade.php ENDPATH**/ ?>