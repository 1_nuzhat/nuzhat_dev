        <ul class="mobile-menu">
            <?php $b=0;  ?>
            <?php $__currentLoopData = $familySubcats; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $familySub): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <li class="<?php echo e($b == 0 ? 'dropdown': ''); ?>">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"> <span class="caret"><?php echo e($familySub['fname']); ?></span></a>
                <ul class="dropdown-menu">
                    <?php if(count($familySub['subcat']) > 0): ?>
                    <?php $__currentLoopData = $familySub['subcat']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $subcat): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                        <?php $fid = \App\Traits\CommonTrait::encodeId($familySub['fid']); $subId = \App\Traits\CommonTrait::encodeId($subcat->categories_id) ?>
                    <li><a href="<?php echo e(route('family.product.list',[$subId,$fid])); ?>"><?php echo e($subcat->category->cat_name); ?></a></li>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        <?php endif; ?>
                </ul>
            </li>
                    <?php $b++; ?>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        </ul>
<?php /**PATH C:\xampp\htdocs\ganga_new\ganga_box_new\resources\views/product/family_sub.blade.php ENDPATH**/ ?>