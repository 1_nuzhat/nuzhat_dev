
<?php $__env->startSection('content'); ?>
<?php echo $__env->make('layouts.header', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php echo $__env->make('layouts.sidebar', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>


<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
        <!-- ============================================================== -->
        
    <form method="get" action="">
      <div class="row">
                <!-- Column -->
        <div class="col-lg-12">
            <div class="card">
                <div class="card-body">
                  <a class="btn waves-effect waves-light btn-secondary" href="/categories/create">CREATE CATEGORY</a>
                </div>
            </div>
        </div>
    </div>
   
    
        <div class="page-breadcrumb">
            <div class="row">
                <div class="col-5 align-self-center">
                    <h4 class="page-title">CATEGORY LIST</h4>
                    <div class="d-flex align-items-center">
                        <nav aria-label="breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="#">Home</a></li>
                                <li class="breadcrumb-item active" aria-current="page">Library</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Bread crumb and right sidebar toggle -->

        <!-- Container fluid  -->
        <!-- ============================================================== -->
      
           
        <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Start Page Content -->
            <!-- ============================================================== -->
            
            <div class="row">
                <!-- Column -->
                <div class="col-lg-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table product-overview" id="datatable">
                                    <thead>
                                        <tr>
                                            <th>FAMILY NAME</th>
                                            <th>CATEGORY NAME</th>
                                            <th>DESCRIPTION</th>
                                            <th>IMAGES</th>
                                            <th>STATUS</th>
                                         <th>ACTION</th>
                                            
                                        </tr>
                                    </thead>
                                <tbody>
                            <?php $__currentLoopData = $categories; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $category): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr>
                                <td><?php echo e($category->name); ?></td>
                                <td><?php echo e($category->cat_name); ?></td>
                                <td><?php echo e($category->cat_desc); ?></td>
                                <td>

                                    <?php if(!empty($category->images)): ?><img src="<?php echo e(URL::asset('storage/images/'. $category->images)); ?>" width="100" height="auto">
                                    <?php else: ?>
                                    <?php echo e(__('N/A')); ?>

                                     <?php endif; ?>

                                </td>
                                <td> <?php echo e($category->catstatus == '1' ? 'Active' : 'InActive'); ?> </td>
                                  <td> <?php if(!$category->deleted_at): ?>
                                    <a href="categories/<?php echo e($category->id); ?>/edit" class="btn btn-success" data-toggle="tooltip" title="edit" >
                            <i class="fa fa-edit"></i>
                        </a>
                                      <span data-url="<?php echo e(route('delete.category')); ?>" data-catId="<?php echo e($category->id); ?>" data-type="delete" class="btn btn-danger js-removeCategory" data-toggle="tooltip" title="remove" >
                                      <i class="fa fa-trash"></i>
                                      </span>
                                           <?php else: ?>
                                          <span data-url="<?php echo e(route('delete.category')); ?>" data-catId="<?php echo e($category->id); ?>" data-type="restore" class="btn btn-danger js-removeCategory" data-toggle="tooltip" title="remove" >
                                              Restore</span>
                                      <?php endif; ?>
                                  </td>
                        
                            </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                          </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Column -->
            </div>
        </div> 
        </form>
        <!-- ============================================================== -->
        <!-- End Container fluid  -->
    </div>

<script>
$(document).ready(function() {
    $('#datatable').DataTable({"bLengthChange" : false});
});
$('.js-removeCategory').off('click').on('click',function () {
    var _that = $(this);
    var _url = _that.attr('data-url');
    var catId = _that.attr('data-catId');
    var type = _that.attr('data-type');

    if(confirm("Are you sure you want to "+ type+ " this Category ?"))
    {
        $.ajax({
            url: _url,
            type: 'POST',
            data: {_token: "<?php echo e(csrf_token()); ?>" ,catId:catId, type :type},
            success: function (data) {
                if (data['success']) {
                    window.location.reload(true);

                }
            },

        });
    }
    else
    {
        return false;
    }
})
</script>


<?php echo $__env->make('layouts.footer', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.master', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\ganga_new\ganga_box_new\resources\views/admin/category_list.blade.php ENDPATH**/ ?>