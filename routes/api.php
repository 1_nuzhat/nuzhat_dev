<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();


});
// Route::group(['middleware' => ['cors', 'json.response']], function () {
//     // public routes
//     Route::post('/login', 'Auth\ApiAuthController@login')->name('login.api');
//     Route::post('/register','Auth\ApiAuthController@register')->name('register.api');
    
// });
Route::post('/product','ApiProductController@store');
Route::get('/product_list','ApiProductController@view');
Route::get('/variant_list','ApiProductController@product_variant_list');
Route::get('/product_category_list','ApiProductController@view_category_product_list');
Route::get('/subcategory_product_list','ApiProductController@product_list');

Route::get('/family_list','ApiCategoryController@family_collection');
Route::get('/subcategory','ApiCategoryController@category_subcategory_list');
Route::get('/category_list','ApiCategoryController@index');
Route::get('/subcategory_list','ApiCategoryController@subcategory');
Route::get('/list','ApiProductController@variantList');

Route::post('add_cart','ApiCartController@cartAdd')->name('cart.add');
Route::get('list_cart','ApiCartController@cartlist');
Route::post('add_guest_info','ApiCartController@clientInformation')->name('add.guest');
Route::post('edit_cart','ApiCartController@editCart')->name('edit.cart');
Route::post('delete_cart','ApiCartController@deleteCart')->name('delete.cart');
Route::post('add_order_details','ApiCartController@addOrder')->name('add.order.details');
Route::get('list_order_details','ApiCartController@orderList')->name('list.order.details');
Route::get('list_user_info','ApiCartController@listUserInfo')->name('list.user.info');
Route::post('order_cancel','ApiCartController@orderCancel')->name('order.cancel');
Route::get('banner_list','ApiCartController@bannerList')->name('banner.list');
Route::get('country_list','ApiCartController@countrylist')->name('country.list');
Route::get('branch_list','ApiCartController@branchlist')->name('branch.list');
Route::get('payment_details_list','ApiCartController@paymentDetails')->name('payment.details.list');
Route::get('shipping_details_list','ApiCartController@shippingDetails')->name('shipping.details.list');
Route::get('branch_product_list','ApiCartController@branchWiseData')->name('branch.product.list');
Route::get('category-product-list','ApiProductController@categoryProducts')->name('category.product.list');
Route::get('branch-filter','ApiProductController@branchFilter')->name('branch.filter.list');
Route::get('product-filter','ApiProductController@productFilter')->name('product.filter.list');
Route::get('message_list','ApiProductController@message_list');
Route::get('/newproduct_list/{id}','ApiProductController@product_list_new');

Route::get('familyproduct_list','ApiProductController@family_products');



// Route::middleware('auth:api')->group(function () {
//     Route::get('/articles', 'ArticleController@index')->middleware('api.admin')->name('articles');
//     Route::post('/logout', 'Auth\ApiAuthController@logout')->name('logout.api');
// });

