<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/index', function () {
//     return view('index');
// });
// Route::get('/', function () {
//     if (Auth::check()) {
// 	    return redirect('/dashboard');
// 	} else {
// 	    return view('auth/home');
// 	}
// })->name('home');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('admin/dashboard', 'HomeController@adminHome')->name('admin.home')->middleware('is_admin');

Route::group([ 'middleware' => 'auth' ], function() {
Route::get('logout', 'Auth\LoginController@logout');
Route::get('/',"FrontController@index");
Route::get('/dashboard', 'AdminController@index');
// Route::middleware('optimizeImages')->group(function () {
Route::resource('/product','ProductController');
// });
// Route::delete('/product/{id}',function(){
// 	dd('hell');
// });
Route::get('editproject/{id}', 'ProductController@edit')->name('edit-project');
Route::post('update-project', 'ProductController@updateProject')->name('update.project');

Route::get('activeAll', 'ProductController@activeAll');
Route::get('inactiveAll', 'ProductController@inactiveAll');

Route::resource('/AddProduct', 'ProductVariantController');
Route::post('product_tag', 'ProductTagController@index')->name('product.tag');
Route::match(['get', 'post'], '/store_tags', 'ProductTagController@storeTags');

/*Excel import export*/
Route::get('export', 'ProductController@export')->name('export');

Route::resource('/collection', 'CollectionController');

Route::resource('/categories', 'CategoryController');

Route::resource('/orders', 'OrderController');
Route::get('exportorder','OrderController@export')->name('exportorder');
Route::put('/order_status/{id}', 'OrderController@updateStatus');
Route::get('export_status','OrderController@exportStatus')->name('exportstatus');
Route::post('status_import', 'OrderController@import')->name('statusImport');
Route::match(['get', 'post'], '/inventory_details/{orders_id}', 'OrderController@checkout_process');

Route::resource('/banner', 'BannerController');
Route::get('/printview','OrderController@prvpriview');

Route::post('remove_image','ProductController@removeImage')->name('remove_image');


Route::get('get-product-catWise','FrontController@getProductCategoryWise');

Route::get('/cart','cart\CartController@index')->name('cart.list');
Route::get('/thanks','cart\CartController@thanksPage')->name('thanks.page');
Route::get('/product_list','product\ProductController@index')->name('product.list');
Route::get('/sub-cat-product/{id}','product\ProductController@subCatProduct')->name('subcat.products');
Route::post('/product-image-view','product\ProductController@imageView')->name('image.view');
Route::post('/product-size','product\ProductController@productSize')->name('product.size');
Route::post('/add-cart','product\ProductController@addCart')->name('add.cart');
Route::post('/update_cart','cart\CartController@updateCart')->name('update.cart');
Route::post('/remove_cart','cart\CartController@removeCart')->name('remove.cart');
Route::post('/guest_confirm','cart\CartController@guestConfirm')->name('guest.confirm');
Route::post('/order_confirm','cart\CartController@orderConfirm')->name('order.confirm');
Route::get('/get-details','cart\CartController@getDetails')->name('get.details');
Route::get('/product_description/{id}/{code}','product\ProductController@productDescription')->name('product.description');

Route::resource('/country', 'CountryController');
Route::resource('/branch', 'BranchController');
Route::resource('/payment_details', 'PaymentDetailController');
Route::resource('/shipping_details', 'ShippingDetailController');
Route::get('/inventory', 'InventoryController@index');

Route::get('globalinv_export','InventoryController@export')->name('invorder');
Route::get('get_branch_product/{name}','FrontController@branchProductList')->name('get.branch.product');

Route::get('get-family-sub','FrontController@getFamilySub')->name('get.family.sub');
Route::get('family-product-list/{sub}/{fid}','FrontController@familyProductList')->name('family.product.list');
Route::post('/get_family','FamilyController@mobileProduct')->name('get.family');
Route::get('family-products/{id}','FrontController@familyProducts')->name('family.products');
Route::post('/product-video-view','product\ProductController@videoView')->name('video.view');
// Route::post('/delete-products','ProductController@deleteProduct')->name('delete.product');

Route::post('delete-category','CategoryController@destroy')->name('delete.category');
Route::post('delete-banner','BannerController@destroy')->name('delete.banner');
Route::post('import', 'InventoryController@import')->name('import');

Route::get('branch-inventory/{id}', 'InventoryController@branchInventory')->name('branch.inventory');
Route::post('remove-variant', 'ProductController@removeVariant')->name('remove.variant');
Route::post('get-size-price', 'ProductController@getSizePrice')->name('get.size.price');

Route::get('get-more-data', 'product\ProductController@getMoreData')->name('get.more.data');
Route::get('get-more-data-category', 'FrontController@familyMoreData')->name('get.more.data.category');
Route::post('search-data', 'FrontController@searchData')->name('search.data');
Route::resource('/color', 'ColorController');
// Route::resource('/dashboard', 'IndexController');
Route::resource('/message', 'DeliveryMessageController');

/*FbCatalogue excel export*/
Route::get('catalogueexport', 'ProductController@fbexport')->name('fbexport');
Route::get('firebase', 'FirebaseController@sendPushNotification');
Route::resource('/size', 'SizeController');
});

