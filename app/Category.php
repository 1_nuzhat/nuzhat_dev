<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    //
    protected $table = "categories";


      public function category_subcategory()
	{
		return $this->belongsTo('App\CategorySubcategory', 'categories_id', 'id');
	}
}
