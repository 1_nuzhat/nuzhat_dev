<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Family extends Model
{
    //
      protected $table = "family";

      public function category()
	{
		return $this->belongsTo('App\CategorySubcategory', 'family_id', 'id');
	}

}
