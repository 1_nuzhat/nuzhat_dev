<?php

namespace App\Imports;

use App\Orders;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class OrderImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $status = $row['status'];
        $order_status = '';
        switch ($status) {
          case "COMPLETED":
            $order_status = 1;
            break;
          case "PREPARED":
            $order_status = 2;
            break;
          case "FIRST DELIVERY ATTEMPT":
            $order_status = 3;
            break;
          case "SECOND DELIVERY ATTEMPT":
            $order_status = 4;
            break;
          case "CANCELLED":
            $order_status = 5;
            break;
          default:
            $order_status = 0;
        }
        if($row['order_number']){
            $data = array('status' => $order_status);
            $order = Orders::where('order_number', $row['order_number'])->update($data);
            $order_dat = Orders::find($row['order_number']);
            return $order_dat;
        }
    }
}
