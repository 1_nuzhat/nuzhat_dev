<?php

namespace App\Imports;

use App\Inventory;
use App\Product;
use App\ProductVariant;
use App\ProductImage;
use App\Color;
use App\Branch;
use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use DB;

class InventoryImport implements ToModel,WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */


    public function model(array $row)
    {
        $price = $row['product_price'];
        $compare_price = $row['product_compare_price'];
        $cost = $row['product_cost'];
        $status = $row['status'];
        $inv_status = '';
        switch ($status) {
          case "YES":
            $inv_status = 1;
            break;
          default:
            $inv_status = 0;
        }
        if($row['product_sku']){
            $pro =  DB::table('products_variants')->get();
            $inv_branch = Branch::where("name", 'cdmx')->select('id')->first();
            $in_b = $inv_branch->id;
            $cdmxexists = ProductVariant::where('branch_id',$inv_branch->id)->where('product_sku',$row['product_sku'])
             ->first();

         if($cdmxexists){
             if ($row['cdmx']) {
               $in_quantity = $row['cdmx'];
               $data = array('status' => $inv_status,'product_price' => $price,'product_compare_price' => $compare_price,'product_cost' => $cost, 'quantity' => $in_quantity);
               $pro_variant = ProductVariant::where('id', 
               $cdmxexists->id)->update($data);
              
            }
          }


      else{

        if ($row['cdmx']){
          $inv_branch = Branch::where("name", 'cdmx')->select('id')->first();
          $in_b = $inv_branch->id;
          $in_quantity = $row['cdmx']; 
          $product_id = Product::where("name", "like", "%".$row['name']."%")->first();
          $getVarinatId = ProductVariant::where('product_id',$product_id->id)->first();
          $get_image = ProductImage::where('product_variant_id',$getVarinatId->id)->first();
          $color = Color::where("color_name", "like", "%".$row['color_name']."%")->first();
          $productVariantId = ProductVariant::create([                
            'product_id'   => $product_id->id,
            'color_id'   => $color->id,
            'product_sku'  =>  $row['product_sku'],
            'product_price' => $row['product_price'],
            'product_compare_price' => $row['product_compare_price'],
            'product_cost'  =>  $row['product_cost'],
            'size' => $row['size'],
            'branch_id' =>$in_b,
            'quantity' => $row['cdmx'],
            'status' => $inv_status,
        ]);
          $pro = ProductImage::create([
            'product_variant_id'  => $productVariantId->id,
            'images'              => $get_image->images,
        ]);
           }
            }
               $inv_branch = Branch::where("name", 'guadalazara')->select('id')->first();
               $in_b = $inv_branch->id;
               $in_quantity = $row['guadalazara'];
               $guadexists = ProductVariant::where('branch_id',$inv_branch->id)->where('product_sku',$row['product_sku'])->first();
            if($guadexists){
              if ($row['guadalazara']) {
               $data = array('status' => $inv_status,'product_price' => $price,'product_compare_price' => $compare_price,'product_cost' => $cost, 'quantity' => $in_quantity);
               $pro_variant = ProductVariant::where('id',$guadexists->id)
               ->update($data);
            }
          }
        
      else{
        if ($row['guadalazara']){
          $inv_branch = Branch::where("name", 'guadalazara')->select('id')->first();
          $in_b = $inv_branch->id;
          $in_quantity = $row['guadalazara'];
          $product_id = Product::where("name", "like", "%".$row['name']."%")->first();
          $getVarinatId = ProductVariant::where('product_id',$product_id->id)->first();
          $get_image = ProductImage::where('product_variant_id',$getVarinatId->id)->first();
          $color = Color::where("color_name", "like", "%".$row['color_name']."%")->first();
          $productVariantId = ProductVariant::create([                
            'product_id'   => $product_id->id,
            'color_id'   =>   $color->id,
            'product_sku'  => $row['product_sku'],
            'product_price' =>$row['product_price'],
            'product_compare_price' => $row['product_compare_price'],
            'product_cost'  =>  $row['product_cost'],
            'size' => $row['size'],
            'branch_id' =>$in_b,
            'quantity' => $row['guadalazara'],
            'status' => $inv_status,
        ]); 
         
          $pro = ProductImage::create([
            'product_variant_id'  => $productVariantId->id,
            'images'              => $get_image->images,
        ]);
           }
            }
               $inv_branch = Branch::where("name", 'puebla')->select('id')->first();
               $in_b = $inv_branch->id;
               $in_quantity = $row['puebla'];
                $pueblaexists = ProductVariant::where('branch_id',$inv_branch->id)
                ->where('product_sku',$row['product_sku'])->first();
               if($pueblaexists){
                if ($row['puebla']) {
               $data = array('status' => $inv_status,'product_price' => $price,'product_compare_price' => $compare_price,'product_cost' => $cost, 'quantity' => $in_quantity);
               $pro_variant = ProductVariant::where('id', $pueblaexists->id)
                ->update($data);
            }
          }
        
      else{
        if ($row['puebla']){
            $inv_branch = Branch::where("name", 'puebla')->select('id')->first();
            $in_b = $inv_branch->id;
            $in_quantity = $row['puebla'];
            $product_id = Product::where("name", "like", "%".$row['name']."%")->first();
            $getVarinatId = ProductVariant::where('product_id',$product_id->id)->first();
            $get_image = ProductImage::where('product_variant_id',$getVarinatId->id)->first();
            $color = Color::where("color_name", "like", "%".$row['color_name']."%")->first();
            $productVariantId = ProductVariant::create([                
            'product_id'   => $product_id->id,
            'color_id'   => $color->id,
            'product_sku'  =>  $row['product_sku'],
            'product_price' => $row['product_price'],
            'product_compare_price' => $row['product_compare_price'],
            'product_cost'  =>  $row['product_cost'],
            'size' => $row['size'],
            'branch_id' =>$in_b,
            'quantity' => $row['puebla'],
            'status' => $inv_status,
        ]);

            $pro = ProductImage::create([
              'product_variant_id'  => $productVariantId->id,
              'images'              => $get_image->images,
          ]);
           }
            }
               $inv_branch = Branch::where("name", 'monterrey')->select('id')->first();
               $in_b = $inv_branch->id;
               $in_quantity = $row['monterrey'];
               $monexists = ProductVariant::where('branch_id',$inv_branch->id)->where('product_sku',$row['product_sku'])->first();
               if($monexists){
                if ($row['monterrey']) {
               $data = array('status' => $inv_status,'product_price' => $price,'product_compare_price' => $compare_price,'product_cost' => $cost,'quantity' => $in_quantity);
               $pro_variant = ProductVariant::where('id', $monexists->id)
               ->update($data);
            }
          }
       
      else{
        if ($row['monterrey']){
            $inv_branch = Branch::where("name", 'monterrey')->select('id')->first();
            $in_b = $inv_branch->id;
            $in_quantity = $row['monterrey'];
            $product_id = Product::where("name", "like", "%".$row['name']."%")->first();
            $getVarinatId = ProductVariant::where('product_id',$product_id->id)->first();
            $get_image = ProductImage::where('product_variant_id',$getVarinatId->id)->first();
            $color = Color::where("color_name", "like", "%".$row['color_name']."%")->first();
            $productVariantId = ProductVariant::create([                
            'product_id'   => $product_id->id,
            'color_id'   => $color->id,
            'product_sku'  =>  $row['product_sku'],
            'product_price' => $row['product_price'],
            'product_compare_price' => $row['product_compare_price'],
            'product_cost'  =>  $row['product_cost'],
            'size' => $row['size'],
            'branch_id' =>$in_b,
            'quantity' => $row['monterrey'],
            'status' => $inv_status,
        ]);
            $pro = ProductImage::create([
              'product_variant_id'  => $productVariantId->id,
              'images'              => $get_image->images,
          ]);
           }
            }
             $variant_data = ProductVariant::find($row['product_sku']);
            return $variant_data;
        }
}
    // public function headingRow(): int
    // {
    //     return 1;
    // }
}
