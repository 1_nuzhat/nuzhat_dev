<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductTag extends Model
{
    //
    protected $table = "products_tags";
    protected $fillable = [
        'product_id','tag_id'];

    public function tags()
    {
        return $this->hasOne('App\Tag', 'id', 'tag_id');
    }
    
}
