<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inventory extends Model
{
    //
    protected $table = "inventory";
    protected $fillable = ['product_variant_id'];

    public function productImage()
	{
		return $this->hasOne('App\ProductVariant', 'id', 'product_variant_id');
	}
}
