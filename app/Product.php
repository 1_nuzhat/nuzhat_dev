<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Product extends Model
{
//    use SoftDeletes;
	protected $table = 'products';

	protected $fillable = ['name', 'description', 'category_id','p_id', 'p_status','size_desc','image_desc','specification','video_link','video_id','is_delete','deleted_at'];

	public function productVariant()
	{
		return $this->hasOne('App\ProductVariant', 'product_id', 'id');
	}

	public function getYouTubeId($url)
    {
        if(strlen($url) > 11)
        {
            if (preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $url, $match))
            {
                return $match[1];
            }
            else
                return false;
        }

        return $url;
    }


}
