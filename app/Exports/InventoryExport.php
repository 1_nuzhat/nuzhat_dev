<?php

namespace App\Exports;

use App\Inventory;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class InventoryExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function __construct($inventory)
    {
        $this->inventory = $inventory;
    }

     public function headings(): array
    {
        return [
            'image link',
            'product name',
            'product id',
            'product sku',
            'color',
            'size',
            'price',
            'compare price',
            'cost',
            'cdmx',
            'puebla',
            'guadalazara',
            'monterrey',
            'status',
        ];
    }

    public function collection()
    {
    	$inventory = $this->inventory;
         return $inventory;
    }
}
