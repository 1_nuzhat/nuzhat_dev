<?php

namespace App\Exports;

use App\Orders;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;
use App\orderDetails;
use App\Shippers;
use App\Payment;
use App\guestUserInfo;

class OrdersExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function __construct($order)
    {
        $this->order = $order;
    }


     public function headings(): array
    {
        return [
            'Order Number',
            'Created At Date',
            'Shipping Name',
            'Email',
            'Shipping Address',
            'Shipping Exterior Number',
            'Shipping Refrence',
            'Shipping City',
            'Shipping Zip Code',
            'Shipping Province',
            'Shipping Phone',
            'PRODUCT Name',
            'Product SKU',
            'Quantity',
            'Product Total Price',
            'Subtotal',
            'Shipping Cost',
            'Total',
            'Status'
        ];
    }

    public function collection()
    {

        $order = $this->order;
         return $order;
           
        
    }
}
