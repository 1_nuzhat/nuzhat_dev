<?php

namespace App\Exports;

use App\Product;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class FbExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function __construct($fbcat)
    {
        $this->fbcat = $fbcat;
    }
    
    public function headings(): array
    {
        return [
            'product id',
            'title',
            'description',
            'availability',
            'condition',
            'price',
            'link',
            'image_link',
            'brand',
        ];
    }

    public function collection()
    {
        $fbcat = $this->fbcat;
         return $fbcat;
    }
}
