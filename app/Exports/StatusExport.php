<?php

namespace App\Exports;

use App\Orders;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;
use App\orderDetails;
use App\Shippers;
use App\Payment;
use App\guestUserInfo;

class StatusExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function __construct($order)
    {
        $this->order = $order;
    }


     public function headings(): array
    {
        return [
            'Order Number',
            'Status'
        ];
    }

    public function collection()
    {

        $order = $this->order;
         return $order;
           
        
    }
}
