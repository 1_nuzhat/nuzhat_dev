<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategorySubcategory extends Model
{
    //
    protected $table = "category_subcategory";


    public function category()
    {
        return $this->hasOne('App\Category', 'id', 'categories_id');
    }
    
     public function family()
    {
        return $this->hasOne('App\Family', 'id', 'family_id');
    }
}
