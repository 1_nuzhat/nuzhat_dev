<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    //
    protected $table = "cart";
    protected $fillable = [
        'product_name','product_price','description','product_image','quantity','product_color','sub_total_price','shipping_price',
        'total_price','device_token','product_id','product_size','variant_id','ip_address','status'];


    
}
