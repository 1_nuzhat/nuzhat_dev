<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVariant extends Model
{
    //
     protected $table = "products_variants";
    protected $fillable = ['product_id','color_id','product_sku','product_price','product_compare_price','product_cost','size','branch_id','quantity','status'];
    public function productImage()
    {
    	return $this->hasOne('App\ProductImage', 'product_variant_id', 'id');
    }

    public function color()
    {
    	return $this->hasMany('App\Color', 'id', 'color_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Product', 'id', 'product_id');
    }
    public function colorData()
    {
        return $this->hasOne('App\Color', 'id', 'color_id');
    }
    public function productData()
    {
        return $this->hasOne('App\Product', 'id', 'product_id');
    }
    public function branch()
    {
    return $this->belongsTo('App\Branch', 'branch_id', 'id');
    }
    
}
