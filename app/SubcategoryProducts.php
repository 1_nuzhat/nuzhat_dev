<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubcategoryProducts extends Model
{
    //
      protected $table = "subcategory_products";

      public function products()
    {
        return $this->hasMany('App\Product', 'id', 'product_id');
    }

    public function categories()
    {
        return $this->hasMany('App\Category', 'id', 'category_id');
    }
}
