<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;

class DeliveryMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
        $message = Message::all();
        $messageExist = Message::orderBy('id', 'desc')->first();
        return view('admin.message',compact('message', 'messageExist'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('admin.add_message');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         request()->validate([
            'title' => 'required'
            
        ]);
         
        $message = new Message;
        $message->title = $request->title;
        $message->save();
           return back()->with('status', 'Message Added Successfully');
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $message_edit = Message::findOrFail($id);
        return view('admin.edit_message', ['message_edit' => $message_edit]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $message_edit = Message::findOrFail($id);
        $message_edit->title = $request->title;
        $message_edit->update();
        return redirect('/message');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
