<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Category;
use App\Product;
use App\Family;
use App\CategorySubcategory;
use Storage;
use DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $categories = DB::table('family')->leftjoin('category_subcategory','family.id', '=', 'category_subcategory.family_id')
         ->join('categories', 'category_subcategory.categories_id','=', 'categories.id')
       ->select('family.*', 'categories.id', 'categories.cat_name', 'categories.cat_desc', 'categories.images','categories.deleted_at','categories.status as catstatus')
            ->orderBy('categories.id','desc') ->groupBy('categories.id')
          ->get();
         return view('admin.category_list', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
      $families = Family::all();
      return view('admin.add_category', compact('families'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
         'cat_name' => 'required',
         'cat_desc' => 'required',
         'status' => 'required',
        ]);
        
       if (isset($request->images)) {
           $image      = $request->images;

           $fileExt = $image->getClientOriginalExtension();
           $renameFileName = strtotime(date('d-m-Y H:i:s')).'.'.$fileExt;
           $path = Storage::disk('public')->put('images/'.$renameFileName, file_get_contents($image));

        
            $categories = new Category;
            $categories->cat_name = $request->cat_name;
            $categories->cat_desc = $request->cat_desc;
            $categories->images = $renameFileName;
            $categories->status = $request->status;
            $categories->save();
        
        if($categories->id){
            foreach ($request->name as $val) {
                $cat_subcat = new CategorySubcategory; 
                $cat_subcat->family_id = $val;
                $cat_subcat->categories_id = $categories->id;
                $cat_subcat->save();  
            }
    
        }
    
        
       
        return redirect('/categories')->with('status', 'category added');
   }
}

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function edit($id)
    {

        $categories = Category::findOrFail($id);
//        dd($categories);
        $families =Family::all();
        return view('admin.edit_category',compact('categories','families'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        request()->validate([
            'cat_name' => 'required',
            'cat_desc' => 'required',
            'status' => 'required',
        ]);
        $categories = Category::findOrFail($id);
        $categories->cat_name = $request->cat_name;
        $categories->cat_desc = $request->cat_desc;
        $categories->status = $request->status;
        $categories->update();

        if($id){
            CategorySubcategory::where('categories_id',$id)->delete();
            foreach ($request->name as $val) {
                $cat_subcat = new CategorySubcategory;
                $cat_subcat->family_id = $val;
                $cat_subcat->categories_id = $id;
                $cat_subcat->save();
            }

        }
        return redirect('categories');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
//        dd($request);
        $category = Category::find($request->catId);
//        dd($request->type);
        if($request->type == 'delete') {
            $category->deleted_at = Carbon::now();
            $category->is_delete = 1;
            $category->save();
        }else{
            $params = array('is_delete' => null,'deleted_at'=> null);
            Category::where('id',$request->catId)->update($params);
        }

        return response()->json([
            'success' => true
        ]);
    }

    public static function getFamily($fId,$cId)
    {
        $familyCate =CategorySubcategory::where('family_id',$fId)->where('categories_id',$cId)->first();
        return $familyCate;
    }
}
