<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Banner;
use Storage;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $banner = Banner::all();
//        dd($banner);
        return view('admin.banner', compact('banner'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.add_banner');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        request()->validate([
            'name' => 'required',
            'description' => 'required',
            'images' => 'required',
            'status' => 'required',
            
        ]);

        $banner = new Banner;

        if ($request->hasFile('images')) {
            $images      = $request->file('images');
            $imageName  = $request->images->getClientOriginalName();
            Storage::disk('public')->put('images/'.$imageName, file_get_contents($images));
        $banner->name = $request->name;
        $banner->description = $request->description;
        $banner->images = $imageName;
        $banner->status = $request->status;
        $banner->save();
    }
         
        return back()->with('status', 'Banner Added Successfully');
      
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $banners = Banner::findOrFail($id);
//        dd($banners);
        return view('admin.edit_banner', ['banners' => $banners]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $banners = Banner::findOrFail($id);
        $banners->name = $request->name;
        $banners->description = $request->description;
        if ($request->hasFile('images')) {
            $images = $request->file('images');
            $imageName = $request->images->getClientOriginalName();
            Storage::disk('public')->put('images/' . $imageName, file_get_contents($images));
            $banners->images = $imageName;
        }else
        {
            $banners->images = $banners->images;
        }

        $banners->status = $request->status;
        $banners->update();
        return redirect('banner');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $banner = Banner::find($request->bannerId);
//        dd($request->type);
        if($request->type == 'delete') {
            $banner->deleted_at = Carbon::now();
            $banner->is_delete = 1;
            $banner->save();
        }else{
            $params = array('is_delete' => null,'deleted_at'=> null);
            Banner::where('id',$request->bannerId)->update($params);
        }

        return response()->json([
            'success' => true
        ]);
    }
}
