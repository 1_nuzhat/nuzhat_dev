<?php

namespace App\Http\Controllers;

use App\Branch;
use App\CategorySubcategory;
use App\Product;
use App\Traits\CommonTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Validator, Redirect, Response;
use Storage;
use App\Banner;
use App\Family;
use DB;
use App\Message;

class FrontController extends Controller
   
{
    //
    public function index(Request $request)
    {

    	$banners = Banner::where('is_delete',null)->get();
    	// $families = Family::all();
    	$families = Family::select('id','name')->get();
        $msg = Message::select('title')->get();

    	$families_subcategory = DB::table('family')
            ->leftjoin('category_subcategory', 'family.id', '=', 'category_subcategory.family_id')
            ->leftJoin('categories', 'category_subcategory.categories_id', '=', 'categories.id')
            ->leftJoin('subcategory_products', 'categories.id', '=', 'subcategory_products.category_id')
            ->leftjoin('products', 'subcategory_products.product_id', '=', 'products.id')
            ->join('products_variants', 'products.id', '=', 'products_variants.product_id')
            ->join('products_images', 'products_variants.id', '=', 'products_images.product_variant_id')
            ->select('family.id as category_id', 'family.name as category_name', 'category_subcategory.family_id as cs_category_id',
                'category_subcategory.categories_id as cs_subcategory_id', 'categories.id as subcat_id',
                'categories.cat_name as subcat_name','products.name as product_name','products_images.images',
                'products_variants.product_price', 'subcategory_products.category_id as sp_subcat_id',
                'subcategory_products.product_id as sp_product_id',
                DB::raw('GROUP_CONCAT(products.name) as productname'),
                DB::raw('GROUP_CONCAT(products.id) as productid'))
            ->groupBy('subcategory_products.product_id')
            ->where('products.p_status',1)

            //->get();
            ->select('family.id as family_id', 'family.name as family_name',
                'categories.id as cat_id', 'products.name as product_name', 'products.description',
                'products_variants.product_price', 'products_images.images')
            ->get();
//        dd($families_subcategory);

        $family = Family::first();
        $request->session()->put('family_id', $family->id);
        $branch = Session::get('branch_id');

        $products = DB::table('family')->where('family.id', $family->id)
            ->leftjoin('category_subcategory', 'family.id', '=', 'category_subcategory.family_id')
            ->leftJoin('categories', 'category_subcategory.categories_id', '=', 'categories.id')
            ->leftJoin('subcategory_products', 'categories.id', '=', 'subcategory_products.category_id')
            ->leftjoin('products', 'subcategory_products.product_id', '=', 'products.id')
            ->join('products_variants', 'products.id', '=', 'products_variants.product_id')
            ->join('products_images', 'products_variants.id', '=', 'products_images.product_variant_id')
            ->select('family.id as category_id', 'family.name as category_name', 'category_subcategory.family_id as cs_category_id', 'category_subcategory.categories_id as cs_subcategory_id', 'categories.id as subcat_id', 'categories.cat_name as subcat_name', 'subcategory_products.category_id as sp_subcat_id',
                'subcategory_products.product_id as sp_product_id',
                'family.id as family_id', 'family.name as family_name',
                'categories.id as cat_id','products.id as pid',
                'products.name as product_name', 'products.description',
                'products_variants.product_price', 'products_images.images',
                DB::raw('GROUP_CONCAT(products.name) as productname'),
                DB::raw('GROUP_CONCAT(products.id) as productid'))->groupBy('subcategory_products.product_id')
        ->when($branch, function ($products) use ($branch) {
            $products->where('products_variants.branch_id',$branch);
        })
            ->where('products.p_status',1)
            ->orderBy('products.id','asc')
            ->take(20)
        ->get();
        if(count($products) > 0) {
            $lastProductId = $products->last()->pid;
        }else{
            $lastProductId = null;
        }
//        dd($products);


        return view('index',compact('banners','families','families_subcategory','products','lastProductId','msg'));
	//}

    }

    // public function product($id){
    //     $product_id = 
    // 	return view('product_filter');

    // }

    public function getProductCategoryWise(Request $request)
    {
        $msg = Message::select('title')->get();
        $request->session()->put('family_id', $request->id);
    	$products = DB::table('family')->where('family.id', $request->id)
    	->leftjoin('category_subcategory', 'family.id', '=', 'category_subcategory.family_id')
            ->leftJoin('categories', 'category_subcategory.categories_id', '=', 'categories.id')
            ->leftJoin('subcategory_products', 'categories.id', '=', 'subcategory_products.category_id')
            ->leftjoin('products', 'subcategory_products.product_id', '=', 'products.id')
            ->join('products_variants', 'products.id', '=', 'products_variants.product_id')
            ->join('products_images', 'products_variants.id', '=', 'products_images.product_variant_id')
            ->select('family.id as category_id', 'family.name as category_name', 'category_subcategory.family_id as cs_category_id', 'category_subcategory.categories_id as cs_subcategory_id', 'categories.id as subcat_id', 'categories.cat_name as subcat_name', 'subcategory_products.category_id as sp_subcat_id', 'subcategory_products.product_id as sp_product_id', DB::raw('GROUP_CONCAT(products.name) as productname'), DB::raw('GROUP_CONCAT(products.id) as productid'))->groupBy('subcategory_products.product_id')
           // ->get();
            ->select('family.id as family_id', 'family.name as family_name', 'categories.id as cat_id','products.id as pid', 'products.name as pname', 'products.description', 'products_variants.product_price', 'products_images.images')->get();

        $data = view('product.sub_product_list',compact('products'))->render();

        return response()->json([
            'success' => 'success','data'=>$data
        ]);
    	//dd($data[0]);
    }


    public function branchProductList(Request $request,$name)
    {
        $msg = Message::select('title')->get();
        $branchId = Branch::where('name',$name)->first();
//        dd($branchId);
        $branch = $branchId->id;
        $request->session()->put('branch_id',$branch);
        $families = Family::all();
        $familySubcats= array();
        foreach ($families as $family)
        {
            $subfam = CategorySubcategory::where('family_id',$family->id)->with('category')->get();
            $familySubcats[] =['fname'=>$family->name,'fid'=>$family->id,'subcat' =>$subfam];
        }
        $products = Product::leftjoin('products_variants','products.id','=','products_variants.product_id')
            ->leftjoin('products_images','products_variants.id','=','products_images.product_variant_id')
            ->select('products.name as pname','products.id as pid','products_variants.id as vid','products_variants.product_price as product_price','products_images.images')
            ->where('products_variants.branch_id',$branch)
            ->take(20)
            ->groupBy('products.id')
            ->orderBy('products.id','asc')
            ->get();
        if (count($products) > 0) {
            $lastProductId = $products->last()->pid;
        } else {
            $lastProductId = null;
        }
        return view('product.branchwise',compact('products','familySubcats','lastProductId','msg'));

    }

    public function getFamilySub(Request $request)
    {
        $families = Family::all();
        $familySubcats= array();
        foreach ($families as $family)
        {
            $subfam = CategorySubcategory::where('family_id',$family->id)->with('category')->get();
            $familySubcats[] =['fname'=>$family->name,'fid'=>$family->id,'subcat' =>$subfam];
        }
//        dd($familySubcats);
        $data = view('product.family_sub',compact('familySubcats'))->render();
        return response()->json([
            'success' => 'success','data'=>$data
        ]);
    }

    public function familyProductList(Request $request,$subId,$fid)
    {
        $msg = Message::select('title')->get();
        $subcatId = CommonTrait::decodeId($subId);
        $familyId = CommonTrait::decodeId($fid);
        $families = Family::all();
        $familySubcats= array();
        foreach ($families as $family)
        {
            $subfam = CategorySubcategory::where('family_id',$family->id)->with('category')->get();
            $familySubcats[] =['fname'=>$family->name,'fid'=>$family->id,'subcat' =>$subfam];
        }
        $request->session()->put('category_id', $subcatId);

        $products = Product::leftjoin('products_variants','products.id','=','products_variants.product_id')
            ->leftjoin('products_images','products_variants.id','=','products_images.product_variant_id')
            ->leftjoin('subcategory_products','products.id','=','subcategory_products.product_id')
            ->select('products.name as pname','products.id as pid','products_variants.id as vid','products_variants.product_price as product_price','products_images.images')
            ->where('subcategory_products.category_id',$subcatId)
            ->take(20)
            ->groupBy('products.id')
            ->orderBy('products.id','asc')
            ->get();
        if(count($products) > 0) {
            $lastProductId = $products->last()->pid;
        }else{
            $lastProductId = null;
        }
//        dd($subcatId);
        return view('product.product_list',compact('familySubcats','products','lastProductId','msg'));

    }

    public function familyProducts(Request $request,$id)
    {
        $msg = Message::select('title')->get();
        $familyId = CommonTrait::decodeId($id);
        $families = Family::all();
        $familySubcats= array();
        $branch = Session::get('branch_id');
        foreach ($families as $family)
        {
            $subfam = CategorySubcategory::where('family_id',$family->id)->with('category')->get();
            $familySubcats[] =['fname'=>$family->name,'fid'=>$family->id,'subcat' =>$subfam];
        }
        $family = Family::find($familyId);
        $request->session()->put('family_id', $family->id);
        $branch = Session::get('branch_id');

        $products = DB::table('family')->where('family.id', $family->id)
            ->leftjoin('category_subcategory', 'family.id', '=', 'category_subcategory.family_id')
            ->leftJoin('categories', 'category_subcategory.categories_id', '=', 'categories.id')
            ->leftJoin('subcategory_products', 'categories.id', '=', 'subcategory_products.category_id')
            ->leftjoin('products', 'subcategory_products.product_id', '=', 'products.id')
            ->join('products_variants', 'products.id', '=', 'products_variants.product_id')
            ->join('products_images', 'products_variants.id', '=', 'products_images.product_variant_id')
            ->select('products.name as pname','products.id as pid','products_variants.id as vid','products_variants.product_price as product_price','products_images.images')
            ->groupBy('products_variants.color_id')
            ->orderBy('products.id','asc')
            ->take(20)
            ->when($branch, function ($products) use ($branch) {
                $products->where('products_variants.branch_id',$branch);
            })
            ->get();
        if(count($products) > 0) {
            $lastProductId = $products->last()->pid;
        }else{
            $lastProductId = null;
        }
//dd($lastProductId);


        return view('product.product_list',compact('familySubcats','products','lastProductId','msg'));

    }

    public function familyMoreData(Request $request)
    {
//        dd(Route::currentRouteName());
        $products= '';
        $pageNo =20;
        $branch = Session::get('branch_id');
//        dd($request->currentriute);

        if($request->currentriute == 'family.products') {
            $familyId = Session::get('family_id');

            if(!empty($request->lastId)) {
                $products = DB::table('family')->where('family.id', $familyId)
                    ->leftjoin('category_subcategory', 'family.id', '=', 'category_subcategory.family_id')
                    ->leftJoin('categories', 'category_subcategory.categories_id', '=', 'categories.id')
                    ->leftJoin('subcategory_products', 'categories.id', '=', 'subcategory_products.category_id')
                    ->leftjoin('products', 'subcategory_products.product_id', '=', 'products.id')
                    ->join('products_variants', 'products.id', '=', 'products_variants.product_id')
                    ->join('products_images', 'products_variants.id', '=', 'products_images.product_variant_id')
                    ->select('products.name as pname', 'products.id as pid', 'products_variants.id as vid', 'products_variants.product_price as product_price', 'products_images.images')
                    ->where('products.id', '>', $request->lastId)
                    ->groupBy('products_variants.color_id')
                    ->orderBy('products.id','asc')
                    ->take($pageNo)
                    ->when($branch, function ($products) use ($branch) {
                        $products->where('products_variants.branch_id', $branch);
                    })
                    ->get();
                if (count($products) > 0) {
                    $lastProductId = $products->last()->pid;
                } else {
                    $lastProductId = null;
                }
            }else{
                $lastProductId = null;
                $products = [];
            }
        }
       elseif($request->currentriute == 'family.product.list')
        {
            $subcatId = Session::get('category_id');
            if(!empty($request->lastId)) {
                $products = Product::leftjoin('products_variants', 'products.id', '=', 'products_variants.product_id')
                    ->leftjoin('products_images', 'products_variants.id', '=', 'products_images.product_variant_id')
                    ->leftjoin('subcategory_products', 'products.id', '=', 'subcategory_products.product_id')
                    ->select('products.name as pname', 'products.id as pid', 'products_variants.id as vid', 'products_variants.product_price as product_price', 'products_images.images')
                    ->where('subcategory_products.category_id', $subcatId)
                    ->where('products.id', '>', $request->lastId)
                    ->take($pageNo)
                    ->groupBy('products.id')
                    ->orderBy('products.id', 'asc')
                    ->when($branch, function ($products) use ($branch) {
                        $products->where('products_variants.branch_id', $branch);
                    })
                    ->get();
                if (count($products) > 0) {
                    $lastProductId = $products->last()->pid;
                } else {
                    $lastProductId = null;
                }
            }else{
                $lastProductId = null;
                $products = [];
            }
        }
        elseif($request->currentriute == 'subcat.products')
        {
            $subcatId = Session::get('category_id');
            if(!empty($request->lastId)) {
                $products = Product::leftjoin('products_variants', 'products.id', '=', 'products_variants.product_id')
                    ->leftjoin('products_images', 'products_variants.id', '=', 'products_images.product_variant_id')
                    ->leftjoin('subcategory_products', 'products.id', '=', 'subcategory_products.product_id')
                    ->select('products.name as pname', 'products.id as pid', 'products_variants.id as vid', 'products_variants.product_price as product_price', 'products_images.images')
                    ->where('subcategory_products.category_id', $subcatId)
                    ->where('products.id', '>', $request->lastId)
                    ->take($pageNo)
                    ->groupBy('products.id')
                    ->orderBy('products.id', 'asc')
                    ->when($branch, function ($products) use ($branch) {
                        $products->where('products_variants.branch_id', $branch);
                    })
                    ->get();
                if (count($products) > 0) {
                    $lastProductId = $products->last()->pid;
                } else {
                    $lastProductId = null;
                }
            }else{
                $lastProductId = null;
                $products = [];
            }
        }
        elseif($request->currentriute == 'get.branch.product')
        {
            $branchId = Session::get('branch_id');
            if(!empty($request->lastId)) {
                $products = Product::leftjoin('products_variants', 'products.id', '=', 'products_variants.product_id')
                    ->leftjoin('products_images', 'products_variants.id', '=', 'products_images.product_variant_id')
                    ->select('products.name as pname', 'products.id as pid', 'products_variants.id as vid', 'products_variants.product_price as product_price', 'products_images.images')
                    ->where('products_variants.branch_id', $branchId)
                    ->where('products.id', '>', $request->lastId)
                    ->take($pageNo)
                    ->groupBy('products.id')
                    ->orderBy('products.id', 'asc')
                    ->when($branch, function ($products) use ($branch) {
                        $products->where('products_variants.branch_id', $branch);
                    })
                    ->get();
                if (count($products) > 0) {
                    $lastProductId = $products->last()->pid;
                } else {
                    $lastProductId = null;
                }
            }else{
                $lastProductId = null;
                $products = [];
            }

        }

        else{
            if(!empty($request->lastId)) {
                $products = Product::leftjoin('products_variants', 'products.id', '=', 'products_variants.product_id')
                    ->leftjoin('products_images', 'products_variants.id', '=', 'products_images.product_variant_id')
                    ->select('products.name as pname', 'products.id as pid', 'products_variants.id as vid', 'products_variants.product_price as product_price', 'products_images.images')
                    ->where('products.id', '>', $request->lastId)
                    ->groupBy('products.id')
                    ->take($pageNo)
                    ->orderBy('products.id', 'asc')
                    ->when($branch, function ($products) use ($branch) {
                        $products->where('products_variants.branch_id', $branch);
                    })
                    ->get();
                if (count($products) > 0) {
                    $lastProductId = $products->last()->pid;
                } else {
                    $lastProductId = null;
                }            }else{
                $lastProductId = null;
                $products = [];
            }
        }
//        dd($products);
        if($request->currentriute == 'cart.list'){
            $data = view('product.cart_list_scroll', compact('products'))->render();
        }else {
            $data = view('product.product_list_scroll', compact('products'))->render();
        }
        return response()->json([
            'success' => 'success','data'=>$data,'lastProductId' =>$lastProductId
        ]);
    }

    public function searchData(Request $request)
    {
//        dd($request->search);
        $msg = Message::select('title')->get();
        $value = $request->search;
        $families = Family::all();
        $familySubcats= array();
        foreach ($families as $family)
        {
            $subfam = CategorySubcategory::where('family_id',$family->id)->with('category')->get();
            $familySubcats[] =['fname'=>$family->name,'fid'=>$family->id,'subcat' =>$subfam];
        }
        $products = Product::leftjoin('products_variants','products.id','=','products_variants.product_id')
            ->leftjoin('products_images','products_variants.id','=','products_images.product_variant_id')
            ->select('products.name as pname','products.id as pid','products_variants.id as vid','products_variants.product_price as product_price','products_images.images')
            ->where('products.p_status',1)
            ->where('products.name', 'like', '%'.$value.'%')
            ->groupBy('products.id')
            ->get();
        return view('product.search_product_list',compact('familySubcats','products','msg'));


    }
}
