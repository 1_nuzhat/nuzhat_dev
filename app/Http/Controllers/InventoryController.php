<?php

namespace App\Http\Controllers;

use App\Inventory;
use App\Orders;
use App\Traits\CommonTrait;
use Illuminate\Http\Request;
use DB;
use App\ProductVariant;
use App\Product;
use App\ProductImage;
use App\Branch;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\InventoryExport;
use App\OrderDetails;
use App\Imports\InventoryImport;


class InventoryController extends Controller
{
    //
    public function index()
    {

        $product_variant = DB::table('products_variants')
            ->join('products','products_variants.product_id', '=','products.id')
            ->join('products_images','products_variants.id', '=', 'products_images.product_variant_id')
            ->join('inventory','products_variants.id', '=', 'inventory.product_variant_id')
            ->select('products_images.images', 'products.name',
                'products.p_status', 'products_variants.quantity', 'products_variants.product_sku',
                'inventory.in_stock','inventory.initial_stock','inventory.sale_qty','inventory.sale_cost',
                'inventory.sale_cost','inventory.sale_date','products_variants.branch_id','products_variants.updated_at as outofstock','products_variants.id as variant_id','products_variants.status')
            ->where('products_variants.status','=', 1)
            ->get();
             return view('admin.inventory', compact('product_variant'));
   
    }
    
    public function export(Request $request)
    {
        $ids = $request->ids;
        $pvId = explode(',',$ids);
        $product_variant = DB::table('products_variants')
            ->whereIn('products_variants.id', $pvId)
            ->join('products','products_variants.product_id', '=','products.id')
            ->join('colors', 'colors.id', '=', 'products_variants.color_id')
            ->join('products_images','products_variants.id', '=', 'products_images.product_variant_id')
            ->join('branch','products_variants.branch_id', '=', 'branch.id')
             ->select('products.id as pid','products_variants.id as variant_id','products_images.images','products.name','products.p_id as product_id','products_variants.product_sku','products_variants.size','colors.color_name','products_variants.product_price','products_variants.product_compare_price','products_variants.product_cost','branch.name as branch_name','branch.name as branch_name1','branch.name as branch_name2','branch.name as branch_name3','products_variants.branch_id','products_variants.quantity','branch.name as branchname2',
                DB::raw('(CASE WHEN p_status = "1" THEN "YES"
                            ELSE "NO"
                            END) AS status'))
             ->get();
             $exportdata = array();
       
        foreach ($product_variant as $key => $value) {

            $filename = asset('storage/images/' . $value->images);
            $image = Array('images' => $filename);
            $value->images = $image['images'];

            $cdmx = $publa = $gar= $mar=0;

            if ($value->branch_name == 'CDMX') {
            $branch_qty = ProductVariant::where('branch_id', $value->branch_id)->first();
          
            $cdmx = $value->quantity;
            }
            else{
            $cdmx = 0;
            }
            if ($value->branch_name == 'MONTERREY') {
            $branch_qty = ProductVariant::where('branch_id', $value->branch_id)->first();
           
            $mar= $value->quantity;
            }
            else {

            $mar = 0;
            }
            if ($value->branch_name == 'PUEBLA') {
            $branch_qty = ProductVariant::where('branch_id', $value->branch_id)->first();
           
            $puebla = $value->quantity;
            }
            else{
            $puebla= 0;
            }
            if ($value->branch_name == 'GUADALAZARA') {
            $branch_qty = ProductVariant::where('branch_id', $value->branch_id)->first();
            
            $gar = $value->quantity;
            }
            else{
            $gar = 0;
            }
          
            $exportdata[] = array('images' => $value->images,'name' =>$value->name,
                'product_sku' =>$value->product_sku,
                'product_id' =>$value->product_id,
                'size' =>$value->size,'color_name'=>$value->color_name,'product_price' =>$value->product_price,
            'product_compare_price' =>$value->product_compare_price,'product_cost' => $value->product_cost,
            'cdmx' =>$cdmx,'puebla' => $puebla,'guadalazara' =>$gar,'monterrey' => $mar,
            'status' =>$value->status,
            
            );
          
            }
        foreach ($product_variant as $key => $value) {
          unset($value->pid);
          unset($value->variant_id);

        }

     

//=========================
    $delimiter = ",";
    $filename = "products"  . ".csv";
    
    //create a file pointer
    $f = fopen('php://memory', 'w');
    
    //set column headers
    $fields = array('images', 'name', 'product_sku','product_id', 'size', 'color_name', 'product_price', 'product_compare_price', 'product_cost', 'cdmx', 'puebla', 'guadalazara', 'monterrey', 'status');
    fputcsv($f, $fields, $delimiter);
    //output each row of the data, format line as csv and write to file pointer
   

    foreach ($exportdata as $key => $row) {
        $lineData = array($row['images'], $row['name'], $row['product_sku'],
            $row['product_id'], $row['size'], $row['color_name'], $row['product_price'], $row['product_compare_price'], $row['product_cost'], $row['cdmx'], $row['puebla'],
            $row['guadalazara'], $row['monterrey'], $row['status']);
        fputcsv($f, $lineData, $delimiter);
    }

    
    //move back to beginning of file
    fseek($f, 0);
    
    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    
    //output all remaining data on a file pointer
    fpassthru($f);
    exit;
//==========================
        //return Excel::download(new InventoryExport($exportdata), 'inventory_details.xlsx');
    }


    public static function productTransist($variant_id)
    {
        $transistData = Orders::leftJoin('order_details','orders.id','=','order_details.orders_id')
            ->select(DB::raw('count(order_details.product_variant_id) as totaltransit'))
            ->where('order_details.product_variant_id',$variant_id)->first();
        return $transistData->totaltransit;

    }

    public static function inventoryProject($productId)
    {
        $variants = ProductVariant::where('product_id',$productId)->get();
        $sum =0;
        foreach ($variants as $variant)
        {
            $inventory = Inventory::where('product_variant_id',$variant->id)->select(DB::raw('sum(in_stock) as in_stock'))->first();
            if ($inventory) {
                $sum = $sum + $inventory->in_stock;
                    }
        }
        return $sum;
    }

    public function import()
    {
        Excel::import(new InventoryImport,request()->file('file'));
        return back()->with('status', 'Product Updated Successfully');
    }

    public function branchInventory(Request $request,$id)
    {
        $branchId = CommonTrait::decodeId($id);
        $product_variant = DB::table('products_variants')
            // ->leftJoin('products_variants', 'branch.id', '=', 'products_variants.branch_id')
            ->join('products','products_variants.product_id', '=','products.id')
            ->join('products_images','products_variants.id', '=', 'products_images.product_variant_id')
            ->join('inventory','products_variants.id', '=', 'inventory.product_variant_id')
            ->select('products_images.images', 'products.name',
                'products.p_status', 'products_variants.quantity', 'products_variants.product_sku',
                'inventory.in_stock','inventory.initial_stock','inventory.sale_qty','inventory.sale_cost',
                'inventory.sale_cost','inventory.sale_date','products_variants.branch_id','products_variants.updated_at as outofstock','products_variants.id as variant_id')
            ->where('products_variants.branch_id',$branchId)
            ->get();
    return view('admin.branch_inventory',compact('product_variant'));
////        $availbleQuantity = $productSizes
//        return response()->json([
//            'success' => 'success','data'=>$data
//        ]);

    }
}
