<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Branch;
use App\Country;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $branches = Branch::with('country')->get();
//        dd($branches);
        return view('admin.branch', compact('branches'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $countries = Country::all();
        return view('admin.add_branch', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'name' => 'required'
            
        ]);
         
        $country = new Country;
        $branch = new Branch;
        $branch_details = Branch::where('name', $request->name)->first();

        if($branch_details) {
        return redirect()->back()->withInput()->withErrors(['name'=>'Branch Name already exist']);

        } else {
            $branch->country_id = $request->country_id;
            $branch->name = $request->name;
            $branch->save();
            return back()->with('status', 'Branch Added Successfully');
        }

        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $branch = Branch::findOrFail($id);
        $countries = Country::all();
        return view('admin.edit_branch', ['branch' => $branch, 'countries' => $countries ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $branch = Branch::findOrFail($id);
        $branch->country_id = $request->country_id;
        $branch->name = $request->name;
        $branch->update();
        return redirect('/branch');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public static function branches()
    {
        $branches  = Branch::all();
        return $branches;
    }
}
