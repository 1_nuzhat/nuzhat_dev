<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Kreait\Firebase;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Kreait\Firebase\Database;
use App\Traits\CommonTrait;

class FirebaseController extends Controller
{
   
    public static function sendPushNotification($id) {
    	
    	$normal_id = CommonTrait::decodeId($id);
        $url = "https://firebasedynamiclinks.googleapis.com/v1/shortLinks?key=AIzaSyB1xTvY4bhGOms9gPM-pg5S3KGBdOKBkt8";
        $header = [
            
            'content-type: application/json'
        ];

        $postdata = '{
		    "dynamicLinkInfo": {
		    "domainUriPrefix": "https://arrivederci.page.link",
		    "link": "https://arrivederci.mx/product_description/'.$id.'/'.$normal_id.'",
		    
		  
           "androidInfo": {
           "androidPackageName": "com.gangabox"
         },
            "iosInfo": {
            "iosBundleId": "com.ios.gangabox"
            }
        }
		}';
		//dd($postdata);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 60);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        $result = curl_exec($ch);
        curl_close($ch);
       

		// Decode JSON data to PHP associative array
		$arr = json_decode($result, true);
		// Access values from the associative ardd($result);

// Decode JSON data to PHP object
// $obj = json_decode($result);
// Access values from the returned object
// echo $obj->shortLink; // Output: 65
       // dd($result);
       // return $result;
        return $arr["shortLink"];

    }
}


