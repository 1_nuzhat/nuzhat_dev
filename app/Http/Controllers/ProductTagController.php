<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\ProductTag;
use App\Tag;
use Illuminate\Support\Facades\Redirect;
use Session;

class ProductTagController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
//        dd($request->selectitem);
        $tags = Tag::whereNull('deleted_at')->get();
//        dd($tags);
//dd($request->remove_button);
//        $remove_button = '';
//        if(isset($request->remove_button) == 'active') {
//            $remove_button = 'active';
//        }

//        if(!empty($request->selectitem)){
            return view('admin.producttag_list', ['tags' => $tags, 'selected_tags' => $request->selectitem]);
//        }

//        return view('admin.producttag_list', ['tags' => $tags, 'remove_pro' => $remove_button]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
        if($request->products) {
            foreach ($request->products as $product) {
                $tags = ProductTag::findOrFail($id);
                $tags->delete();
                //return redirect('/product');
                //ProductVariant::findOrFail("$id")->delete();
            }

            $tags = ProductTag::findOrFail($id);

            if($tags){
                ProductTag::where('tag_id', $id)->delete();
            }
            return back()->with("success", "Product Tag Removed Successfully.");
        }
    }


    public function storeTags(Request $request)
    {

//        dd($request->tagType);
        //dd($request->tid);
       // dd($request->tid);
        if($request->tagType == 'removetag') {
                if ($request->tags) {
                    foreach ($request->tags as $tag) {
//                        dd($tag);
                        $params = array('deleted_at' => Carbon::now(),'is_delete'=>1);
                        ProductTag::where('tag_id', $tag)->update($params);
                        Tag::where('id', $tag)->update($params);
                    }
                }
        }
        else {
            if ($request->products) {

                foreach ($request->products as $product) {
                    foreach ($request->tags as $tag) {
                        $ptag = new ProductTag();
                        $ptag->product_id = $product;
                        $ptag->tag_id = $tag;
                        $ptag->save();
                    }

                }
            }
        }

        return redirect('product');

        

    }

    //public function changeStatus(Request $request)
    // {
    //     try{

    //         if($request->status =='active')
    //         {
    //           $ticketSubCategory = ticketSubCategory::findOrFail($request->id);
    //           $ticketSubCategory->status = 'inactive';
    //           $ticketSubCategory->update();
    //           return back()->with('status', 'Sub Category Inactivated');  
    //         }else
    //         {
    //             $ticketSubCategory = ticketSubCategory::findOrFail($request->id);
    //             $ticketSubCategory->status = 'active';
    //             $ticketSubCategory->update();
    //             return back()->with('status', 'Sub Category activated');
    //         }
    //     }catch (Exception $ex)
    //     {
    //         return back()->with('error', $ex->getMessage());    
    //     }
    //  }
}
