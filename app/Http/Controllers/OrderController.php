<?php

namespace App\Http\Controllers;

use App\ProductVariant;
use Illuminate\Http\Request;
use App\Orders;
use App\OrderDetails;
use App\Shippers;
use App\Payment;
use DB;
use App\Imports\OrderImport;
use App\Exports\OrdersExport;
use App\Exports\StatusExport;
use Maatwebsite\Excel\Facades\Excel;
use PDF;
use App\Inventory;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = DB::table('order_details') 
                ->leftJoin('orders', 'order_details.orders_id', '=', 'orders.id')
                ->join('guest_user_info', 'orders.guest_user_id', '=', 'guest_user_info.id')
                ->join('shippers', 'orders.shipper_id', '=', 'shippers.id')
                ->leftJoin('payment', 'orders.payment_id', '=', 'payment.id')
                ->select('orders.id','order_details.order_number','orders.order_date','guest_user_info.email','order_details.total','order_details.pay_by','orders.status')
                ->groupBy('order_details.order_number')
                ->get();
        return view('admin.order_list', compact('orders'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $order_number = Orders::find($id);
        $orders = Orders::with('guestUser', 'payment', 'shipping', 
        'orderDetails', 'orderDetails.product', 
        'orderDetails.product.productVariant',
        'orderDetails.product.productVariant.branch', 
        'orderDetails.product.productVariant.productImage')
        ->where('order_number',$order_number->order_number)->get();
        //dd($orders);
        if(!empty($orders)) {
            $orderAddress = array();
            $orderCalculation = array();

            $exteriorNumber = '';
            $streetNumber = '';
            $postalCode = '';
            $state = '';
            $mobile = '';
            $email = '';
            $name = '';

            $orderSubTotal = '';
            $orderShipping = '';
            $orderTotal = '';
            $subTotal = 0;
            $orderShipping = 0;
            $subQuant = 0;
            $shipping = '';
            $total = '';

            foreach ($orders as $order) {
                $exteriorNumber = isset($order->guestUser->exterior_number) && !empty($order->guestUser->exterior_number) ? $order->guestUser->exterior_number : '-' ;
                $street = $order->guestUser->street;
                $streetNumber = $order->guestUser->street_number;
                $reference = $order->guestUser->reference;
                $postalCode = $order->guestUser->postal_code;
                $state = $order->guestUser->state;
                $mobile = $order->guestUser->mobile;
                $email = $order->guestUser->email;
                $name = $order->guestUser->name;
                
                // $orderShippingPrice = isset($order->orderDetails) &&  !empty($order->ship_charges) ? $order->ship_charges : '0.00';

                $orderShipping =  $order->ship_charges ;

                $orderSubTotal = $order->orderDetails['price'] * $order->orderDetails['quantity'];

                $subTotal = $orderSubTotal + $subTotal;
                $subQuant = $order->orderDetails['quantity'] + $subQuant;

            }
            $orderTotal = $subTotal + $orderShipping;

            $orderAddress = [
                'street'         => $street,
                'exteriorNumber' => $exteriorNumber,
                'reference'      => $reference,
                'streetNumber'   => $streetNumber,
                'postalCode'     => $postalCode,
                'state'          => $state,
                'mobile'         => $mobile,
                'email'          => $email,
                'name'           => $name,
            ];


        }
       // dd($orderTotal.'-'.$subTotal.'-'.$orderShipping);
        return view('admin.order_view', compact('orders', 'order_number', 'orderAddress', 'orderTotal', 'subTotal', 'orderShipping','subQuant'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }
    
    public function updateStatus(Request $request, $id)
    { 
        $orders = Orders::findOrFail($id);
        $orderDetails = OrderDetails::where('orders_id',$orders->id)->get();
        $orders->status = $request->status;
        $orders->update();
        foreach ($orderDetails as $orderDetail)
        {
            $variantId = $orderDetail->product_variant_id;
            $invtoryData = Inventory::where('product_variant_id',$variantId)->first();
            if ($orders->status == 5) {
            $sales_qty = (int)$invtoryData->quantity - (int)$orderDetail->sale_qty;
            $actual_stock = (int)$invtoryData->initial_stock + (int)$sales_qty;
            $original_price = $invtoryData->sale_cost;
            $order_price = $orderDetail->price;
            $sales_price = $original_price - $order_price;
            $sale_date = $orderDetail->created_at;
            $invtoryData->in_stock = $actual_stock;
            $invtoryData->sale_qty = $sales_qty;
            $invtoryData->sale_cost = $sales_price;
            $invtoryData->sale_date = $sale_date;
            $invtoryData->save();
            $var_update = ProductVariant::where('id',$invtoryData->product_variant_id)
           ->update(['quantity' => $actual_stock]);
           }
           else {
            $sales_qty = (int)$invtoryData->sale_qty + (int)$orderDetail->quantity;
            $actual_stock = (int)$invtoryData->initial_stock - (int)$sales_qty;
            $original_price = $invtoryData->sale_cost;
            $order_price = $orderDetail->price;
            $sales_price = $original_price + $order_price;
            $sale_date = $orderDetail->created_at;
            $invtoryData->in_stock = $actual_stock;
            $invtoryData->sale_qty = $sales_qty;
            $invtoryData->sale_cost = $sales_price;
            $invtoryData->sale_date = $sale_date;
            $invtoryData->save();

             $var_update = ProductVariant::where('id',$invtoryData->product_variant_id)
           ->update(['quantity' => $actual_stock]);
           }
        }
        return back()->with('status', 'Order Status Changed Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    // public function export(Request $request) 
    // {
    //     // dd('sd');
    //      $ids = $request->ids;
    //     $orderId = explode(',',$ids);
    //     $orderIds = Orders::whereIn('id', $orderId)->get();
    //     $orderNos = [];
    //     foreach ($orderIds as $value) {
    //         $orderNos[] = Orders::where('order_number', $value->order_number)->pluck('id')->toArray();
    //     }
    //     if(!empty($orderNos)) {
    //         foreach ($orderNos as $orderNo) {
    //             if(!empty($orderNo)) {
    //                 foreach ($orderNo as $ordNo) {
                        
    //                 }
    //             }
    //         }
       // }
        // dd($orderNo);
       //  $orders = Orders::with('guestUser', 'payment', 'shipping', 
       //  'orderDetails', 'orderDetails.product', 
       //  'orderDetails.product.productVariant',
       //  'orderDetails.product.productVariant.branch', 
       //  'orderDetails.product.productVariant.productImage')
       //  // ->whereIn('order_number',$orderId)
       //  ->get();
       // dd($orders);
        // $orders = DB::table('products_variants')
                    // ->join('products','products_variants.product_id', '=','products.id')
                    // ->join('order_details','products_variants.id', '=','order_details.product_variant_id')
                    
                    //  // ->whereIn('orders.id', $orderId)  
                    //  ->leftJoin('orders', 'order_details.orders_id', '=', 'orders.id')
                    //  ->join('guest_user_info', 'orders.guest_user_id', '=', 'guest_user_info.id')
                    //  ->join('shippers', 'orders.shipper_id', '=', 'shippers.id')
                    //  ->leftJoin('payment', 'orders.payment_id', '=', 'payment.id')
                    // ->select('orders.order_number','orders.order_date','guest_user_info.name','guest_user_info.email','guest_user_info.street','guest_user_info.exterior_number','guest_user_info.reference','guest_user_info.state','guest_user_info.postal_code','guest_user_info.street_number','guest_user_info.mobile','products.name as product_name','order_details.product_sku','order_details.quantity','order_details.price','order_details.price as sub_total','orders.ship_charges','order_details.total',DB::raw('(CASE
                    //     WHEN orders.status = "1" THEN "COMPLETED"
                    //     WHEN orders.status = "2" THEN "PREPARED"
                    //     WHEN orders.status = "3" THEN "FIRST DELIVERY ATTEMPT"
                    //     WHEN orders.status = "4" THEN "SECOND DELIVERY ATTEMPT"
                    //     WHEN orders.status = "5" THEN "CANCELLED"
                    //     ELSE "CREATED"
                    //     END) AS status'))
                    //   ->get();
                         //dd($orders);

        
    //     return Excel::download(new OrdersExport($orders), 'order_details.xlsx');
    // }

//     public function export(Request $request) 
//     {
//         $ids = $request->ids;
//         $orderId = explode(',',$ids);
//         // dd($orderId);
//         $orders = DB::table('products_variants')
//                     ->join('products','products_variants.product_id', '=','products.id')

//                     ->join('order_details','products_variants.id', '=','order_details.product_variant_id')
//                     //->where('orders.id', $orderId)  
//                     ->leftJoin('orders', 'order_details.orders_id', '=', 'orders.id')
//                     ->whereIn('orders.id', $orderId)
                   
//                     ->join('guest_user_info', 'orders.guest_user_id', '=', 'guest_user_info.id')
//                     ->join('shippers', 'orders.shipper_id', '=', 'shippers.id')
//                     ->leftJoin('payment', 'orders.payment_id', '=', 'payment.id')
//                     ->select('orders.order_number','orders.order_date','guest_user_info.name','guest_user_info.email','guest_user_info.street',
//                         'guest_user_info.exterior_number','guest_user_info.reference','guest_user_info.state','guest_user_info.postal_code',
//                         'guest_user_info.street_number','guest_user_info.mobile','products.name as product_name',
//                         'order_details.product_sku','order_details.quantity','order_details.price','order_details.price as sub_total','orders.ship_charges','order_details.total',
//                         DB::raw('(CASE
//                         WHEN orders.status = "1" THEN "COMPLETED"
//                         WHEN orders.status = "2" THEN "PREPARED"
//                         WHEN orders.status = "3" THEN "FIRST DELIVERY ATTEMPT"
//                         WHEN orders.status = "4" THEN "SECOND DELIVERY ATTEMPT"
//                         WHEN orders.status = "5" THEN "CANCELLED"
//                         ELSE "CREATED"
//                         END) AS status'))  
//                     ->get();

//                     dd($orders);
// //        $usersOrders =  array();
//         // foreach ($orders as $order)
//         // {
//         //     $Ordersubtotal = OrderDetails::where('order_number',$order->order_number)->select(DB::raw('sum(price) as subtotal'))->first();
//         //     $order->sub_total= $Ordersubtotal->subtotal;
//         // }

// //                         dd($orders);
//         return Excel::download(new OrdersExport($orders), 'order_details.xlsx');
//     }
    
//     public function export(Request $request) 
//     {
//         $ids = $request->ids;
//         $orderId = explode(',',$ids);
//         $orders = DB::table('products_variants')
//                     ->join('products','products_variants.product_id', '=','products.id')
//                     ->join('order_details','products_variants.id', '=','order_details.product_variant_id')
//                     ->whereIn('orders.id', $orderId)  
//                     ->leftJoin('orders', 'order_details.orders_id', '=', 'orders.id')
//                     ->join('guest_user_info', 'orders.guest_user_id', '=', 'guest_user_info.id')
//                     ->join('shippers', 'orders.shipper_id', '=', 'shippers.id')
//                     ->leftJoin('payment', 'orders.payment_id', '=', 'payment.id')
//                     ->select('orders.order_number','orders.order_date','guest_user_info.name','guest_user_info.email','guest_user_info.street',
//                         'guest_user_info.exterior_number','guest_user_info.reference','guest_user_info.state','guest_user_info.postal_code',
//                         'guest_user_info.street_number','guest_user_info.mobile','products.name as product_name',
//                         'order_details.product_sku','order_details.quantity','order_details.price','order_details.price as sub_total','orders.ship_charges','order_details.total',
//                         DB::raw('(CASE
//                         WHEN orders.status = "1" THEN "COMPLETED"
//                         WHEN orders.status = "2" THEN "PREPARED"
//                         WHEN orders.status = "3" THEN "FIRST DELIVERY ATTEMPT"
//                         WHEN orders.status = "4" THEN "SECOND DELIVERY ATTEMPT"
//                         WHEN orders.status = "5" THEN "CANCELLED"
//                         ELSE "CREATED"
//                         END) AS status'))
//                     ->get();
//                     dd($orders);
// //        $usersOrders =  array();
//         foreach ($orders as $order)
//         {
//             $Ordersubtotal = OrderDetails::where('order_number',$order->order_number)->select(DB::raw('sum(price) as subtotal'))->first();
//             $order->sub_total= $Ordersubtotal->subtotal;
//         }

// //                         dd($orders);
//         return Excel::download(new OrdersExport($orders), 'order_details.xlsx');
//     }
       
    //    public function export(Request $request) 
    // {
    //     $ids = $request->ids;
    //     $orderId = explode(',',$ids);
    //     $orders = DB::table('products_variants')
    //                 ->join('products','products_variants.product_id', '=','products.id')
    //                 ->join('order_details','products_variants.id', '=','order_details.product_variant_id')
    //                ->whereIn('orders.id', $orderId)  
    //                 ->leftJoin('orders', 'order_details.orders_id', '=', 'orders.id')
    //                 ->join('guest_user_info', 'orders.guest_user_id', '=', 'guest_user_info.id')
    //                 ->join('shippers', 'orders.shipper_id', '=', 'shippers.id')
    //                 ->leftJoin('payment', 'orders.payment_id', '=', 'payment.id')
                     // ->select('orders.order_number','orders.order_date','guest_user_info.name','guest_user_info.email','guest_user_info.street',
                     //    'guest_user_info.exterior_number','guest_user_info.reference','guest_user_info.state','guest_user_info.postal_code',
                     //    'guest_user_info.street_number','guest_user_info.mobile','products.name as product_name',
                     //    'order_details.product_sku','order_details.quantity','order_details.price','order_details.price as sub_total','orders.ship_charges','order_details.total'
                       //  , DB::raw('(CASE
                       // WHEN orders.status = "1" THEN "COMPLETED"
                       //  WHEN orders.status = "2" THEN "PREPARED"
                       //  WHEN orders.status = "3" THEN "FIRST DELIVERY ATTEMPT"
                       //   WHEN orders.status = "4" THEN "SECOND DELIVERY ATTEMPT"
                       //   WHEN orders.status = "5" THEN "CANCELLED"
                       //   ELSE "CREATED"
                       //  END) AS status')
                    // )
            //         ->toSql();
            //         dd($orders);


                  
            // $orderSubTotal = '';
            // $orderShipping = '';
            // $orderTotal = '';
            // $subTotal = 0;
            // $orderShipping = 0;
            // $subQuant = 0;
            // $shipping = '';
            // $total = '';
                   // $exportdata = array();
                   // dd($orders);
//        $usersOrders =  array();
        // foreach ($orders as $order)
        // {
        //     $orderShipping =  $order->ship_charges ;
           
        //     $orderSubTotal = $order->orderDetails['price'] * $order->orderDetails['quantity'];
        //     $subTotal = $orderSubTotal + $subTotal;
        //     $subQuant = $order->orderDetails['quantity'] + $subQuant;

        //     }

        //     $orderTotal = $subTotal + $orderShipping;
            //dd($orderTotal);
            // $Ordersubtotal = OrderDetails::where('order_number',$order->order_number)->select(DB::raw('sum(price) as subtotal'))->first();
            // $order->sub_total= $Ordersubtotal->subtotal;
        //}

//                         dd($orders);
    //     return Excel::download(new OrdersExport($orders), 'order_details.xlsx');
    // }

    public function prvpriview()
      {
        $orders = Orders::with('guestUser', 'payment', 'shipping', 'orderDetails', 'orderDetails.product','orderDetails.product')->get();
         return view('admin.lists_order')->with('orders', $orders);
      
     }

     public static function checkout_process($orderNumber)
     {
        $orderDetails = OrderDetails::where('order_number',$orderNumber)->get();
        foreach ($orderDetails as $orderDetail)
        {
            $variantId = $orderDetail->product_variant_id;
            $invtoryData = Inventory::where('product_variant_id',$variantId)->first();
            
            $sales_qty = (int)$invtoryData->sale_qty + (int)$orderDetail->quantity;
            $actual_stock = (int)$invtoryData->initial_stock - (int)$sales_qty;
            $original_price = $invtoryData->sale_cost;
            $order_price = $orderDetail->price;
            $sales_price = $original_price + $order_price;
            $sale_date = $orderDetail->created_at;
            $invtoryData->in_stock = $actual_stock;
            $invtoryData->sale_qty = $sales_qty;
            $invtoryData->sale_cost = $sales_price;
            $invtoryData->sale_date = $sale_date;
            $invtoryData->save();

             $var_update = ProductVariant::where('id',$invtoryData->product_variant_id)
           ->update(['quantity' => $actual_stock]);
            
          
           

        }
    }

    public function exportStatus(Request $request) 
    {
        $ids = $request->ids;
        $orderId = explode(',',$ids);
        $orders = DB::table('order_details')
                    ->whereIn('orders.id', $orderId)  
                     ->leftJoin('orders', 'order_details.orders_id', '=', 'orders.id')
                    ->select('orders.order_number',DB::raw('(CASE
                        WHEN status = "1" THEN "COMPLETED"
                        WHEN status = "2" THEN "PREPARED"
                        WHEN status = "3" THEN "FIRST DELIVERY ATTEMPT"
                        WHEN status = "4" THEN "SECOND DELIVERY ATTEMPT"
                        WHEN status = "5" THEN "CANCELLED"
                        ELSE "CREATED"
                        END) AS status'))
                      ->get();

        return Excel::download(new StatusExport($orders), 'order_status.xlsx');
    }

    public function import() 
    {
        Excel::import(new OrderImport,request()->file('file'));
        return back()->with('status', 'Order Status Imported Successfully');
    }
    
}
