<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductVariant;
use App\Product;
use App\ProductTag;
use App\ProductImage;
use App\Color;
use App\Category;
use App\Branch;
class ProductVariantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $categories = Category::all();
         $colors = Color::all();
         $branch = Branch::all();
         $pro_variant = ProductVariant::with('productImage','color')->get();
        return view ('admin.add_product', compact('pro_variant','categories', 'colors', 'branch'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $product_variant = ProductVariant::findOrFail($id);
        $product_variant->delete();
        //dd($product_variant);
        //return redirect('/product');
       //  ProductVariant::findOrFail("$id")->delete();
       
        return back()->with("success", "Product Variant deleted Successfully.");

    }
}
