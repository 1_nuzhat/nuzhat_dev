<?php

namespace App\Http\Controllers;

use App\Banner;
use App\Branch;
use App\Cart;
use App\CategorySubcategory;
use App\Color;
use App\Country;
use App\Family;
use App\GuestUserInfo;
use App\OrderDetails;
use App\Orders;
use App\PaymentDetail;
use App\Product;
use App\ProductVariant;
use App\Shippers;
use App\ShippingDetail;
use App\SubcategoryProducts;
use App\User;
use Carbon\Carbon;
use Carbon\CarbonTimeZone;
use Dompdf\Exception;
use Illuminate\Http\Request;
use Validator,Redirect,Response;
use Storage;
use DB;

class ApiCartController extends Controller
{

    public function cartAdd(Request $request)
    {

        try {
            set_time_limit(0);
            $rules = [
                'product_name' => 'required',
                'product_price' => 'required',
                'description' => 'required',
                'product_image' => 'required',
                'quantity' => 'required',
                'product_color' => 'required',
                'product_size' => 'required',
                'device_token' => 'required',
                'product_id' => 'required',
            ];
            $requestData = $request->all();
            $validator = \Illuminate\Support\Facades\Validator::make($requestData, $rules);
            if ($validator->fails()) {
                $response = ['success' => false, 'errors' => $validator->errors()->all()];
//
//              $response = ['success' => false, 'message' => 'Cart added failed.'];
            } else {
                $product = Product::find($requestData['product_id']);
                $colorName = $requestData['product_color'];
                $color = Color::where('color_name',$colorName)->first();


                if($product) {
                    $variants = ProductVariant::where('color_id',$color->id)->where('product_id',$product->id)->where('size',$requestData['product_size'])->first();
//                    dd($variants);
                    if($variants) {
                        $cartExist = Cart::where('variant_id',$variants->id)->where('product_id',$requestData['product_id'])->where('status',0)->first();
//                        dd($cartExist);
                        if($cartExist)
                        {
                            $quantity = $requestData['quantity'] + $cartExist->quantity;
                            $data = array('product_name' => $requestData['product_name'],
                                'product_price' => $requestData['product_price'],
                                'description' => $requestData['description'],
                                'product_image' => $requestData['product_image'],
                                'quantity' => $quantity,
                                'product_color' => $requestData['product_color'],
                                'product_size' => $requestData['product_size'],
                                'device_token' => $requestData['device_token'],
                                'variant_id' => $variants->id,
                                'product_id' => $requestData['product_id'],
                                'status' => 0);
//                        dd($variants);
                            $cartData = Cart::where('id',$cartExist->id)->update($data);

                        }else {
                            $data = array('product_name' => $requestData['product_name'],
                                'product_price' => $requestData['product_price'],
                                'description' => $requestData['description'],
                                'product_image' => $requestData['product_image'],
                                'quantity' => $requestData['quantity'],
                                'product_color' => $requestData['product_color'],
                                'product_size' => $requestData['product_size'],
                                'device_token' => $requestData['device_token'],
                                'variant_id' => $variants->id,
                                'product_id' => $requestData['product_id'],
                                'status' => 0);
//                        dd($variants);
                            $cartData = Cart::create($data);
                        }
                        $response = ['success' => true, 'message' => 'Cart added successfully.'];
                    }
                    else{
                        $response = ['success' => true, 'message' => 'Cart added failed.'];
                    }
                }
                else
                {
                    $response = ['success' => true, 'message' => 'Cart added failed.'];
                }

            }

            return Response::json($response,200);


        }
        catch (Exception $e) {

            return Response::json(['success' => false, 'message' => $e->getMessage(),'data' => []],404);

        }
    }

    public function cartlist(Request $request)
    {

        try {
            set_time_limit(0);
            $rules = [
                'device_token' => 'required',
            ];
            $requestData = $request->all();
            $validator = \Illuminate\Support\Facades\Validator::make($requestData, $rules);
            if ($validator->fails()) {

                $response = ['success' => false, 'message' => 'Cart listing failed.'];
            } else {
                $cartList = Cart::where('device_token',$request->device_token)->where('status','0')->get();
//                dd($cartList);
                $response = ['success' => true, 'message' => 'Cart list successfully.','data' => $cartList];

            }

            return Response::json($response,200);


        }
        catch (Exception $e) {

            return Response::json(['success' => false, 'message' => $e->getMessage(),'data' => []],404);

        }
    }

    public function clientInformation(Request $request)
    {
        try {set_time_limit(0);
            $rules = [
                'email' => 'required|regex:/(.+)@(.+)\.(.+)/i',
                'name' => 'required',
                'state' => 'required',
                'phone' => 'required',
                'postal_code' => 'required',
                'street' => 'required',
                'street_number' => 'required',
                'reference' => 'required',
                'device_token' => 'required',
            ];
            $requestData = $request->all();
            $validator = \Illuminate\Support\Facades\Validator::make($requestData, $rules);
            if ($validator->fails()) {

                $response = ['success' => false, 'message' => 'Client info failed.'];
            } else {
                $data = array(
                    'email' =>$requestData['email'],
                    'name' =>$requestData['name'],
                    'state' =>$requestData['state'],
                    'mobile' =>$requestData['phone'],
                    'postal_code' =>$requestData['postal_code'],
                    'street' =>$requestData['street'],
                    'street_number' =>$requestData['street_number'],
                    'reference' =>$requestData['reference'],
                    'device_token' =>$requestData['device_token']);
                $clientExist = GuestUserInfo::where('device_token',$requestData['device_token'])->first();
                $clientData ='';
                if($clientExist)
                {
                    GuestUserInfo::where('device_token',$requestData['device_token'])->update($data);
                    $clientData = GuestUserInfo::where('device_token',$requestData['device_token'])->first();
                }else {
                    $clientData = GuestUserInfo::create($data);
                }
                $response = ['success' => true, 'message' => 'Su información se ha guardado con éxito!','data'=>$clientData];
            }
            return Response::json($response,200);
        }
        catch (Exception $e) {
            return Response::json(['success' => false, 'message' => $e->getMessage(),'data' => []],404);
        }

    }

    public function editCart(Request $request)
    {
        try {
            set_time_limit(0);
            $rules = [
                'cart_id' => 'required',
                'product_name' => 'required',
                'product_price' => 'required',
                'description' => 'required',
                'product_image' => 'required',
                'quantity' => 'required',
                'product_color' => 'required',
                'product_size' => 'required',
//                'sub_total_price' => 'required',
//                'shipping_price' => 'required',
//                'total_price' => 'required',
                'device_token' => 'required',
                'product_id' => 'required',
            ];
            $requestData = $request->all();
            $validator = \Illuminate\Support\Facades\Validator::make($requestData, $rules);
            if ($validator->fails()) {
//                $response = ['success' => false, 'errors' => $validator->errors()->all()];

                $response = ['success' => false, 'message' => 'Cart edit failed.'];
            } else {
                $product = Product::find($requestData['product_id']);
                $cart = Cart::find($requestData['cart_id']);
                if($cart) {
                    if ($product) {
                        $data = array('product_name' => $requestData['product_name'],
                            'product_price' => $requestData['product_price'],
                            'description' => $requestData['description'],
                            'product_image' => $requestData['product_image'],
                            'quantity' => $requestData['quantity'],
                            'product_color' => $requestData['product_color'],
                            'product_size' => $requestData['product_size'],
//                            'sub_total_price' => $requestData['sub_total_price'],
//                            'shipping_price' => $requestData['shipping_price'],
//                            'total_price' => $requestData['total_price'],
                            'device_token' => $requestData['device_token'],
                            'product_id' => $requestData['product_id']);
                        $cartData = Cart::where('id',$requestData['cart_id'])->update($data);
                        $response = ['success' => true, 'message' => 'Cart edit successfully.'];
                    } else {
                        $response = ['success' => true, 'message' => 'Cart edit failed.'];
                    }
                }
                else
                {
                    $response = ['success' => true, 'message' => 'Cart edit failed.'];
                }

            }

            return Response::json($response,200);


        }
        catch (Exception $e) {

            return Response::json(['success' => false, 'message' => $e->getMessage(),'data' => []],404);

        }
    }

    public function deleteCart(Request $request)
    {
        try {
            set_time_limit(0);
            $rules = [
                'cart_id' => 'required',
            ];
            $requestData = $request->all();
            $validator = \Illuminate\Support\Facades\Validator::make($requestData, $rules);
            if ($validator->fails()) {
//                $response = ['success' => false, 'errors' => $validator->errors()->all()];

                $response = ['success' => false, 'message' => 'Cart delete failed.'];
            } else {
                $cart = Cart::find($requestData['cart_id']);
                if($cart) {
                    Cart::where('id',$requestData['cart_id'])->delete();
                    $response = ['success' => true, 'message' => 'Cart delete successfully.'];
                }
                else
                {
                    $response = ['success' => false, 'message' => 'Cart delete failed.'];
                }

            }

            return Response::json($response,200);


        }
        catch (Exception $e) {

            return Response::json(['success' => false, 'message' => $e->getMessage(),'data' => []],404);

        }
    }

    public function addOrder(Request $request)
    {

        try {
            set_time_limit(0);
            $rules = [
                'product_id' => 'required',
                'price' => 'required',
                'quantity' => 'required',
                'discount' => 'required',
                'tax' => 'required',
                'total' => 'required',
                'product_variant_id' => 'required',
//                'product_sku' => 'required',
//                'size' => 'required',
//                'color' => 'required',
                'shipdate' => 'required',
                'billdate' => 'required',
                'device_token' => 'required',
                'pay_by' => 'required',
                'cart_id' => 'required',
            ];
            $requestData = $request->all();
            $validator = \Illuminate\Support\Facades\Validator::make($requestData, $rules);
            if ($validator->fails()) {
                $response = ['success' => false, 'errors' => $validator->errors()->all()];

//                $response = ['success' => false, 'message' => 'Order add failed.'];
            } else {
                $ordered = '';
                $guestUser = GuestUserInfo::where('device_token', $requestData['device_token'])->first();
                if ($guestUser) {
//                    dd($request->product_id);
                    $productId = explode(',',$request->product_id);
                    $price = explode(',',$request->price);
                    $quantity = explode(',',$request->quantity);
                    $discount = explode(',',$request->discount);
                    $cart_id = explode(',',$request->cart_id);
                    $tax = $request->tax;
                    $total = $request->total;
                    $variantId = explode(',',$request->product_variant_id);
                    $lengthofArray =count($productId);
                    // $order_number = rand(10000, 99999);
                    $orderNumber = OrderDetails::where('order_number','200000')->first();

                    if($orderNumber)
                    {
                       $orderData = OrderDetails::all()->last();
             
                       $orderNumberData = $orderData->order_number;
             
                        $orderNumberData = $orderNumberData + 1;
             
                        $random = $orderNumberData;
                        
                    }else{
                        $random= 200000;
                    }
                   
                    $order_number = $random;
                    $shippersData = array('name'=>'test name' , 'contact_number' =>'1234567890');
                    $todayDate = date("Y-m-d");
                    $shippers = Shippers::create($shippersData);

                    for ($i=0;$i<$lengthofArray;$i++) {

                        $product = Product::where('id', $productId[$i])->first();
                        $productVariant = ProductVariant::where('id', $variantId[$i])->first();
                        $shippingCharge =  ShippingDetail::first();
                        if($productVariant) {
                            if ($product) {
                                $orderData = new Orders;
                                $orderData->guest_user_id = $guestUser->id;
                                $orderData->shipper_id = $shippers->id;
                                $orderData->order_number = $order_number;
                                $orderData->order_date = Carbon::now();
                                $orderData->ship_date = date("Y-m-d", strtotime($todayDate . '+ 10 days'));
                                $orderData->delivery_date = date("Y-m-d", strtotime($todayDate . '+ 11 days'));
                                $orderData->sales_tax = $tax;
                                $orderData->status = 0;
                                $orderData->ship_charges = $shippingCharge->charge;
                                $orderData->save();

                                $cart =Cart::find($cart_id[$i]);
                                $cart->status = 1;
                                $cart->save();


                                $data = array('product_id' => $product->id, 'price' => $price[$i], 'quantity' => $quantity[$i],
                                    'discount' => $discount[$i], 'total' => $total, 'product_variant_id' => $variantId[$i], 'shipdate' => $requestData['shipdate'], 'billdate' => $requestData['billdate'], 'order_number' => $order_number, 'orders_id' => $orderData->id, 'pay_by' => $requestData['pay_by']);
//                    dd($data);
                                $orderDetails = OrderDetails::create($data);


                                $response = ['success' => true, 'message' => 'Order add successfully.','order_number' => $order_number];

                            } else {
                                $response = ['success' => false, 'message' => 'product Order add failed.'];
                            }
                        }
                        else{
                            $response = ['success' => false, 'message' => 'variant Order add failed.'];
                        }
                    }
                }
                else {
                    $response = ['success' => false, 'message' => 'User details failed.'];
                }
            }
//            OrderController::checkout_process($order_number);
            return Response::json($response,200);
        }
        catch (Exception $e) {

            return Response::json(['success' => false, 'message' => $e->getMessage(),'data' => []],404);

        }

    }

    public function orderList(Request $request)
    {

        try {
            set_time_limit(0);
            $rules = [
                'device_token' => 'required',
            ];
            $requestData = $request->all();
            $validator = \Illuminate\Support\Facades\Validator::make($requestData, $rules);
            if ($validator->fails()) {
//                $response = ['success' => false, 'errors' => $validator->errors()->all()];

                $response = ['success' => false, 'message' => 'Order list failed.'];
            } else {
                $guestUser = GuestUserInfo::where('device_token',$requestData['device_token'])->first();
                if($guestUser) {
                    $orderlist = Orders::join('guest_user_info','orders.guest_user_id','=','guest_user_info.id')
                        ->leftjoin('order_details','orders.id','=','order_details.orders_id')
                        ->leftjoin('products','order_details.product_id','=','products.id')
                        ->select('orders.id as orderid','order_details.order_number as order_number','products.name as product_name','order_details.price as product_price',
                            'order_details.quantity as product_quantity','order_details.discount as product_discount','order_details.total as product_total',
                            'order_details.product_sku','order_details.size as product_size','order_details.color as product_color')
                        ->whereNull('orders.deleted_at')
                        ->groupBy('order_details.order_number')
                        ->get();
//                    dd($orderlist);
                    if ($orderlist) {
                        $response = ['success' => true, 'message' => 'Order list successfully.', 'data' => $orderlist];
                    } else {
                        $response = ['success' => false, 'message' => 'Order list not found'];
                    }
                }
                else {
                    $response = ['success' => false, 'message' => 'User not found'];
                }
            }
            return Response::json($response,200);
        }
        catch (Exception $e) {

            return Response::json(['success' => false, 'message' => $e->getMessage(),'data' => []],404);
        }

    }

    public function listUserInfo(Request $request)
    {
        try {
            set_time_limit(0);
            $rules = [
                'device_token' => 'required',
            ];
            $requestData = $request->all();
            $validator = \Illuminate\Support\Facades\Validator::make($requestData, $rules);
            if ($validator->fails()) {
//                $response = ['success' => false, 'errors' => $validator->errors()->all()];

                $response = ['success' => false, 'message' => 'User list failed.'];
            } else {
                $guestlist = GuestUserInfo::where('device_token',$requestData['device_token'])->first();
//                dd($guestlist);
                if($guestlist) {
                    $response = ['success' => true, 'message' => 'User list successfully.' ,'data' => $guestlist];
                }
                else
                {
                    $response = ['success' => false, 'message' => 'User list not found'];
                }
            }
            return Response::json($response,200);
        }
        catch (Exception $e) {

            return Response::json(['success' => false, 'message' => $e->getMessage(),'data' => []],404);
        }
    }

    public function orderCancel(Request $request)
    {
        try {
            set_time_limit(0);
            $rules = [
                'order_number' => 'required',
            ];
            $requestData = $request->all();
            $validator = \Illuminate\Support\Facades\Validator::make($requestData, $rules);
            if ($validator->fails()) {
//                $response = ['success' => false, 'errors' => $validator->errors()->all()];

                $response = ['success' => false, 'message' => 'Order failed.'];
            } else {
                $orderData = Orders::where('order_number',$requestData['order_number'])->first();
//                dd($orderData->id);
                if($orderData) {
                    $orderData= array('deleted_at' =>Carbon::now());
                    $orders = Orders::where('order_number',$requestData['order_number'])->update($orderData);
//                    $orderData->deleted_at = Carbon::now();
//                    $orderData->save();
                    $response = ['success' => true, 'message' => 'Order cancel successfully.'];
                }else{
                    $response = ['success' => false, 'message' => 'Order not found.'];
                }

            }
            return Response::json($response,200);
        }
        catch (Exception $e) {

            return Response::json(['success' => false, 'message' => $e->getMessage(),'data' => []],404);
        }
    }

    public function bannerList(Request $request)
    {
        try {
            set_time_limit(0);
            $banners = Banner::select('name','description','images','status')->get();
            $bannerArray= array();
            foreach ($banners as $banner)
            {
                $bannerArray []= ['name' => $banner->name,'description' =>strip_tags(preg_replace('/\s|&nbsp;/', '', $banner->description)),'image'=>asset('storage/images/' . $banner->images),'status' =>$banner->status];
            }
//            dd($bannerArray);
            $response = ['success' => true, 'message' => 'Banner get successfully.', 'data' =>$bannerArray];

            return Response::json($response,200);
        }
        catch (Exception $e) {

            return Response::json(['success' => false, 'message' => $e->getMessage(),'data' => []],404);
        }
    }

    public function countrylist()
    {
        try {set_time_limit(0);
            $Countries = Country::select('id','name','image')->get();
            $bannerArray= array();
            foreach ($Countries as $country)
            {
                $bannerArray []= ['country_id' =>$country->id,'name' => $country->name,'flag'=>asset('storage/images/' . $country->image)];
            }
            $response = ['success' => true, 'message' => 'Country get successfully.', 'data' =>$bannerArray];
            return Response::json($response,200);
        }
        catch (Exception $e) {

            return Response::json(['success' => false, 'message' => $e->getMessage(),'data' => []],404);
        }

    }

    public function branchlist(Request $request)
    {
        try {set_time_limit(0);
            $rules = [
                'country_id' => 'required',
            ];
            $requestData = $request->all();
            $validator = \Illuminate\Support\Facades\Validator::make($requestData, $rules);
            if ($validator->fails()) {
//                $response = ['success' => false, 'errors' => $validator->errors()->all()];

                $response = ['success' => false, 'message' => 'branch failed.'];
            }
            else {
                $country = Country::find($requestData['country_id']);
                if($country) {
                    $branchs = Branch::where('country_id',$country->id)->get();
                    $response = ['success' => true, 'message' => 'Country get successfully.', 'data' => $branchs];
                }else{
                    $response = ['success' => false, 'message' => 'Country not found.'];

                }
            }
            return Response::json($response,200);
        }
        catch (Exception $e) {

            return Response::json(['success' => false, 'message' => $e->getMessage(),'data' => []],404);
        }

    }

    public function shippingDetails(Request $request)
    {
        try {
            set_time_limit(0);
            $shippingDetail = ShippingDetail::first();
            $response = ['success' => true, 'message' => 'Shipping detail method successfully.', 'data' => $shippingDetail];
            return Response::json($response,200);
        }
        catch (Exception $e) {

            return Response::json(['success' => false, 'message' => $e->getMessage(),'data' => []],404);
        }
    }

    public function paymentDetails(Request $request)
    {
        try {set_time_limit(0);

            $branchs = PaymentDetail::first();
            $response = ['success' => true, 'message' => 'payment method successfully.', 'data' => $branchs];
            return Response::json($response,200);
        }
        catch (Exception $e) {

            return Response::json(['success' => false, 'message' => $e->getMessage(),'data' => []],404);
        }
    }

    public function branchWiseData(Request $request)
    {
        try {set_time_limit(0);
            $rules = [
                'family_id' => 'required',
                'branch_id' => 'required',
            ];
            $requestData = $request->all();
            $validator = \Illuminate\Support\Facades\Validator::make($requestData, $rules);
            if ($validator->fails()) {
                $response = ['success' => false, 'errors' => $validator->errors()->all()];
//                $response = ['success' => false, 'message' => 'product failed.'];
            }
            else {

                   $families_subcategory = DB::table('family')
                       ->leftjoin('category_subcategory', 'family.id', '=', 'category_subcategory.family_id')
                       ->leftJoin('categories', 'category_subcategory.categories_id', '=', 'categories.id')
                       ->leftJoin('subcategory_products', 'categories.id', '=', 'subcategory_products.category_id')
                       ->leftjoin('products', 'subcategory_products.product_id', '=', 'products.id')->join('products_variants', 'products.id', '=', 'products_variants.product_id')
                       ->join('products_images', 'products_variants.id', '=', 'products_images.product_variant_id')
                       ->select('family.id as category_id', 'family.name as category_name',
                           'category_subcategory.family_id as cs_category_id',
                           'category_subcategory.categories_id as cs_subcategory_id', 'categories.id as subcat_id',
                           'categories.cat_name as subcat_name', 'subcategory_products.category_id as sp_subcat_id',
                           'subcategory_products.product_id as sp_product_id', DB::raw('GROUP_CONCAT(products.name) as productname'),
                           DB::raw('GROUP_CONCAT(products.id) as productid'))
                       ->groupBy('subcategory_products.product_id')

                       ->select('family.id as family_id', 'family.name as family_name', 'categories.id as cat_id',
                           'products.name as product_name', 'products.description', 'products_variants.product_price',
                           'products_images.images')
                       ->where('category_subcategory.family_id',$requestData['family_id'])
                       ->where('products_variants.branch_id',$requestData['branch_id'])
                       ->get();
                   //dd($families_subcategory);


                   foreach ($families_subcategory as $key => $value) {
                       $filename = asset('storage/images/' . $value->images);
                       $image = Array('images' => $filename);
                       $value->images = $image;
                       $des = $value->description;
                       $value->description = strip_tags(preg_replace('/\s|&nbsp;/', '', $des));
                   }

                   $response=[
                       'success' => true,
                       'message' => 'Branch Product List',
                       'data' => $families_subcategory
                   ];
            }
            return Response::json($response,200);
        }
        catch (Exception $e) {

            return Response::json(['success' => false, 'message' => $e->getMessage(),'data' => []],404);
        }
    }


}
