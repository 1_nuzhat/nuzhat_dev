<?php

namespace App\Http\Controllers\cart;
use App\Message;
use App\Branch;
use App\Cart;
use App\GuestUserInfo;
use App\Http\Controllers\Controller;
use App\Http\Controllers\OrderController;
use App\Inventory;
use App\OrderDetails;
use App\Orders;
use App\PaymentDetail;
use App\Product;
use App\ProductVariant;
use App\Shippers;
use App\ShippingDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class CartController extends Controller
{
    //
    public function index(Request $request)
    {   
        $msg = Message::select('title')->get();
        $value = $request->session()->getId();
        $ip = $request->ip();

        $carts = Cart::join('products', 'cart.product_id', '=', 'products.id')
            ->join('products_variants', 'cart.variant_id', '=', 'products_variants.id')
            ->select('cart.product_name','cart.id', 'cart.product_color', 'cart.description', 'cart.product_image', 'cart.product_price', 'cart.quantity','cart.product_size','products_variants.product_price as mainPrice')
            ->where('cart.status',0)
            ->where('ip_address', $ip)
            ->get();

        $products = Product::leftjoin('products_variants','products.id','=','products_variants.product_id')
            ->leftjoin('products_images','products_variants.id','=','products_images.product_variant_id')
            ->select('products.name as pname','products.id as pid','products_variants.id as vid','products_variants.product_price as product_price','products_images.images')
            ->groupBy('products.id')
            ->orderBy('products.id','asc')
            ->take(20)
            ->get();
        if (count($products) > 0) {
            $lastProductId = $products->last()->pid;
        } else {
            $lastProductId = null;
        }
        $shippingMethod = ShippingDetail::first();
        if (count($carts) > 0){

            $subtotal = Cart::where('ip_address', $ip)->select(DB::raw('sum(product_price) as price'))->where('status',0)->first();
            return view('cart.cartnew',compact('carts','subtotal','products','shippingMethod','lastProductId','msg'));
    } else{
            return view('cart.empty_cart',compact('products','lastProductId','msg'));

        }

    }

    public function updateCart(Request $request)
    {
        $cart = Cart::find($request->cartid);
        $variant = Inventory::where('product_variant_id',$cart->variant_id)->first();
        $quantity=  $variant->in_stock > 0 ? $variant->in_stock : $variant->initial_stock;
        if($quantity >= $request->qunty) {
            $cart->quantity = $request->qunty;
            $cart->product_price = $request->subtotal;
            $cart->save();
            return response()->json([
                'success' => true
            ]);
        }
        else
        {
            return response()->json([
                'success' => false,'quantity' => $quantity
            ]);
        }
    }

    public function removeCart(Request $request)
    {
        $cart = Cart::where('id',$request->cartid)->delete();
        return response()->json([
            'success' => true
        ]);
    }

    public function guestConfirm(Request $request)
    {
        $total = $request->total;
        $sessionId = $request->session()->getId();
        $ip = $request->ip();
        $carts = Cart::join('products', 'cart.product_id', '=', 'products.id')
            ->join('products_variants', 'cart.variant_id', '=', 'products_variants.id')
            ->select('cart.product_name','cart.id', 'cart.product_color', 'cart.description', 'cart.product_image', 'cart.product_price', 'cart.quantity','cart.product_size','products_variants.product_price as mainPrice')
            ->where('cart.status',0)
            ->where('ip_address', $ip)
            ->get();

        $subtotal = Cart::where('ip_address', $ip)->select(DB::raw('sum(product_price) as price'))->where('status',0)->first();
        $shippingDetails = ShippingDetail::first();
        $paymentDetails = PaymentDetail::first();
        $branches= Branch::all();
        $message= Message::first();
        return view('cart.checkout',compact('total','subtotal','carts','shippingDetails','paymentDetails','branches','message'));
    }

    public function orderConfirm(Request $request)
    {

        $value = $request->session()->getId();
        $ip = $request->ip();
        $shippingCharge =  ShippingDetail::first();
        $guestInfo = new GuestUserInfo;
        $guestInfo->name= $request->fullname;
        $guestInfo->email= $request->email;
        $guestInfo->state= $request->state;
        $guestInfo->mobile_code= $request->mobile_code;
        $guestInfo->mobile= $request->mobile_number;
        $guestInfo->postal_code= $request->postal_code;
        $guestInfo->street= $request->street;
        $guestInfo->street_number= $request->street_number;
        $guestInfo->reference= $request->reference;
        $guestInfo->session_id= $value;
        $guestInfo->ip_address= $ip;
        $guestInfo->save();

        $orderNumber = OrderDetails::where('order_number','200000')->first();

        if($orderNumber)
        {
            $orderData = OrderDetails::all()->last();
            $orderNumberData = $orderData->order_number;
            $orderNumberData = $orderNumberData + 1;
            $random = $orderNumberData;
        }else{
            $random= 200000;
        }

        $order_number = $random;
        $shippersData = array('name'=>'test name' , 'contact_number' =>'1234567890');
        $todayDate = date("Y-m-d");
        $shippers = Shippers::create($shippersData);
        $cartIds = $request->cartId;
        $orderDetails = '';
        foreach ($cartIds as $cartId)
        {
            $cart = Cart::find($cartId);
            $product = Product::where('id', $cart->product_id)->with('productVariant')->first();
            $variant = ProductVariant::where('id', $cart->variant_id)->first();
            $cart->status =1;
            $cart->save();
            $orderData = new Orders;
            $orderData->guest_user_id = $guestInfo->id;
            $orderData->shipper_id = $shippers->id;
            $orderData->order_number = $order_number;
            $orderData->order_date = Carbon::now();
            $orderData->ship_date = date("Y-m-d", strtotime($todayDate . '+ 10 days'));
            $orderData->delivery_date = date("Y-m-d", strtotime($todayDate . '+ 11 days'));
            $orderData->sales_tax = $request->tax ? $request->tax : 0;
            $orderData->status = 0;
            $orderData->ship_charges = $shippingCharge->charge;
            $orderData->save();

            $data = array('product_id' => $product->id, 'price' => $cart->product_price, 'quantity' => $cart->quantity,
                'discount' => 0, 'total' => $request->total, 'product_sku' => $product->productVariant->product_sku, 'size' => $cart->product_size
            , 'color' => $cart->product_color,'product_variant_id' =>$variant->id, 'shipdate' => $orderData->ship_date, 'billdate' => $orderData->delivery_date, 'order_number' => $order_number,
                'orders_id' => $orderData->id,'cart_id' => $cartId,'pay_by' =>$request->pay_by);
          $orderDetails =  OrderDetails::create($data);
    }
        $request->session()->put('orderNumber', $orderDetails->order_number);
        OrderController::checkout_process($orderDetails->order_number);
//        Session::set('orderNumber', $orderDetails->order_number);
        return Redirect::route('thanks.page');
//        return view('cart.thanku_page');
    }

    public function thanksPage(Request $request)
    {
        $sessionId = $request->session()->getId();
        $ip = $request->ip();
        $orderNumber = Session::get('orderNumber');
        $cartsId = OrderDetails::where('order_number',$orderNumber)->select('cart_id')->get();
        $carts =array();
        $subtotal=0;
        foreach ($cartsId as $id) {
            if($id->cart_id) {
                $carts[] = Cart::join('products', 'cart.product_id', '=', 'products.id')
                    ->join('products_variants', 'cart.variant_id', '=', 'products_variants.id')
                    ->select('cart.product_name', 'cart.id as cart_id', 'cart.product_color',
                        'cart.description', 'cart.product_image', 'cart.product_price', 'cart.quantity',
                        'cart.product_size', 'products_variants.product_price as mainPrice','products_variants.product_sku')
                    ->where('cart.id', $id->cart_id)->first();
                $productTotal = Cart::where('id',$id->cart_id)->select('product_price as price')->first();
                $subtotal = $subtotal + $productTotal->price;

            }
        }
        $user = GuestUserInfo::where('ip_address',$ip)->first();
        $email = $user->email;
        $currentDate = Carbon::now();
        $orderDetails = OrderDetails::where('order_number',$orderNumber)->first();
        $shippingDetails = ShippingDetail::first();
//        return view('email.order_confirm', ['user'=>$user,'orderNumber' => $orderNumber,'carts' =>$carts,'currentDate' =>$currentDate,'subtotal'=>$subtotal,'shippingDetails'=>$shippingDetails]);

        if(env('MAIL_SERVICE')) {
            Mail::send('email.order_confirm', ['user'=>$user,'orderNumber' => $orderNumber,'carts' =>$carts,'currentDate' =>$currentDate,'subtotal'=>$subtotal,'shippingDetails'=>$shippingDetails], function ($message) use ($email) {

                $message->from(env('MAIL_FROM'), env('MAIL_FROM_NAME'));
                $message->to($email);
                $message->subject('Email Order Confirmation');
            });
        }
//        $subtotal = Cart::where('ip_address',$ip)->select(DB::raw('sum(product_price) as price'))->first();

        return view('cart.thanku_page',compact('carts','subtotal','orderNumber','user','orderDetails','shippingDetails'));
    }

    public function getDetails(Request $request)
    {
        $sessionId = $request->session()->getId();
        $ip = $request->ip();
        $carttotal = Cart::select(DB::raw('count(id) as cartcount'))->where('status',0)->where('ip_address',$ip)->first();
        $branches = Branch::all();
        $data = view('product.branch',compact('branches'))->render();
        return response()->json([
            'success' => 'success','data'=>$data,'cartTotal' => $carttotal
        ]);

    }

}
