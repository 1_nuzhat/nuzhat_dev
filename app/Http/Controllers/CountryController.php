<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use Storage;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $countries = Country::all();
        return view('admin.country', compact('countries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.add_country');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        request()->validate([
            'name' => 'required'
            
        ]);

        $country = new Country;
        $country_details = Country::where('name', $request->name)->first();
        if($country_details) {
        return redirect()->back()->withInput()->withErrors(['name'=>'Country Name already exist']);

        } else {
         if ($request->hasFile('image')) {
            $image      = $request->file('image');
            $imageName  = $request->image->getClientOriginalName();
            Storage::disk('public')->put('images/'.$imageName, file_get_contents($image));
        
        $country->name = $request->name;
        $country->image = $imageName;
        $country->save();
     }
     return redirect('/country');
       // return back()->with('status', 'Country Added Successfully');
    }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $country = Country::findOrFail($id);
        return view('admin.edit_country', compact('country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $country = Country::findOrFail($id);
        if ($request->hasFile('image')) {
            $image      = $request->file('image');
            $imageName  = $request->image->getClientOriginalName();
            Storage::disk('public')->put('images/'.$imageName, file_get_contents($image));
       
        $country->name = $request->name;
        $country->image = $imageName;
        $country->update();
    }
        return redirect('/country');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
