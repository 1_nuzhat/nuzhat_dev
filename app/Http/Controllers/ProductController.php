<?php

namespace App\Http\Controllers;

// use Google\Cloud\Storage\StorageClient;
// use Kreait\Firebase\Storage;

use App\Branch;
use App\Images;
use App\OrderDetails;
use Carbon\Carbon;
use GrahamCampbell\Binput\Facades\Binput;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Validator,Redirect,Response;
use App\Http\Controllers\ProductVariantController;
use App\Product;
use App\ProductTag;
use App\Category;
use App\ProductVariant;
use App\ProductImage;
use App\Tag;
use App\Color;
use App\Inventory;
use App\SubcategoryProducts;
use Storage;
use App\Exports\BulkExport;
use App\Imports\BulkImport;
use Maatwebsite\Excel\Facades\Excel;
use DB;
use App\Exports\FbExport;
use App\Http\Controllers\FirebaseController;
use App\Traits\CommonTrait;

class ProductController extends Controller
{
    protected $FirebaseController;
    public function __construct(FirebaseController $FirebaseController,Storage $storage)
    {
        $this->FirebaseController = $FirebaseController;
        $this->storage = $storage;
    }

    // public function __construct(Storage $storage)
    // {
    //     $this->storage = $storage;
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $products =  DB::table('products')
            ->leftjoin('products_variants', 'products.id', '=', 'products_variants.product_id')
            ->leftjoin('products_images', 'products_variants.id', '=', 'products_images.product_variant_id')
          // 
//            ->join('inventory', 'products_variants.id', '=', 'inventory.product_variant_id')
            ->select('products.id','products_images.images','products.name','products.p_status','products.p_id','products.deleted_at')
//            ->whereNull('products.deleted_at')
            ->groupBy('products.id')
            ->get();
//        dd($products);
        return view('admin.product_list', compact('products'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.add_product', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        request()->validate([
            'cat_name' => 'required',
            'name' => 'required',
            'description' => 'required',
            'p_id' => 'required',
            'p_status' => 'required',
            'title' => 'required',
            't_status' => 'required',
            'image_desc' => 'required',
            'size_desc' => 'required',
            'specification' => 'required',

        ]);

        $product_tag = new Tag;
        $products['name'] = $request->name;
        $products['description'] = $request->description;
        $products['p_id'] = $request->p_id;
        $products['p_status'] = $request->p_status;
        $products['image_desc'] = $request->image_desc;
        $products['size_desc'] = $request->size_desc;
        $products['specification'] = $request->specification;
 	    $productYou = new Product;
        $youTubeId = $productYou->getYouTubeId($request->videos);
        $products['video_id'] = $youTubeId;
        $products['video_link'] = $request->videos;
        $ProductCreate = Product::create($products);
        $insertedId = $ProductCreate->id;
        // dd($insertedId);
        $encrypted_prod_id = CommonTrait::encodeId($insertedId);
        $dynamicLink = $this->FirebaseController->sendPushNotification($encrypted_prod_id);
        //($dynamicLink);
        // if($insertedId){
           
        // }

        if($insertedId){
            foreach ($request->cat_name as $val) {
                $product_subcategory = new SubcategoryProducts;
                $product_subcategory->category_id = $val;
                $product_subcategory->product_id = $ProductCreate->id;
                $product_subcategory->save();
            }

            $prodLink = Product::find($insertedId);
            if($prodLink) {
                $prodLink->link = $dynamicLink;
                // dd($prodLink);
                $prodLink->update();
            }
        }
        else {
        $products['name'] = $request->name;
        $products['description'] = $request->description;
        $products['p_id'] = $request->p_id;
        $products['p_status'] = $request->p_status;
        $products['image_desc'] = $request->image_desc;
        $products['size_desc'] = $request->size_desc;
        $products['specification'] = $request->specification;
        $productYou = new Product;
        $youTubeId = $productYou->getYouTubeId($request->videos);
        $products['video_id'] = $youTubeId;
        $products['video_link'] = $request->videos;
        $products['link'] = $request->link;
        $ProductCreate = Product::create($products);
        }
         
         // $storageClient = $storage->getStorageClient();
         // $defaultBucket = $storage->getBucket();
         // $anotherBucket = $storage->getBucket('another-bucket');

        $product_tag->title = $request->title;
        $product_tag->t_status = $request->t_status;
        $product_tag->save();
        $data = array('product_id'=>$insertedId,'tag_id'=>$product_tag->id);
        $productTagAdd = ProductTag::Create($data);
        $productImages = $request->file('product_image');
        $i = 0;
        if ($productImages) {
            foreach ($productImages as $key=>$product_image)
            {
//                dd($product_image);
                $productImageTable = new Images;
                $name = $product_image;
                $fileExt = $product_image->getClientOriginalExtension();
                $renameFileNames = strtotime(date('d-m-Y H:i:s')).$i. '.' . $fileExt;
                $path = Storage::disk('public')->put('images/' . $renameFileNames, file_get_contents($name));
                $productImageTable->image = $renameFileNames;
                $productImageTable->product_id = $insertedId;
                $productImageTable->save();
                $i++;
            }

        }
        if($product_tag->id && $request->color_name) {
            foreach ($request->color_name as $key => $value){

                $product_variant = new ProductVariant;
                $saveImage = new ProductImage;
                $saveInventory = new Inventory;

                $product_variant->product_id = $insertedId;
                $product_variant->color_id = $request->color_name[$key];
                $product_variant->size = $request->size[$key];
                $product_variant->branch_id = $request->branch_name[$key];
                $product_variant->product_sku = $request->product_sku[$key];
                $product_variant->product_price = $request->product_price[$key];
                $product_variant->product_compare_price = $request->product_compare_price[$key];
                $product_variant->product_cost = $request->product_cost[$key];
                $product_variant->quantity = $request->quantity[$key];
                
                if(isset($request->status[$key]) && !is_null($request->status[$key])) {
                    $product_variant->status = $request->status[$key];
                }
                $product_variant->save();

                $saveInventory->product_variant_id = $product_variant->id;

                $saveInventory->initial_stock = $product_variant->quantity; 
                $saveInventory->in_stock = $product_variant->quantity;

                $saveInventory->save();

                if (isset($request->images[$key])) {
                    $image = $request->images[$key];

                    $fileExt = $image->getClientOriginalExtension();
                    $renameFileName = strtotime(date('d-m-Y H:i:s')) . '.' . $fileExt;
                    $path = Storage::disk('public')->put('images/' . $renameFileName, file_get_contents($image));
                    $saveImage->images = $renameFileName;
                }

                if (isset($request->images_dual[$key])) {

                    $imaged = $request->images_dual[$key];
                    $fileExt = $imaged->getClientOriginalExtension();
                    $renameFileNames = strtotime(date('d-m-Y H:i:s')) . '.' . $fileExt;
                    $path = Storage::disk('public')->put('images/' . $renameFileNames, file_get_contents($imaged));
                    $saveImage->images_dual = $renameFileNames;
                }
                    $saveImage->product_variant_id = $product_variant->id;

                    $saveImage->save();

            }
        }


        return back()->with('status', 'Product Created Successfully');


    }




    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $pro_variant = ProductVariant::with('productImage','color')->get();
        return view('admin.product_list', ['pro_variant' => $pro_variant]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $product = Product::find($id);
        $categories = Category::all();
        $colors = Color::all();
        $branch = Branch::all();
        $subCatProject = SubcategoryProducts::where('product_id',$id)->get();
        $pro_variant = ProductVariant::with('productImage','color')->with('branch')->where('product_id',$id)->get();
        $tags = ProductTag::with('tags')->where('product_id',$id)->orderBy('id','desc')->first();
        $productTag = ProductTag::where('product_id',$id)->with('tags')->orderBy('product_id')->first();
        $productImages = Images::where('product_id',$id)->get();
//        dd($pro_variant);

        return view('admin.edit_product', compact('pro_variant','categories','colors','product','subCatProject','tags','productTag','productImages','branch'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatenn(Request $request, $id)
    {
        request()->validate([
            'cat_name' => 'required',
            'name' => 'required',
            'description' => 'required',
            'p_id' => 'required',
            'p_status' => 'required',
            'title' => 'required',
            't_status' => 'required',

        ]);


        $product_tag = new Tag;


        $products['name'] = $request->name;
        $products['description'] = $request->description;
        $products['p_id'] = $request->p_id;
        $products['p_status'] = $request->p_status;
        $ProductCreate = Product::create($products);
        $insertedId = $ProductCreate->id;

        if($insertedId){
            foreach ($request->cat_name as $val) {
                $product_subcategory = new SubcategoryProducts;
                $product_subcategory->category_id = $val;
                $product_subcategory->product_id = $ProductCreate->id;
                $product_subcategory->save();
            }
        }
        $product_tag->title = $request->title;
        $product_tag->t_status = $request->t_status;
        $product_tag->save();
        if($product_tag->id && $request->color_name) {
            foreach ($request->color_name as $key => $value){

                $product_variant = new ProductVariant;
                $saveImage = new ProductImage;

                $product_variant->product_id = $insertedId;
                $product_variant->color_id = $request->color_name[$key];
                $product_variant->size = $request->size[$key];
                $product_variant->branch_id = $request->branch_name[$key];
                $product_variant->product_sku = $request->product_sku[$key];
                $product_variant->product_price = $request->product_price[$key];
                $product_variant->product_compare_price = $request->product_compare_price[$key];
                $product_variant->product_cost = $request->product_cost[$key];
                $product_variant->quantity = $request->quantity[$key];
               
                if(isset($request->status[$key]) && !is_null($request->status[$key])) {
                    $product_variant->status = $request->status[$key];
                }
                $product_variant->save();


                if (isset($request->images[$key])) {
                    $image      = $request->images[$key];

                    $fileExt = $image->getClientOriginalExtension();
                    $renameFileName = strtotime(date('d-m-Y H:i:s')).'.'.$fileExt;
                    $path = Storage::disk('public')->put('images/'.$renameFileName, file_get_contents($image));


                    $imaged      = $request->images_dual[$key];
                    $fileExt = $imaged->getClientOriginalExtension();
                    $renameFileNames = strtotime(date('d-m-Y H:i:s')).'.'.$fileExt;
                    $path = Storage::disk('public')->put('images/'.$renameFileNames, file_get_contents($imaged));
                    // Save image into database

                    $saveImage->product_variant_id = $product_variant->id;
                    $saveImage->images = $renameFileName;
                    $saveImage->images_dual = $renameFileNames;
                    $saveImage->save();
                }

            }
        }


        return back()->with('status', 'Product Created Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::findOrFail($id);
        $product->delete();
        return back()->with('/product_list')->with('status', 'Product Deleted');

    }

    public function export(Request $request)
    {
        return Excel::download(new BulkExport, 'bulkData.xlsx');
    }


    public function activeAll(Request $request)
    {

        $ids = $request->ids;

        \DB::table("products")->whereIn('id',explode(",",$ids))->update(['p_status'=>1]);

        return response()->json(['success'=>"status updated successfully."]);
    }

    public function inactiveAll(Request $request)
    {

        $ids = $request->ids;

        \DB::table("products")->whereIn('id',explode(",",$ids))->update(['p_status'=>0]);

        return response()->json(['success'=>"status updated successfully."]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateProject(Request $request)
    {
        request()->validate([
            'cat_name' => 'required',
            'name' => 'required',
            'description' => 'required',
            'p_id' => 'required',
            'p_status' => 'required',
            'title' => 'required',
            't_status' => 'required',
            'image_desc' => 'required',
            'size_desc' => 'required',
            'specification' => 'required',

        ]);
//        dd($request);
        $product_tag ='';
        $tagData = Tag::find($request->product_name);
        if($tagData)
        {
            $product_tag= $tagData;
        }
        else
        {
            $product_tag= new Tag;
        }

        $product = Product::find($request->product_id);

        $prodId = $request->product_id;
        // dd($insertedId);
        $encrypted_prod_id = CommonTrait::encodeId($prodId);
        $dynamicLink = $this->FirebaseController->sendPushNotification($encrypted_prod_id);

        $product->name = $request->name;
        $product->description = $request->description;
        $product->p_id = $request->p_id;
        $product->p_status = $request->p_status;
        $product->image_desc = $request->image_desc;
        $product->size_desc = $request->size_desc;
        $product->specification = $request->specification;
        $product->link = $dynamicLink;
        $productYou = new Product;
        $youTubeId = $productYou->getYouTubeId($request->videos);
        $product->video_id = $youTubeId;
        $product->video_link = $request->videos;
        $ProductCreate = $product->save();
        $insertedId = $request->product_id;

        SubcategoryProducts::where('product_id',$insertedId)->delete();
        if($insertedId){

            foreach ($request->cat_name as $val) {
                $product_subcategory = new SubcategoryProducts;
                $product_subcategory->category_id = $val;
                $product_subcategory->product_id = $insertedId;
                $product_subcategory->save();
            }
        }

        $product_tag->title = $request->title;
        $product_tag->t_status = $request->t_status;
        $product_tag->save();
//         $pVariants = ProductVariant::where('product_id',$insertedId)->get();
//        foreach ($pVariants as $pVariant)
//        {
//            Inventory::where('product_variant_id',$pVariant->id)->delete();
//        }
//        ProductVariant::where('product_id',$insertedId)->delete();
        $productImages = $request->file('product_image');
        $i = 0;
        if ($productImages) {
            foreach ($productImages as $product_image)
            {
//                dd($product_image);
                $productImageTable = new Images;
                $name = $product_image;
//                dd($name);
                $fileExt = $product_image->getClientOriginalExtension();
                $renameFileNames = strtotime(date('d-m-Y H:i:s')).$i. '.' . $fileExt;
                $path = Storage::disk('public')->put('images/' . $renameFileNames, file_get_contents($name));

                $productImageTable->image = $renameFileNames;
                $productImageTable->product_id = $insertedId;
                $productImageTable->save();
                $i++;
            }

        }
        if(isset($request->variantid))
        { $j=0;
            foreach ($request->variantid as $key =>$variantid) {

                $product_variant = ProductVariant::find($variantid);
//                dd($request);
                if ($product_variant) {
                    $saveImage = ProductImage::where('product_variant_id', $variantid)->first();
                    $saveInventory = Inventory::where('product_variant_id', $variantid)->first();

                    $product_variant->product_id = $insertedId;
                    $product_variant->color_id = $request->color_name_edit[$key];
                    $product_variant->size = $request->size_edit[$key];
                    $product_variant->branch_id = $request->branch_name_edit[$key];
                    $product_variant->product_sku = $request->product_sku_edit[$key];
                    $product_variant->product_price = $request->product_price_edit[$key];
                    $product_variant->product_compare_price = $request->product_compare_price_edit[$key];
                    $product_variant->product_cost = $request->product_cost_edit[$key];
                    if (isset($request->quantity_edit[$key]) && !is_null($request->quantity_edit[$key])) {

                        $product_variant->quantity = $request->quantity_edit[$key];
                    }
                    if (isset($request->status_edit[$key]) && !is_null($request->status_edit[$key])) {
                        $product_variant->status = $request->status_edit[$key];
                    }
                    $product_variant->save();
//                dd($request);
                    $saveInventory->product_variant_id = $variantid;

                    $saveInventory->initial_stock = $product_variant->quantity;
                    $saveInventory->in_stock = $product_variant->quantity;

                    $saveInventory->save();

                    if (isset($request->images_edit[$key])) {
                        $image = $request->images_edit[$key];

                        $fileExt = $image->getClientOriginalExtension();
                        $renameFileName = strtotime(date('d-m-Y H:i:s')) . $j . '.' . $fileExt;
                        $path = Storage::disk('public')->put('images/' . $renameFileName, file_get_contents($image));
                        $saveImage->images = $renameFileName;
                    }

                    if (isset($request->images_dual_edit[$key])) {

                        $imaged = $request->images_dual_edit[$key];
                        $fileExt1 = $imaged->getClientOriginalExtension();
                        $renameFileNames1 = strtotime(date('d-m-Y H:i:s')) . $j . 'dual.' . $fileExt1;
                        $path = Storage::disk('public')->put('images/' . $renameFileNames1, file_get_contents($imaged));
                        $saveImage->images_dual = $renameFileNames1;
                    }
                    // Save image into database

                    $saveImage->product_variant_id = $product_variant->id;
                    $saveImage->save();
                    $j++;
                }
            }
        }
        if($product_tag->id && $request->color_name) {
            $j =0;
//            dd($request);
            foreach ($request->color_name as $key => $value){

                $product_variant = new ProductVariant;
                $saveImage = new ProductImage;
                $saveInventory = new Inventory;

                $product_variant->product_id = $insertedId;
                $product_variant->color_id = $request->color_name[$key];
                $product_variant->size = $request->size[$key];
                $product_variant->branch_id = $request->branch_name[$key];
                $product_variant->product_sku = $request->product_sku[$key];
                $product_variant->product_price = $request->product_price[$key];
                $product_variant->product_compare_price = $request->product_compare_price[$key];
                $product_variant->product_cost = $request->product_cost[$key];
                if(isset($request->quantity[$key]) && !is_null($request->quantity[$key])) {

                    $product_variant->quantity = $request->quantity[$key];
                }
                if(isset($request->status[$key]) && !is_null($request->status[$key])) {
                    $product_variant->status = $request->status[$key];
                }
                $product_variant->save();
//                dd($request);
                $saveInventory->product_variant_id = $product_variant->id;

                $saveInventory->initial_stock = $product_variant->quantity;
                $saveInventory->in_stock = $product_variant->quantity;

                $saveInventory->save();

                if (isset($request->images[$key])) {
                    $image = $request->images[$key];

                    $fileExt = $image->getClientOriginalExtension();
                    $renameFileName = strtotime(date('d-m-Y H:i:s')) .$j. '.' . $fileExt;
                    $path = Storage::disk('public')->put('images/' . $renameFileName, file_get_contents($image));
                    $saveImage->images = $renameFileName;
                }

                    if (isset($request->images_dual[$key])) {

                        $imaged = $request->images_dual[$key];
                        $fileExt1 = $imaged->getClientOriginalExtension();
                        $renameFileNames1 = strtotime(date('d-m-Y H:i:s')) .$j. 'dual.' . $fileExt1;
                        $path = Storage::disk('public')->put('images/' . $renameFileNames1, file_get_contents($imaged));
                        $saveImage->images_dual = $renameFileNames1;
                    }
                    // Save image into database

                    $saveImage->product_variant_id = $product_variant->id;
                    $saveImage->save();
                    $j++;
            }
        }


        return back()->with('status', 'Product Created Successfully');

    }

    public static function getChecked($cat,$id)
    {
        $subCatProject = SubcategoryProducts::where('product_id',$id)->where('category_id',$cat)->first();
        return $subCatProject;
    }

    public function removeImage(Request $request)
    {
        $images = Images::find($request->idval);
        if($images)
        {
            Images::where('id',$request->idval)->delete();
            return true;
        }
        else
        {
            return false;
        }

    }

    public function deleteProduct(Request $request)
    {
        $product = Product::find($request->productId);
//        dd($request->productId);
        if($request->type == 'delete') {
            $product->deleted_at = Carbon::now();
            $product->is_delete = 1;
            $product->save();
        }else{
            $params = array('is_delete' => null,'deleted_at'=> null);
            Product::where('id',$request->productId)->update($params);
        }

        return response()->json([
            'success' => true
        ]);
    }

    public function removeVariant(Request $request)
    {
        $variant = ProductVariant::find($request->variantId);
        $variant_images = ProductImage::where('product_variant_id',$variant->id)->delete();
        $variant->delete();
        return response()->json([
            'success' => true
        ]);

    }

    public function getSizePrice(Request $request)
    {
        $products = ProductVariant::where('product_id',$request->productId)->where('color_id',$request->colorid)->where('size',$request->size)->first();
        return response()->json([
            'success' => true,'data' =>$products->product_price
        ]);
    }

public static function getGlobalPrice($pid)
    {
        $totalPrice =  OrderDetails::where('product_id',$pid)->select(DB::raw('IFNULL(sum(price),0) as globalprice'))->first();
        return $totalPrice;
    }
    public static function getGlobalQuty($pid)
    {
        $totalQuantity =  OrderDetails::where('product_id',$pid)->select(DB::raw('IFNULL(sum(quantity),0) as globalquantity'))->first();
        return $totalQuantity;
    }

    public function fbexport(Request $request) 
    {
       $condition = 'new';
       $brand = 'gangabox';
       $products =  DB::table('products')
        ->leftjoin('products_variants', 'products.id', '=', 'products_variants.product_id')
        ->join('products_images', 'products_variants.id', '=', 'products_images.product_variant_id')
        ->select('products.p_id','products.name',
        'products.description','products_variants.product_price','products.link','products_images.images','products.specification as gangabox', 
            DB::raw('(CASE 
                        WHEN products.p_status = "1" THEN "in stock"
                        ELSE "out stock"
                        END) AS p_status'))
        ->groupBy('products.id')
        ->get();
        $exportdata = array();
         foreach ($products as $key => $value) {
        $des = $value->description;
        $des_new = strip_tags(preg_replace('/\s|&nbsp;/', '', $des));
        $filename = asset('storage/images/' . $value->images);
        $image = Array('images' => $filename);
        $value->images = $image;
            $exportdata[] = array(
                'p_id' => $value->p_id,
                'name' => $value->name,
                'description' => $des_new,
                'p_status' =>$value->p_status,
                'condition' =>$condition,
                'product_price' =>$value->product_price,
                'link' =>$value->link,
                'image_link' =>json_encode($value->images),
                'brand' =>$brand,
            
            );
          
            }


//=========================
    $delimiter = ",";
    $filename = "catalogue"  . ".csv";
    
    //create a file pointer
    $f = fopen('php://memory', 'w');
    
    //set column headers
    $fields = array('product id', 'title', 'description','availability', 'condition', 'price', 'link', 'image_link', 'brand');
    fputcsv($f, $fields, $delimiter);
    //output each row of the data, format line as csv and write to file pointer
   

    foreach ($exportdata as $key => $row) {
        //echo();
        $field = preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $row['name']);
         $lineData = array($row['p_id'], $field, $row['description'],
             $row['p_status'], $row['condition'], $row['product_price'], $row['link'], json_decode($row['image_link'])->images, $row['brand']);
         fputcsv($f, $lineData, $delimiter);
    }
   // dd(1);

    
    //move back to beginning of file
    fseek($f, 0);
    
    //set headers to download file rather than displayed
    header('Content-Type: text/csv');
    header('Content-Disposition: attachment; filename="' . $filename . '";');
    
    //output all remaining data on a file pointer
    fpassthru($f);
    exit;
//==========================

            //dd($exportdata);
        return Excel::download(new FbExport($exportdata), 'catalogue.xlsx');
    }

}
