<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\OrderDetails;
use Carbon\Carbon;
use DB;
use DateTime;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

      /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function adminHome()
    {
        $today = Carbon::today();
        $order = OrderDetails::select('id','price','quantity','created_at')
        ->whereDate('created_at', $today)
        ->get();
        $branch_orders = DB::table('branch')
            ->join('products_variants','branch.id', '=','products_variants.branch_id')
            ->join('order_details', 'products_variants.id', '=', 'order_details.product_variant_id')
            ->select('branch.name as branch_name','products_variants.branch_id','order_details.price as branch_price','order_details.quantity as branch_quantity')
             ->whereDate('order_details.created_at', $today)
            ->get();
        $price = 0;
        $qty = 0;
        foreach($order as $orders) {
            $price += $orders->price;
            $qty += $orders->quantity;
        }

        $guadalazara_price = 0;
        $guadalazara_qty = 0;
        foreach ($branch_orders as $key => $value) {
            $branch_name = $value->branch_name;
            $branch_qty = $value->branch_quantity;
            if ($branch_name == 'GUADALAZARA') {
            $guadalazara_price += $value->branch_price;
            $guadalazara_qty += $value->branch_quantity;
   
        }
        }

        $mont_price = 0;
        $mont_qty = 0;
        foreach ($branch_orders as $key => $value) {
            $branch_name = $value->branch_name;
            $branch_qty = $value->branch_name;
            if ($branch_name == 'MONTERREY') {
            $mont_price += $value->branch_price;
            $mont_qty += $value->branch_quantity;
    
        }
        }

        $puebla_price = 0;
        $puebla_qty = 0;
        foreach ($branch_orders as $key => $value) {
            $branch_name = $value->branch_name;
            $branch_qty = $value->branch_name;
            if ($branch_name == 'PUEBLA') {
            $puebla_price += $value->branch_price;
            $puebla_qty += $value->branch_quantity;
        }
        }

        $cdmx_price = 0;
        $cdmx_qty = 0;
        foreach ($branch_orders as $key => $value) {
            $branch_name = $value->branch_name;
            $branch_qty = $value->branch_name;
            if ($branch_name == 'CDMX') {
            $cdmx_price += $value->branch_price;
            $cdmx_qty += $value->branch_quantity;
        }
        }
        $date = \Carbon\Carbon::today()->subDays(2);
        $latest_product = DB::table('products_variants')
            ->join('order_details', 'products_variants.id', '=', 'order_details.product_variant_id')
            ->join('products_images', 'products_variants.id', '=', 'products_images.product_variant_id')
            ->select('products_variants.product_sku','products_images.images')
            ->where('order_details.created_at', '>=', $date)
            ->get();
       return view('admin.index',compact('price','qty','guadalazara_price','guadalazara_qty','mont_price','mont_qty','puebla_price','puebla_qty','cdmx_price','cdmx_qty','latest_product'));
    }

}
