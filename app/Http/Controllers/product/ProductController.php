<?php

namespace App\Http\Controllers\product;

use App\Cart;
use App\CategorySubcategory;
use App\Color;
use App\Family;
use App\Http\Controllers\Controller;
use App\Images;
use App\Inventory;
use App\Product;
use App\ProductImage;
use App\ProductVariant;
use App\SubcategoryProducts;
use App\Traits\CommonTrait;
use Illuminate\Http\Request;
use DB,Storage;
use Illuminate\Support\Facades\Session;
use Response;
use App\Message;


class ProductController extends Controller
{
    //
    public function index(Request $request){
        $value = $request->session()->getId();
        $families = Family::all();
        $familySubcats= array();
        foreach ($families as $family)
        {
            $subfam = CategorySubcategory::where('family_id',$family->id)->with('category')->get();
            $familySubcats[] =['fname'=>$family->name,'fid'=>$family->id,'subcat' =>$subfam];
        }
        $products = Product::leftjoin('products_variants','products.id','=','products_variants.product_id')
            ->leftjoin('products_images','products_variants.id','=','products_images.product_variant_id')
            ->select('products.name as pname','products.id as pid','products_variants.id as vid','products_variants.product_price as product_price','products_images.images')
            ->groupBy('products.id')
            ->orderBy('products.id','asc')
            ->take(20)
            ->get();
        if (count($products) > 0) {
            $lastProductId = $products->last()->pid;
        } else {
            $lastProductId = null;
        }
//        dd($request->route()->getName());
    	return view('product.product_list',compact('familySubcats','products','lastProductId'));
    }

    public function varianSize($id,$product_id)
    {
        $sizes = ProductVariant::where('color_id',$id)->where('product_id',$product_id)->select('size')->groupBy('size')->get()->toArray();
        return $sizes;
    }

    public function productDescription($id,$code)
    {
        $msg = Message::select('title')->get();
        $productId = CommonTrait::decodeId($id);
        $pvariant = Product::where('id',$productId)->with('productVariant')->first();
        
        $str = str_replace('https://arrivederci.page.link/','',$pvariant->link);
        //dd($str);
        $varients = ProductVariant::where('product_id',$productId)->with('colorData')->with('productImage')->get();
        $colors = ProductVariant::where('product_id',$productId)->with('colorData')->with('productImage')->groupBy('products_variants.color_id')->get();
//        dd($colors);
        $productImage = Images::where('product_id',$productId)->get();
        $products = Product::leftjoin('products_variants','products.id','=','products_variants.product_id')
            ->leftjoin('products_images','products_variants.id','=','products_images.product_variant_id')
            ->select('products.name as pname','products.id as pid','products_variants.id as vid','products_variants.product_price as product_price','products_images.images')
            ->groupBy('products.id')
            ->orderBy('products.id','asc')
            ->take(5)
            ->get();
        if (count($products) > 0) {
            $lastProductId = $products->last()->pid;
        } else {
            $lastProductId = null;
        }
//        dd($products);
        return view('product.product',compact('pvariant','varients','productImage','products','colors','lastProductId','msg','str'));

    }

    public function subCatProduct(Request $request,$id)
    {
//        dd($request);
        $subId = CommonTrait::decodeId($id);
        $branch = Session::get('branch_id');
//        dd($subId);
//        $subProduct = SubcategoryProducts::where('')->get();
        $families = Family::all();
        $familySubcats= array();
        foreach ($families as $family)
        {
            $subfam = CategorySubcategory::where('family_id',$family->id)->with('category')->get();
            $familySubcats[] =['fname'=>$family->name,'fid'=>$family->id,'subcat' =>$subfam];
        }
        $request->session()->put('category_id', $subId);

        $products = Product::leftjoin('products_variants','products.id','=','products_variants.product_id')
            ->leftjoin('products_images','products_variants.id','=','products_images.product_variant_id')
            ->leftjoin('subcategory_products','products.id','=','subcategory_products.product_id')
            ->select('products.name as pname','products.id as pid','products_variants.id as vid','products_variants.product_price as product_price','products_images.images')
            ->where('products.p_status',1)
            ->where('subcategory_products.category_id',$subId)
            ->groupBy('products.id')
            ->orderBy('products.id','asc')
            ->when($branch, function ($products) use ($branch) {

                $products->where('products_variants.branch_id',$branch);
            })
            ->take(20)
            ->get();
        if (count($products) > 0) {
            $lastProductId = $products->last()->pid;
        } else {
            $lastProductId = null;
        }

        return view('product.sub_product_list',compact('familySubcats','products','lastProductId'));
//        return response()->json([
//            'success' => 'success','data'=>$data
//        ]);
    }

    public function imageView(Request $request)
    {
        $imageid = $request->imageid;
        $productImage = ProductImage::find($imageid);
        $data = view('product.image_view',compact('productImage'))->render();
        return response()->json([
            'success' => 'success','data'=>$data
        ]);
    }

    public function productSize(Request $request)
    {
        $colorId = $request->color;
        $productId = $request->productId;
        $branch = Session::get('branch_id');
        $productSizes = ProductVariant::where('product_id',$productId)->where('color_id',$colorId)->select('size')->groupBy('size')
//            ->when($branch, function ($products) use ($branch) {
//                $products->where('branch_id', $branch);
//            })
            ->get();
        $data = view('product.image_sizes',compact('productSizes'))->render();
//        $availbleQuantity = $productSizes
        return response()->json([
            'success' => 'success','data'=>$data
        ]);
    }

    public function addCart(Request $request)
    {
        $value = $request->session()->getId();
        $ip = $request->ip();
        $productId = $request->productId;
        $colorId = $request->colorId;
        $colorSize = $request->colorSize;
        $products = Product::find($productId);
        $color = Color::find($colorId);
        $variants = ProductVariant::where('color_id',$colorId)->where('product_id',$productId)->where('size',$colorSize)->first();
        $InstockData = Inventory::where('product_variant_id',$variants->id)->first();
        if ($InstockData) {
            $quantity = $InstockData->in_stock > 0 ? $InstockData->in_stock : $InstockData->initial_stock;
            if ($request->quanti <= $quantity) {
                $ipAddress = $request->ip();
                $cartExist = Cart::where('variant_id',$variants->id)->where('product_id',$productId)->where('status',0)->where('ip_address',$ipAddress)->first();
                if($cartExist)
                {
                    $cartExist->quantity = $cartExist->quantity + $request->quanti;
                    $cartExist->product_price = ($variants->product_price * $cartExist->quantity);

                    $cartExist->save();
                }
                else {
                    $productImage = ProductImage::where('product_variant_id', $variants->id)->first();
                    $carts = new cart;
                    $carts->product_name = $products->name;
                    $carts->product_price = ($variants->product_price * $request->quanti);
                    $carts->description = $products->description;
                    $carts->quantity = $request->quanti;
                    $carts->product_color = $color->color_name;
                    $carts->product_id = $productId;
                    $carts->product_size = $colorSize;
                    $carts->session_id = $value;
                    $carts->ip_address = $ip;
                    $carts->variant_id = $variants->id;
                    $carts->product_image = asset('storage/images/' . $productImage->images);
//        dd($carts->product_image );
                    $carts->save();
                }
                return response()->json([
                    'success' => true
                ]);
            } else {
                return response()->json([
                    'success' => false, 'quantity' => $quantity
                ]);
            }
        }
        else {
            return response()->json([
                'success' => false, 'quantity' => 0
            ]);
        }
    }

    public function videoView(Request $request)
    {
        $videoid = $request->videoid;
        $productVideo = Product::find($videoid);
        $data = view('product.video_view',compact('productVideo'))->render();
        return response()->json([
            'success' => 'success','data'=>$data
        ]);
    }
    public function getMoreData(Request $request)
    {
        $familyId = Session::get('family_id');
        $branch = Session::get('branch_id');
//        dd($request->lastId);
        $pageNo =20;
//        if(empty($request->lastId))
//        {}
//        $pageNo = 3;
//        dd($request->lastId);
        if (!empty($request->lastId)) {
            $products = DB::table('family')->where('family.id', $familyId)
                ->leftjoin('category_subcategory', 'family.id', '=', 'category_subcategory.family_id')
                ->leftJoin('categories', 'category_subcategory.categories_id', '=', 'categories.id')
                ->leftJoin('subcategory_products', 'categories.id', '=', 'subcategory_products.category_id')
                ->leftjoin('products', 'subcategory_products.product_id', '=', 'products.id')
                ->join('products_variants', 'products.id', '=', 'products_variants.product_id')
                ->join('products_images', 'products_variants.id', '=', 'products_images.product_variant_id')
                ->select('family.id as category_id', 'family.name as category_name', 'category_subcategory.family_id as cs_category_id', 'category_subcategory.categories_id as cs_subcategory_id', 'categories.id as subcat_id', 'categories.cat_name as subcat_name', 'subcategory_products.category_id as sp_subcat_id', 'subcategory_products.product_id as sp_product_id', DB::raw('GROUP_CONCAT(products.name) as productname'), DB::raw('GROUP_CONCAT(products.id) as productid'))->groupBy('subcategory_products.product_id')
                // ->get();
                ->select('family.id as family_id', 'family.name as family_name', 'categories.id as cat_id', 'products.id as pid', 'products.name as product_name', 'products.description', 'products_variants.product_price', 'products_images.images')
                ->where('products.id', '>', $request->lastId)
                ->take($pageNo)
                ->orderBy('products.id','asc')
                ->when($branch, function ($products) use ($branch) {
                    $products->where('products_variants.branch_id', $branch);
                })
                ->get();
//        dd($products);
            if (count($products) > 0) {
                $lastProductId = $products->last()->pid;
            } else {
                $lastProductId = null;
            }
//        dd($lastProductId);
            $data = view('product.productscroll', compact('products'))->render();
            return response()->json([
                'success' => 'success', 'data' => $data, 'lastProductId' => $lastProductId
            ]);
        } else {
            return response()->json([
                'success' => false,'lastProductId' => null
            ]);
        }
    }

}
