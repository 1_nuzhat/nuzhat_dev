<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PaymentDetail;

class PaymentDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
         $payment_detail = PaymentDetail::all();
         return view('admin.payment_list', compact('payment_detail'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.add_payment');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
      // $payment = PaymentDetail::updateOrCreate(
      //   ['title' =>  request('title')],
      //   ['description' => request('description')]
      //  );
      // return back()->with('status', 'Successfully');
         request()->validate([
            'online_pay_title' => 'required'
            
        ]);
         
        $payment = new PaymentDetail;

        $chk_details = PaymentDetail::where('online_pay_title', $request->online_pay_title)->first();

        if($chk_details) {
            //$payment = $chk_details; 
        return redirect()->back()->withInput()->withErrors(['online_pay_title'=>'Payment Title already exist']);

        } else {
            $payment->online_pay_title = $request->online_pay_title;
            $payment->online_pay_description = $request->online_pay_description;
            $payment->cod_title = $request->cod_title;
            $payment->save();
            return back()->with('status', 'Payment Text Added Successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $payment_details = PaymentDetail::findOrFail($id);
        return view('admin.edit_payment_details', ['payment_details' => $payment_details]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $payment_detail = PaymentDetail::findOrFail($id);
        
        $payment_detail->online_pay_title = $request->online_pay_title;
        $payment_detail->online_pay_description = $request->online_pay_description;
        $payment_detail->cod_title = $request->cod_title;
        $payment_detail->update();
        return redirect('/payment_details');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
