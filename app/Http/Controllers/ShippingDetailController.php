<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ShippingDetail;

class ShippingDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $shipping_list = ShippingDetail::all();
        return view('admin.shipping_list',compact('shipping_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.add_shipping');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         request()->validate([
            'title' => 'required',
            'charge' => 'required'
            
        ]);
         
        $shipping = new ShippingDetail;
        $chk_details = ShippingDetail::where('title', $request->title)->first();
        if($chk_details) {
           // $shipping = $chk_details; 
        return redirect()->back()->withInput()->withErrors(['title'=>'Payment Title already exist']);

        } else {
        $shipping->title = $request->title;
        $shipping->charge = $request->charge;
        $shipping->save();
            return back()->with('status', 'Shipping Added Successfully');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $shipping_details = ShippingDetail::findOrFail($id);
        return view('admin.edit_shipping', ['shipping_details' => $shipping_details]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $shipping_details = ShippingDetail::findOrFail($id);
        $shipping_details->title = $request->title;
        $shipping_details->charge = $request->charge;
        $shipping_details->update();
        return redirect('/shipping_details');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
