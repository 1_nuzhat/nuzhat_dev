<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Color extends Model
{
    //
      protected $table = "colors";

    public function productVariant()
    {
    	return $this->belongsTo('App\productVariant', 'color_id', 'id');
    }
}
