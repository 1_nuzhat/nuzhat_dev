$(function () {

    function generateData(baseval, count, yrange) {
        var i = 0;
        var series = [];
        while (i < count) {
            //var x =Math.floor(Math.random() * (750 - 1 + 1)) + 1;;
            var y = Math.floor(Math.random() * (yrange.max - yrange.min + 1)) + yrange.min;
            var z = Math.floor(Math.random() * (75 - 15 + 1)) + 15;

            series.push([baseval, y, z]);
            baseval += 86400000;
            i++;
        }
        return series;
    }

    // 3D Bubble Chart -------> BUBBLE CHART
    var options_3d = {
        series: [{
            name: 'Product1',
            data: generateData(new Date('11 Feb 2017 GMT').getTime(), 20, {
                min: 10,
                max: 60
            })
        },
        {
            name: 'Product2',
            data: generateData(new Date('11 Feb 2017 GMT').getTime(), 20, {
                min: 10,
                max: 60
            })
        },
        {
            name: 'Product3',
            data: generateData(new Date('11 Feb 2017 GMT').getTime(), 20, {
                min: 10,
                max: 60
            })
        },
        {
            name: 'Product4',
            data: generateData(new Date('11 Feb 2017 GMT').getTime(), 20, {
                min: 10,
                max: 60
            })
        }],
        chart: {
            fontFamily: '"Nunito Sans", sans-serif',
            height: 350,
            type: 'bubble',
            toolbar: {
                show: false,
            },
            
        },
        grid: {
            borderColor: 'rgba(0,0,0,0.3)',
        },
        colors: ['#2962ff', '#4fc3f7', '#ffaf0e', '#f62d51'],
        dataLabels: {
            enabled: false
        },
        fill: {
            type: 'gradient',
        },
        xaxis: {
            tickAmount: 12,
            type: 'datetime',
            labels: {
                rotate: 0,
                style: {
                    colors: ["#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2"]
                }
            }
        },
        yaxis: {
            max: 70,
            labels: {
                style: {
                     colors: ["#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2"]   
                }
            }
        },
        legend: {
            labels: {
                colors: ["#a1aab2"]
            }
        },
        tooltip: {
            theme: "dark"
        },
        theme: {
            palette: 'palette2'
        }
    };

    var chart_bubble_3d = new ApexCharts(document.querySelector("#chart-bubble-3d"), options_3d);
    chart_bubble_3d.render();

    function generateDayWiseTimeSeries(baseval, count, yrange) {
        var i = 0;
        var series = [];
        while (i < count) {
            var y = Math.floor(Math.random() * (yrange.max - yrange.min + 1)) + yrange.min;

            series.push([baseval, y]);
            baseval += 86400000;
            i++;
        }
        return series;
    }


    // Datetime Scatter Chart -------> SCATTER CHART
    var options_datetime = {
        series: [{
            name: 'TEAM 1',
            data: generateDayWiseTimeSeries(new Date('11 Feb 2017 GMT').getTime(), 20, {
                min: 10,
                max: 60
            })
        },
        {
            name: 'TEAM 2',
            data: generateDayWiseTimeSeries(new Date('11 Feb 2017 GMT').getTime(), 20, {
                min: 10,
                max: 60
            })
        },
        {
            name: 'TEAM 3',
            data: generateDayWiseTimeSeries(new Date('11 Feb 2017 GMT').getTime(), 30, {
                min: 10,
                max: 60
            })
        },
        {
            name: 'TEAM 4',
            data: generateDayWiseTimeSeries(new Date('11 Feb 2017 GMT').getTime(), 10, {
                min: 10,
                max: 60
            })
        },
        {
            name: 'TEAM 5',
            data: generateDayWiseTimeSeries(new Date('11 Feb 2017 GMT').getTime(), 30, {
                min: 10,
                max: 60
            })
        },
        ],
        chart: {
            fontFamily: '"Nunito Sans", sans-serif',
            height: 350,
            type: 'scatter',
            toolbar: {
                show: false,
            },
            
            zoom: {
                type: 'xy'
            }
        },
        colors: ['#2962ff', '#4fc3f7', '#ffaf0e', '#f62d51', '#adb5bd'],
        dataLabels: {
            enabled: false
        },
        grid: {
            xaxis: {
                lines: {
                    show: true
                }
            },
            yaxis: {
                lines: {
                    show: true
                }
            },
            borderColor: 'rgba(0,0,0,0.3)',
        },
        xaxis: {
            type: 'datetime',
            labels: {
                style: {
                    colors: ["#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2"]
                }
            }
        },
        yaxis: {
            max: 70,
            labels: {
                style: {
                    colors: ["#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2"]
                }
            }
        },
        legend: {
            labels: {
                colors: ["#a1aab2"]
            }
        },
        tooltip: {
            theme: "dark"
        }
    };

    var chart = new ApexCharts(document.querySelector("#chart-scatter-datetime  "), options_datetime);
    chart.render();

    function generateData1(count, yrange) {
        var i = 0;
        var series = [];
        while (i < count) {
            var x = 'w' + (i + 1).toString();
            var y = Math.floor(Math.random() * (yrange.max - yrange.min + 1)) + yrange.min;

            series.push({
                x: x,
                y: y
            });
            i++;
        }
        return series;
    }

    // Basic HeatMap Chart -------> HEATMAP CHART
    var options_basic = {
        series: [{
            name: 'Metric1',
            data: generateData1(18, {
                min: 0,
                max: 90
            })
        },
        {
            name: 'Metric2',
            data: generateData1(18, {
                min: 0,
                max: 90
            })
        },
        {
            name: 'Metric3',
            data: generateData1(18, {
                min: 0,
                max: 90
            })
        },
        {
            name: 'Metric4',
            data: generateData1(18, {
                min: 0,
                max: 90
            })
        },
        {
            name: 'Metric5',
            data: generateData1(18, {
                min: 0,
                max: 90
            })
        },
        {
            name: 'Metric6',
            data: generateData1(18, {
                min: 0,
                max: 90
            })
        },
        {
            name: 'Metric7',
            data: generateData1(18, {
                min: 0,
                max: 90
            })
        },
        {
            name: 'Metric8',
            data: generateData1(18, {
                min: 0,
                max: 90
            })
        },
        {
            name: 'Metric9',
            data: generateData1(18, {
                min: 0,
                max: 90
            })
        }
        ],
        chart: {
            fontFamily: '"Nunito Sans", sans-serif',
            height: 350,
            type: 'heatmap',
            toolbar: {
                show: false,
            },
            
        },
        xaxis: {
            labels: {
                style: {
                    colors: ["#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2"]
                }
            }
        },
        yaxis: {
            labels: {
                style: {
                    colors: ["#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2","#a1aab2"]
                }
            }
        },
        grid: {
            borderColor: 'rgba(0,0,0,0.3)',
        },
        dataLabels: {
            enabled: false
        },
        colors: ['#2962ff'],
        tooltip: {
            theme: "dark"
        },

    };

    var chart_heatmap_basic = new ApexCharts(document.querySelector("#chart-heatmap-basic"), options_basic);
    chart_heatmap_basic.render();

})
